target = "xilinx"
action = "synthesis"

syn_device = "xc6slx45t"
syn_grade = "-3"
syn_package = "fgg484"

syn_top = "spec_bupdown_conv_top"
syn_project = "spec_bupdown_conv.xise"

syn_tool = "ise"

modules = { "local" : "../../top/spec_bupdown_conv/"}

files = [
            "spec_bupdown_conv.ucf"
        ]
