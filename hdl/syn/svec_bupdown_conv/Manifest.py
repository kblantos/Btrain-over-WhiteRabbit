target = "xilinx"
action = "synthesis"

syn_device = "xc6slx150t"
syn_grade = "-3"
syn_package = "fgg900"

syn_top     = "svec_bupdown_conv_top"
syn_project = "svec_bupdown_conv.xise"

syn_tool = "ise"

files = [
    "svec_bupdown_conv.ucf",
]

modules = {
    "local" : [
        "../../top/svec_bupdown_conv/",
    ]
}
