target = "xilinx"
action = "synthesis"

syn_device = "xc6slx45t"
syn_grade = "-3"
syn_package = "fgg484"

syn_top = "spec_fixed_latency_ref_top"
syn_project = "spec_fixed_latency_ref.xise"

syn_tool = "ise"

modules = { "local" : "../../top/spec_fixed_latency_ref_design/"}

files = [
            "spec_fixed_latency_ref.ucf"
        ]
