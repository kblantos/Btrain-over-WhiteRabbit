target = "altera"
action = "synthesis"

syn_family  = "Arria V"
syn_device  = "5agxmb1g4f"
syn_grade   = "c4"
syn_package = "40"

syn_top     = "vfchd_btrain_ref_top"
syn_project = "vfchd_btrain_ref"

syn_tool = "quartus"

quartus_preflow = "quartus_preflow.tcl"

files = [
    "vfchd_btrain_ref.sdc",
    "quartus_preflow.tcl",
]

modules = {
    "local" : [
        "../../top/vfchd_ref_design/",
    ]
}
