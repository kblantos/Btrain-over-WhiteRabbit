target = "xilinx"
action = "synthesis"

syn_device = "xc6slx150t"
syn_grade = "-3"
syn_package = "fgg900"

syn_top     = "svec_btrain_ref_top"
syn_project = "svec_btrain_ref.xise"

syn_tool = "ise"

files = [
    "svec_btrain_ref.ucf",
]

modules = {
    "local" : [
        "../../top/svec_ref_design/",
    ]
}
