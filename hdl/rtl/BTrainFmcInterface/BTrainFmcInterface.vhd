-------------------------------------------------------------------------------
-- Title      : BTrainFrameInterface
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFrameInterface.vhd
-- Author     : 
-- Company    : CERN
-- Created    : 
-- Platform   : FPGA-generics
-- Standard   : VHDL
----------------------------------------------------------------------------------
-- Description:
--
-- 
--
-------------------------------------------------------------------------------
--
-- Copyright (c) 2018 CERN BE/CO/HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.gencores_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;
use work.wishbone_pkg.all;
use IEEE.NUMERIC_STD.ALL; -- to_unsigned
use work.RxBTFmcIF_wbgen2_pkg.all;
use work.TxBTFmcIF_wbgen2_pkg.all;
use work.BTrainFmcIf_pkg.all;
use work.BTrainFrame_pkg.all;

library unisim;
use unisim.vcomponents.all; -- for bufg

entity BTrainFmcInterface is
  generic (
    g_with_chipscope        : boolean :=FALSE
  );
  port(
    clk_sys_i               : in std_logic;
    rst_sys_n_i             : in std_logic;

    ----------------------------------------------------------------
    -- Interface with BTrain FMC
    ----------------------------------------------------------------
    --received from WR network:
    rx_FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
    rx_BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
    rx_IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
    rx_CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;
    rx_Frame_valid_pX_i    : in std_logic;
    rx_Frame_typeID_i      : in std_logic_vector(c_type_ID_size-1 downto 0);
    -- transmit to wr network:
    tx_ready_i             : in  std_logic; -- if ignored, any tx request is gracefully ignored
    tx_TransmitFrame_p1_o  : out std_logic;
    tx_FrameHeader_o       : out t_FrameHeader   := c_FrameHeader_zero;
    tx_BFramePayloads_o    : out t_BFramePayload := c_BFramePayload_zero;
    tx_IFramePayloads_o    : out t_IFramePayload := c_IFramePayload_zero;
    tx_CFramePayloads_o    : out t_CFramePayload := c_CFramePayload_zero;

    rx_FrameIrq_o          : out std_logic;
    rst_carrier_n_a_i      : in  std_logic;
    ----------------------------------------------------------------
    -- Wishbone interface (connect only if g_use_wb_config=true,
    -- otherwise you will run into problems
    ----------------------------------------------------------------
    rx_wb_clk_i            : in std_logic;
    rx_wb_slave_i          : in  t_wishbone_slave_in := cc_dummy_slave_in;
    rx_wb_slave_o          : out t_wishbone_slave_out;

    tx_wb_clk_i            : in std_logic;
    tx_wb_slave_i          : in  t_wishbone_slave_in := cc_dummy_slave_in;
    tx_wb_slave_o          : out t_wishbone_slave_out

  );
end BTrainFmcInterface;

architecture Behavioral of BTrainFmcInterface is
  
  -- in clk_sys_i clock domain
  signal tx_FrameHeader       : t_FrameHeader;
  signal tx_BFramePayloads    : t_BFramePayload;
  signal tx_IFramePayloads    : t_IFramePayload;
  signal tx_CFramePayloads    : t_CFramePayload;
  signal tx_scr_d             : std_logic;

  
  -- in rx_wb_clk_i domain
  signal rx_wb_clk            : std_logic;
  signal rx_reg_in            : t_RxBTFmcIF_in_registers;
  signal rx_reg_out           : t_RxBTFmcIF_out_registers;
  signal rx_wb_rst_n          : std_logic;
  signal rx_wb_slave_out      : t_wishbone_slave_out;
  signal rx_wb_slave_in       : t_wishbone_slave_in;
  signal rx_wb_slave_i_d      : t_wishbone_slave_in;
  signal rx_wb_regs_slave_in  : t_wishbone_slave_in;
  signal rx_wb_regs_slave_out : t_wishbone_slave_out;
  signal rx_FrameIrq          : std_logic;
  signal rx_ack_d1            : std_logic;
  signal rx_ack_d2            : std_logic;
  signal rx_mask              : std_logic;


  signal rx_Frame_typeID      : std_logic_vector(c_type_ID_size-1 downto 0);
  signal rx_Frame_valid_p1    : std_logic;
  signal rx_FrameHeader       : t_FrameHeader;
  signal rx_BFramePayloads    : t_BFramePayload;
  signal rx_IFramePayloads    : t_IFramePayload;
  signal rx_CFramePayloads    : t_CFramePayload;

  signal rx_Frame_typeID_reg  : std_logic_vector(c_type_ID_size-1 downto 0);
  signal rx_Frame_valid_p1_reg: std_logic;
  signal rx_FrameHeader_reg   : t_FrameHeader;
  signal rx_BFramePayloads_reg: t_BFramePayload;
  signal rx_IFramePayloads_reg: t_IFramePayload;
  signal rx_CFramePayloads_reg: t_CFramePayload;

  -- in tx_wb_clk_i domain
  signal tx_wb_clk            : std_logic;
  signal tx_reg_in            : t_TxBTFmcIF_in_registers;
  signal tx_reg_out           : t_TxBTFmcIF_out_registers;
  signal tx_wb_slave_out      : t_wishbone_slave_out;
  signal tx_wb_slave_in       : t_wishbone_slave_in;
  signal tx_wb_slave_i_d      : t_wishbone_slave_in;
  signal tx_wb_rst_n          : std_logic;
  signal tx_wb_regs_slave_in  : t_wishbone_slave_in;
  signal tx_wb_regs_slave_out : t_wishbone_slave_out;
  signal tx_TransmitFrame_p1  : std_logic;
  signal tx_ready_d           : std_logic;
  signal tx_ack_d1            : std_logic;
  signal tx_ack_d2            : std_logic;
  signal tx_mask              : std_logic;

  component chipscope_spartan6_icon
    port (
      CONTROL0 : inout std_logic_vector(35 downto 0));
  end component;

  component chipscope_spartan6_ila
    port (
      CONTROL : inout std_logic_vector(35 downto 0);
      CLK     : in    std_logic;
      TRIG0   : in    std_logic_vector(31 downto 0);
      TRIG1   : in    std_logic_vector(31 downto 0);
      TRIG2   : in    std_logic_vector(31 downto 0);
      TRIG3   : in    std_logic_vector(31 downto 0));
  end component;

  signal control0                   : std_logic_vector(35 downto 0);
  signal trig0, trig1, trig2, trig3 : std_logic_vector(31 downto 0);

  signal control20                   : std_logic_vector(35 downto 0);
  signal trig20, trig21, trig22, trig23 : std_logic_vector(31 downto 0);

begin
  ------------------------------------------------------------------------------
  -- TX: TxWB in tx_WB clock domain (provided by the carrier), tx in sys_clk
  ------------------------------------------------------------------------------
  -- synchronize request from the carrier to send data with the sys_clk
  U_sync_tx_valid : gc_sync_ffs
    port map (
      clk_i          => clk_sys_i,
      rst_n_i        => rst_sys_n_i,
      data_i         => tx_reg_out.txbtfmcif_scr_tx_o,
      ppulse_o       => tx_scr_d);

  -- latch data in the sys_clk and then request transmission
  p_tx_data: process (clk_sys_i) 
  begin
    if rising_edge(clk_sys_i) then
      if(rst_sys_n_i = '0') then
        tx_TransmitFrame_p1   <= '0';
        tx_FrameHeader_o        <= c_FrameHeader_zero;
        tx_BFramePayloads_o     <= c_BFramePayload_zero;
        tx_IFramePayloads_o     <= c_IFramePayload_zero;
        tx_CFramePayloads_o     <= c_CFramePayload_zero;

      else
        if( tx_scr_d = '1' and tx_ready_i = '1') then
          tx_TransmitFrame_p1 <= '1';
          tx_FrameHeader_o      <= tx_FrameHeader;
          tx_BFramePayloads_o   <= tx_BFramePayloads;
          tx_IFramePayloads_o   <= tx_IFramePayloads;
          tx_CFramePayloads_o   <= tx_CFramePayloads;
        else
          tx_TransmitFrame_p1 <= '0';
        end if;
      end if;
    end if;
  end process;

  tx_TransmitFrame_p1_o <= tx_TransmitFrame_p1;

  cmp_tx_wb_clk_buf_i : BUFG
    port map (
      O => tx_wb_clk,
      I => tx_wb_clk_i);

  U_sync_tx_rst : gc_sync_ffs
    port map (
      clk_i          => tx_wb_clk,
      rst_n_i        => '1',
      data_i         => rst_carrier_n_a_i,
      synced_o       => tx_wb_rst_n);

  --------------------------------------------------------------------------------
  -- special mode for PIPELINED & CLASSIC WB with FF in IOB
  --------------------------------------------------------------------------------

  p_tx_wb_reg: process (tx_wb_clk)
  begin
    if rising_edge(tx_wb_clk) then
      if(tx_wb_rst_n = '0') then
        tx_wb_slave_o  <= cc_dummy_slave_out;
      else
        ----------------------------------------------------------------
        -- ensure IOB
        ----------------------------------------------------------------
        -- Input must be exclusively registered (no logic attached) to
        -- be placed in IOB
        tx_wb_slave_i_d <= tx_wb_slave_i;
        -- Output must exit directly flip-flop (no logic attached) to 
        -- be palced in IOB
        tx_wb_slave_o   <= tx_wb_slave_out;

        ----------------------------------------------------------------
        -- making wbgen2-generated slave to work (it assumes that
        -- stb goes down in the next cycle after ack)
        ----------------------------------------------------------------
        tx_ack_d1 <= tx_wb_slave_out.ack;
        tx_ack_d2 <= tx_ack_d1;
      end if;
    end if;
  end process;

  tx_mask <= '1' when (tx_ack_d1 = '1' or  tx_ack_d2 = '1') else '0';

  tx_wb_slave_in.adr <= tx_wb_slave_i_d.adr ;
  tx_wb_slave_in.dat <= tx_wb_slave_i_d.dat ;
  tx_wb_slave_in.cyc <= tx_wb_slave_i_d.cyc;
  tx_wb_slave_in.sel <= tx_wb_slave_i_d.sel ;
  tx_wb_slave_in.stb <= tx_wb_slave_i_d.stb and not tx_mask;
  tx_wb_slave_in.we  <= tx_wb_slave_i_d.we ;

  cmp_wb_tx_regs: TxBTFmcIF_wb
    port map(
      rst_n_i            => tx_wb_rst_n,
      clk_sys_i          => tx_wb_clk,
      wb_adr_i           => tx_wb_slave_in.adr(5 downto 0),
      wb_dat_i           => tx_wb_slave_in.dat(7 downto 0),
      wb_dat_o           => tx_wb_slave_out.dat(7 downto 0),
      wb_cyc_i           => tx_wb_slave_in.cyc,
      wb_sel_i           => tx_wb_slave_in.sel(0 downto 0),
      wb_stb_i           => tx_wb_slave_in.stb,
      wb_we_i            => tx_wb_slave_in.we,
      wb_ack_o           => tx_wb_slave_out.ack,
      wb_stall_o         => tx_wb_slave_out.stall,
      regs_i             => tx_reg_in,
      regs_o             => tx_reg_out
  );

  U_sync_tx_ready : gc_sync_ffs
    port map (
      clk_i          => tx_wb_clk,
      rst_n_i        => tx_wb_rst_n,
      data_i         => tx_ready_i,
      synced_o       => tx_ready_d);
  ------------------------------------------------------------------------------
  -- RX, mainly in rx_WB clock domain (clock provided by the carrier)
  ------------------------------------------------------------------------------
  
  cmp_rx_wb_clk_buf_i : BUFG
    port map (
      O => rx_wb_clk,
      I => rx_wb_clk_i);
  
  U_sync_rx_rst : gc_sync_ffs
    port map (
      clk_i          => rx_wb_clk,
      rst_n_i        => '1',
      data_i         => rst_carrier_n_a_i,
      synced_o       => rx_wb_rst_n);

  -- synch rx valid signal with rx_WB clock domain
  U_sync_rx_valid : gc_sync_ffs
    port map (
      clk_i          => rx_wb_clk,
      rst_n_i        => rx_wb_rst_n,
      data_i         => rx_Frame_valid_pX_i,
      ppulse_o       => rx_Frame_valid_p1);

  -- latch content of received frame in the rx_WB clock domain and 
  -- let know the Carrier (via IRQ line) that something has been received
  p_rx_data: process (rx_wb_clk)
  begin
    if rising_edge(rx_wb_clk) then
      if(rx_wb_rst_n = '0') then
        rx_FrameHeader    <= c_FrameHeader_zero;
        rx_BFramePayloads <= c_BFramePayload_dummy;
        rx_IFramePayloads <= c_IFramePayload_zero;
        rx_CFramePayloads <= c_CFramePayload_zero;
        rx_FrameIrq       <= '0';
        rx_FrameIrq_o     <= '0';
        rx_Frame_typeID   <= (others => '0');
        rx_Frame_valid_p1_reg <= '0';
        rx_Frame_typeID_reg   <= (others => '0');
        rx_FrameHeader_reg    <= c_FrameHeader_zero;
        rx_BFramePayloads_reg <= c_BFramePayload_dummy;
        rx_IFramePayloads_reg <= c_IFramePayload_zero;
        rx_CFramePayloads_reg <= c_CFramePayload_zero;
      else

        -- clear irq, it is set later only if cleard
        if(rx_reg_out.rxbtfmcif_scr_irq_clr_o = '1') then
          rx_FrameIrq           <= '0';
        end if;


        if(rx_Frame_valid_p1 = '1' and rx_FrameIrq = '0' and rx_Frame_valid_p1_reg = '0') then
          -- 1) Normal situation: 
          --    frame received when no readout
          rx_Frame_typeID       <= rx_Frame_typeID_i;
          rx_FrameHeader        <= rx_FrameHeader_i;
          rx_BFramePayloads     <= rx_BFramePayloads_i;
          rx_IFramePayloads     <= rx_IFramePayloads_i;
          rx_CFramePayloads     <= rx_CFramePayloads_i;
          rx_FrameIrq           <= '1';

        elsif(rx_Frame_valid_p1 = '1' and rx_FrameIrq = '1' and rx_Frame_valid_p1_reg = '0') then
          -- 2) Abnormal and likely:
          --    Readout of the full frame takes 3us, the jitter of frame arrival
          --    is ~1.2us. Frames are sent very 4us. Thus, it is possible that
          --    a frame arrives when the previous is still read, yet this should
          --    average out, so only single stage of buffer is needed.
          rx_Frame_valid_p1_reg <= '1';
          rx_Frame_typeID_reg   <= rx_Frame_typeID_i;
          rx_FrameHeader_reg    <= rx_FrameHeader_i;
          rx_BFramePayloads_reg <= rx_BFramePayloads_i;
          rx_IFramePayloads_reg <= rx_IFramePayloads_i;
          rx_CFramePayloads_reg <= rx_CFramePayloads_i;

        elsif(rx_Frame_valid_p1 = '1' and rx_FrameIrq = '0' and rx_Frame_valid_p1_reg = '1') then
          -- 3) Abnormal situation quite unlikely:
          --    new frame was received just at the moment when we stopped
          --    transfering to the carrier frame and we could release the
          --    buffer (reg), so we transfer the frame from the buffer and
          --    store the new frame.
          -- Transfer stored frame:
          rx_Frame_typeID       <= rx_Frame_typeID_reg;
          rx_FrameHeader        <= rx_FrameHeader_reg;
          rx_BFramePayloads     <= rx_BFramePayloads_reg;
          rx_IFramePayloads     <= rx_IFramePayloads_reg;
          rx_CFramePayloads     <= rx_CFramePayloads_reg;
          rx_FrameIrq           <= '1';
          -- store new frame:
          rx_Frame_valid_p1_reg <= '1';
          rx_Frame_typeID_reg   <= rx_Frame_typeID_i;
          rx_FrameHeader_reg    <= rx_FrameHeader_i;
          rx_BFramePayloads_reg <= rx_BFramePayloads_i;
          rx_IFramePayloads_reg <= rx_IFramePayloads_i;
          rx_CFramePayloads_reg <= rx_CFramePayloads_i;

        elsif(rx_Frame_valid_p1 = '0' and rx_FrameIrq = '0' and rx_Frame_valid_p1_reg = '1') then
          -- clear buffer: push the frame received when previous readout
          rx_Frame_valid_p1_reg <= '0';
          rx_Frame_typeID       <= rx_Frame_typeID_reg;
          rx_FrameHeader        <= rx_FrameHeader_reg;
          rx_BFramePayloads     <= rx_BFramePayloads_reg;
          rx_IFramePayloads     <= rx_IFramePayloads_reg;
          rx_CFramePayloads     <= rx_CFramePayloads_reg;
          rx_FrameIrq           <= '1';

        end if;
        -- to put it into IOB
        rx_FrameIrq_o <= rx_FrameIrq;

      end if;
    end if;
  end process;

  --------------------------------------------------------------------------------
  -- special mode for PIPELINED & CLASSIC WB with FF in IOB
  --------------------------------------------------------------------------------

  p_rx_wb_reg: process (rx_wb_clk)
  begin
    if rising_edge(rx_wb_clk) then
      if(rx_wb_rst_n = '0') then
        rx_wb_slave_o  <= cc_dummy_slave_out;
      else
        ----------------------------------------------------------------
        -- ensure IOB
        ----------------------------------------------------------------
        -- Input must be exclusively registered (no logic attached) to
        -- be placed in IOB
        rx_wb_slave_i_d <= rx_wb_slave_i;
        -- Output must exit directly flip-flop (no logic attached) to 
        -- be palced in IOB
        rx_wb_slave_o  <= rx_wb_slave_out;

        ----------------------------------------------------------------
        -- making wbgen2-generated slave to work (it assumes that
        -- stb goes down in the next cycle after ack)
        ----------------------------------------------------------------
        rx_ack_d1 <= rx_wb_slave_out.ack;
        rx_ack_d2 <= rx_ack_d1;
      end if;
    end if;
  end process;

  rx_mask <= '1' when (rx_ack_d1 = '1' or  rx_ack_d2 = '1') else '0';

  rx_wb_slave_in.adr <= rx_wb_slave_i_d.adr ;
  rx_wb_slave_in.dat <= rx_wb_slave_i_d.dat ;
  rx_wb_slave_in.cyc <= rx_wb_slave_i_d.cyc;
  rx_wb_slave_in.sel <= rx_wb_slave_i_d.sel ;
  rx_wb_slave_in.stb <= rx_wb_slave_i_d.stb and not rx_mask;
  rx_wb_slave_in.we  <= rx_wb_slave_i_d.we ;

  cmp_wb_rx_regs: RxBTFmcIF_wb
    port map(
      rst_n_i            => rx_wb_rst_n,
      clk_sys_i          => rx_wb_clk,
      wb_adr_i           => rx_wb_slave_in.adr(5 downto 0),
      wb_dat_i           => rx_wb_slave_in.dat(7 downto 0),
      wb_dat_o           => rx_wb_slave_out.dat(7 downto 0),
      wb_cyc_i           => rx_wb_slave_in.cyc,
      wb_sel_i           => rx_wb_slave_in.sel(0 downto 0),
      wb_stb_i           => rx_wb_slave_in.stb,
      wb_we_i            => rx_wb_slave_in.we,
      wb_ack_o           => rx_wb_slave_out.ack,
      wb_stall_o         => rx_wb_slave_out.stall,
      regs_i             => rx_reg_in,
      regs_o             => rx_reg_out
  );
  ------------------------------------------------------------------------------
  -- rx WB registers
  ------------------------------------------------------------------------------
  -- header
  rx_reg_in.rxbtfmcif_scr_irq_i                  <= rx_FrameIrq;
  rx_reg_in.rxbtfmcif_bheader_1_frame_type_i     <= rx_FrameHeader.frame_type;
  rx_reg_in.rxbtfmcif_bheader_2_sim_eff_i        <= rx_FrameHeader.sim_eff;
  rx_reg_in.rxbtfmcif_bheader_2_error_i          <= rx_FrameHeader.error;
  rx_reg_in.rxbtfmcif_bheader_2_c0_i             <= rx_FrameHeader.C0;
  rx_reg_in.rxbtfmcif_bheader_2_zero_cycle_i     <= rx_FrameHeader.zero_cycle;
  rx_reg_in.rxbtfmcif_bheader_2_f_low_marker_i   <= rx_FrameHeader.f_low_marker;
  rx_reg_in.rxbtfmcif_bheader_2_d_low_marker_i   <= rx_FrameHeader.d_low_marker;
  rx_reg_in.rxbtfmcif_bheader_2_version_id_i     <= rx_FrameHeader.version_id;
  
  -- BFrame
  rx_reg_in.rxbtfmcif_bframepayload_0_b_0_i      <= rx_BFramePayloads.B( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_1_b_1_i      <= rx_BFramePayloads.B(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_2_b_2_i      <= rx_BFramePayloads.B(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_3_b_3_i      <= rx_BFramePayloads.B(31 downto 24);
  rx_reg_in.rxbtfmcif_bframepayload_4_bdot_0_i   <= rx_BFramePayloads.Bdot( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_5_bdot_1_i   <= rx_BFramePayloads.Bdot(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_6_bdot_2_i   <= rx_BFramePayloads.Bdot(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_7_bdot_3_i   <= rx_BFramePayloads.Bdot(31 downto 24);
  rx_reg_in.rxbtfmcif_bframepayload_8_oldb_0_i   <= rx_BFramePayloads.oldB( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_9_oldb_1_i   <= rx_BFramePayloads.oldB(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_10_oldb_2_i  <= rx_BFramePayloads.oldB(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_11_oldb_3_i  <= rx_BFramePayloads.oldB(31 downto 24);
  rx_reg_in.rxbtfmcif_bframepayload_12_measb_0_i <= rx_BFramePayloads.measB( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_13_measb_1_i <= rx_BFramePayloads.measB(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_14_measb_2_i <= rx_BFramePayloads.measB(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_15_measb_3_i <= rx_BFramePayloads.measB(31 downto 24);
  rx_reg_in.rxbtfmcif_bframepayload_16_simb_0_i  <= rx_BFramePayloads.simB( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_17_simb_1_i  <= rx_BFramePayloads.simB(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_18_simb_2_i  <= rx_BFramePayloads.simB(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_19_simb_3_i  <= rx_BFramePayloads.simB(31 downto 24);
  rx_reg_in.rxbtfmcif_bframepayload_20_synb_0_i  <= rx_BFramePayloads.synB( 7 downto  0);
  rx_reg_in.rxbtfmcif_bframepayload_21_synb_1_i  <= rx_BFramePayloads.synB(15 downto  8);
  rx_reg_in.rxbtfmcif_bframepayload_22_synb_2_i  <= rx_BFramePayloads.synB(23 downto 16);
  rx_reg_in.rxbtfmcif_bframepayload_23_synb_3_i  <= rx_BFramePayloads.synB(31 downto 24);

  -- IFrame
  rx_reg_in.rxbtfmcif_iframepayload_0_i_0_i      <= rx_IFramePayloads.I( 7 downto  0);
  rx_reg_in.rxbtfmcif_iframepayload_1_i_1_i      <= rx_IFramePayloads.I(15 downto  8);
  rx_reg_in.rxbtfmcif_iframepayload_2_i_2_i      <= rx_IFramePayloads.I(23 downto 16);
  rx_reg_in.rxbtfmcif_iframepayload_3_i_3_i      <= rx_IFramePayloads.I(31 downto 24);
  
  -- CFrame
  rx_reg_in.rxbtfmcif_cframepayload_0_i_0_i         <= rx_CFramePayloads.I( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_1_i_1_i         <= rx_CFramePayloads.I(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_2_i_2_i         <= rx_CFramePayloads.I(23 downto 16);
  rx_reg_in.rxbtfmcif_cframepayload_3_i_3_i         <= rx_CFramePayloads.I(31 downto 24);
  rx_reg_in.rxbtfmcif_cframepayload_4_v_0_i         <= rx_CFramePayloads.V( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_5_v_1_i         <= rx_CFramePayloads.V(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_6_v_2_i         <= rx_CFramePayloads.V(23 downto 16);
  rx_reg_in.rxbtfmcif_cframepayload_7_v_3_i         <= rx_CFramePayloads.V(31 downto 24);
  rx_reg_in.rxbtfmcif_cframepayload_8_srcid_0_i     <= rx_CFramePayloads.CSrcId( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_9_srcid_1_i     <= rx_CFramePayloads.CSrcId(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_10_srcid_2_i    <= rx_CFramePayloads.MSrcId( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_11_srcid_3_i    <= rx_CFramePayloads.MSrcId(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_12_userdata_0_i <= rx_CFramePayloads.UserData( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_13_userdata_1_i <= rx_CFramePayloads.UserData(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_14_userdata_2_i <= rx_CFramePayloads.UserData(23 downto 16);
  rx_reg_in.rxbtfmcif_cframepayload_15_userdata_3_i <= rx_CFramePayloads.UserData(31 downto 24);
  rx_reg_in.rxbtfmcif_cframepayload_16_reserved_1_0_i <= rx_CFramePayloads.Reserved_1( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_17_reserved_1_1_i <= rx_CFramePayloads.Reserved_1(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_18_reserved_1_2_i <= rx_CFramePayloads.Reserved_1(23 downto 16);
  rx_reg_in.rxbtfmcif_cframepayload_19_reserved_1_3_i <= rx_CFramePayloads.Reserved_1(31 downto 24);
  rx_reg_in.rxbtfmcif_cframepayload_20_reserved_2_0_i <= rx_CFramePayloads.Reserved_2( 7 downto  0);
  rx_reg_in.rxbtfmcif_cframepayload_21_reserved_2_1_i <= rx_CFramePayloads.Reserved_2(15 downto  8);
  rx_reg_in.rxbtfmcif_cframepayload_22_reserved_2_2_i <= rx_CFramePayloads.Reserved_2(23 downto 16);
  rx_reg_in.rxbtfmcif_cframepayload_23_reserved_2_3_i <= rx_CFramePayloads.Reserved_2(31 downto 24);

  ------------------------------------------------------------------------------
  -- tx WB registers
  ------------------------------------------------------------------------------
  -- Header
  tx_FrameHeader.frame_type                      <= tx_reg_out.txbtfmcif_bheader_1_frame_type_o;
  tx_FrameHeader.sim_eff                         <= tx_reg_out.txbtfmcif_bheader_2_sim_eff_o;
  tx_FrameHeader.error                           <= tx_reg_out.txbtfmcif_bheader_2_error_o;
  tx_FrameHeader.C0                              <= tx_reg_out.txbtfmcif_bheader_2_c0_o;
  tx_FrameHeader.zero_cycle                      <= tx_reg_out.txbtfmcif_bheader_2_zero_cycle_o;
  tx_FrameHeader.f_low_marker                    <= tx_reg_out.txbtfmcif_bheader_2_f_low_marker_o;
  tx_FrameHeader.d_low_marker                    <= tx_reg_out.txbtfmcif_bheader_2_d_low_marker_o;
  tx_FrameHeader.version_id                      <= tx_reg_out.txbtfmcif_bheader_2_version_id_o;

  -- BFrame
  tx_BFramePayloads.B( 7 downto  0)              <= tx_reg_out.txbtfmcif_bframepayload_0_b_0_o;
  tx_BFramePayloads.B(15 downto  8)              <= tx_reg_out.txbtfmcif_bframepayload_1_b_1_o;
  tx_BFramePayloads.B(23 downto 16)              <= tx_reg_out.txbtfmcif_bframepayload_2_b_2_o;
  tx_BFramePayloads.B(31 downto 24)              <= tx_reg_out.txbtfmcif_bframepayload_3_b_3_o;
  tx_BFramePayloads.Bdot( 7 downto  0)           <= tx_reg_out.txbtfmcif_bframepayload_4_bdot_0_o;
  tx_BFramePayloads.Bdot(15 downto  8)           <= tx_reg_out.txbtfmcif_bframepayload_5_bdot_1_o;
  tx_BFramePayloads.Bdot(23 downto 16)           <= tx_reg_out.txbtfmcif_bframepayload_6_bdot_2_o;
  tx_BFramePayloads.Bdot(31 downto 24)           <= tx_reg_out.txbtfmcif_bframepayload_7_bdot_3_o;
  tx_BFramePayloads.oldB( 7 downto  0)           <= tx_reg_out.txbtfmcif_bframepayload_8_oldb_0_o;
  tx_BFramePayloads.oldB(15 downto  8)           <= tx_reg_out.txbtfmcif_bframepayload_9_oldb_1_o;
  tx_BFramePayloads.oldB(23 downto 16)           <= tx_reg_out.txbtfmcif_bframepayload_10_oldb_2_o;
  tx_BFramePayloads.oldB(31 downto 24)           <= tx_reg_out.txbtfmcif_bframepayload_11_oldb_3_o;
  tx_BFramePayloads.measB( 7 downto  0)          <= tx_reg_out.txbtfmcif_bframepayload_12_measb_0_o;
  tx_BFramePayloads.measB(15 downto  8)          <= tx_reg_out.txbtfmcif_bframepayload_13_measb_1_o;
  tx_BFramePayloads.measB(23 downto 16)          <= tx_reg_out.txbtfmcif_bframepayload_14_measb_2_o;
  tx_BFramePayloads.measB(31 downto 24)          <= tx_reg_out.txbtfmcif_bframepayload_15_measb_3_o;
  tx_BFramePayloads.simB( 7 downto  0)           <= tx_reg_out.txbtfmcif_bframepayload_16_simb_0_o;
  tx_BFramePayloads.simB(15 downto  8)           <= tx_reg_out.txbtfmcif_bframepayload_17_simb_1_o;
  tx_BFramePayloads.simB(23 downto 16)           <= tx_reg_out.txbtfmcif_bframepayload_18_simb_2_o;
  tx_BFramePayloads.simB(31 downto 24)           <= tx_reg_out.txbtfmcif_bframepayload_19_simb_3_o;
  tx_BFramePayloads.synB( 7 downto  0)           <= tx_reg_out.txbtfmcif_bframepayload_20_synb_0_o;
  tx_BFramePayloads.synB(15 downto  8)           <= tx_reg_out.txbtfmcif_bframepayload_21_synb_1_o;
  tx_BFramePayloads.synB(23 downto 16)           <= tx_reg_out.txbtfmcif_bframepayload_22_synb_2_o;
  tx_BFramePayloads.synB(31 downto 24)           <= tx_reg_out.txbtfmcif_bframepayload_23_synb_3_o;

  -- IFrame

  tx_IFramePayloads.I( 7 downto  0)              <= tx_reg_out.txbtfmcif_iframepayload_0_i_0_o;
  tx_IFramePayloads.I(15 downto  8)              <= tx_reg_out.txbtfmcif_iframepayload_1_i_1_o;
  tx_IFramePayloads.I(23 downto 16)              <= tx_reg_out.txbtfmcif_iframepayload_2_i_2_o;
  tx_IFramePayloads.I(31 downto 24)              <= tx_reg_out.txbtfmcif_iframepayload_3_i_3_o;

  -- CFrame
  tx_CFramePayloads.I( 7 downto  0)              <= tx_reg_out.txbtfmcif_cframepayload_0_i_0_o;
  tx_CFramePayloads.I(15 downto  8)              <= tx_reg_out.txbtfmcif_cframepayload_1_i_1_o;
  tx_CFramePayloads.I(23 downto 16)              <= tx_reg_out.txbtfmcif_cframepayload_2_i_2_o;
  tx_CFramePayloads.I(31 downto 24)              <= tx_reg_out.txbtfmcif_cframepayload_3_i_3_o;
  tx_CFramePayloads.V( 7 downto  0)              <= tx_reg_out.txbtfmcif_cframepayload_4_v_0_o;
  tx_CFramePayloads.V(15 downto  8)              <= tx_reg_out.txbtfmcif_cframepayload_5_v_1_o;
  tx_CFramePayloads.V(23 downto 16)              <= tx_reg_out.txbtfmcif_cframepayload_6_v_2_o;
  tx_CFramePayloads.V(31 downto 24)              <= tx_reg_out.txbtfmcif_cframepayload_7_v_3_o;
  tx_CFramePayloads.CSrcId( 7 downto  0)         <= tx_reg_out.txbtfmcif_cframepayload_8_srcid_0_o;
  tx_CFramePayloads.CSrcId(15 downto  8)         <= tx_reg_out.txbtfmcif_cframepayload_9_srcid_1_o;
  tx_CFramePayloads.MSrcId( 7 downto  0)         <= tx_reg_out.txbtfmcif_cframepayload_10_srcid_2_o;
  tx_CFramePayloads.MSrcId(15 downto  8)         <= tx_reg_out.txbtfmcif_cframepayload_11_srcid_3_o;
  tx_CFramePayloads.UserData( 7 downto  0)       <= tx_reg_out.txbtfmcif_cframepayload_12_userdata_0_o;
  tx_CFramePayloads.UserData(15 downto  8)       <= tx_reg_out.txbtfmcif_cframepayload_13_userdata_1_o;
  tx_CFramePayloads.UserData(23 downto 16)       <= tx_reg_out.txbtfmcif_cframepayload_14_userdata_2_o;
  tx_CFramePayloads.UserData(31 downto 24)       <= tx_reg_out.txbtfmcif_cframepayload_15_userdata_3_o;
  tx_CFramePayloads.Reserved_1( 7 downto  0)     <= tx_reg_out.txbtfmcif_cframepayload_16_reserved_1_0_o;
  tx_CFramePayloads.Reserved_1(15 downto  8)     <= tx_reg_out.txbtfmcif_cframepayload_17_reserved_1_1_o;
  tx_CFramePayloads.Reserved_1(23 downto 16)     <= tx_reg_out.txbtfmcif_cframepayload_18_reserved_1_2_o;
  tx_CFramePayloads.Reserved_1(31 downto 24)     <= tx_reg_out.txbtfmcif_cframepayload_19_reserved_1_3_o;
  tx_CFramePayloads.Reserved_2( 7 downto  0)     <= tx_reg_out.txbtfmcif_cframepayload_20_reserved_2_0_o;
  tx_CFramePayloads.Reserved_2(15 downto  8)     <= tx_reg_out.txbtfmcif_cframepayload_21_reserved_2_1_o;
  tx_CFramePayloads.Reserved_2(23 downto 16)     <= tx_reg_out.txbtfmcif_cframepayload_22_reserved_2_2_o;
  tx_CFramePayloads.Reserved_2(31 downto 24)     <= tx_reg_out.txbtfmcif_cframepayload_23_reserved_2_3_o;

  tx_reg_in.txbtfmcif_scr_ready_i                <= tx_ready_d;

  ------------------------------------------------------------------------------
  -- chipscipe
  -- Instantiate chipscope only if explicitely required.
  ------------------------------------------------------------------------------
  gen_with_chipscope: if (g_with_chipscope = TRUE) generate
    TRIG0 ( 7 downto  0) <= rx_wb_slave_in.adr(7 downto 0);
    TRIG0 (15 downto  8) <= rx_wb_slave_out.dat(7 downto 0);
    TRIG0 (23 downto 16) <= rx_wb_slave_in.dat(7 downto 0);
    TRIG0 (          24) <= rx_wb_slave_in.we;
    TRIG0 (          25) <= rx_wb_slave_in.cyc;
    TRIG0 (          26) <= rx_wb_slave_in.stb;
    TRIG0 (          27) <= rx_wb_slave_out.ack;
    TRIG0 (          28) <= rx_wb_slave_out.err;
    TRIG0 (          29) <= rx_wb_slave_out.rty;
    TRIG0 (          30) <= rx_wb_slave_out.stall;
    TRIG0 (          31) <= rx_FrameIrq;

    TRIG1 ( 7 downto  0) <= tx_wb_slave_in.adr(7 downto 0);
    TRIG1 (15 downto  8) <= tx_wb_slave_out.dat(7 downto 0);
    TRIG1 (23 downto 16) <= tx_wb_slave_in.dat(7 downto 0);
    TRIG1 (          24) <= tx_wb_slave_in.we;
    TRIG1 (          25) <= tx_wb_slave_in.cyc;
    TRIG1 (          26) <= tx_wb_slave_in.stb;
    TRIG1 (          27) <= tx_wb_slave_out.ack;
    TRIG1 (          28) <= tx_wb_slave_out.err;
    TRIG1 (          29) <= tx_wb_slave_out.rty;
    TRIG1 (          30) <= tx_wb_slave_out.stall;
    TRIG1 (          31) <= tx_ready_i;


    TRIG2 ( 7 downto  0) <= rx_FrameHeader_i.frame_type;
    TRIG2 (15 downto  8) <= rx_FrameHeader.frame_type;
    TRIG2 (23 downto 16) <= tx_FrameHeader.frame_type;
    TRIG2 (          24) <= rx_Frame_valid_p1;
    TRIG2 (          25) <= rx_reg_out.rxbtfmcif_scr_irq_clr_o;
    TRIG2 (          26) <= rx_Frame_valid_pX_i;
    TRIG2 (          27) <= tx_scr_d;
    TRIG2 (          28) <= tx_reg_out.txbtfmcif_scr_tx_o;
    TRIG2 (          29) <= '0';
    TRIG2 (          30) <= '0';
    TRIG2 (          31) <= '0';

    TRIG3 (15 downto  0) <= rx_BFramePayloads_i.B(15 downto  0);
    TRIG3 (31 downto 16) <= tx_BFramePayloads.B(15 downto  0);

    CS_ICON : chipscope_spartan6_icon
      port map (
        CONTROL0 => CONTROL0);
    CS_ILA : chipscope_spartan6_ila
      port map (
        CONTROL => CONTROL0,
        CLK     => rx_wb_clk,
        TRIG0   => TRIG0,
        TRIG1   => TRIG1,
        TRIG2   => TRIG2,
        TRIG3   => TRIG3);
  end generate gen_with_chipscope;
end Behavioral;

