--------------------------------------------------------------------------------
-- CERN
-- BTrain-over-WhiteRabbit
-- https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
--------------------------------------------------------------------------------
--
-- unit name:     bupdown_converter
--
-- description:
--
-- This module converts the B field value received using WR-BTrain in Ethernet
-- frame into the legacy output. In particular it produces two types of pulses:
-- * Up   pulse indicates increase of B field by 0.1G
-- * Down pulse indicates decrease of B field by 0.1G
--
-- Each pulse has width of g_PULSE_WIDTH_NS (by default 63*16=1008 ns).
-- There is minimum space between pulses of g_MIN_PULSE_GAP_NS (by
-- default (62*16=992 ns). Thus, the max frequency of the pulses is
-- 1/(g_PULSE_WIDTH_NS+g_MIN_PULSE_GAP_NS), by default 500kHz.
--
--------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN BE/CO/HT
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- to_unsigned

library work;
use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;
use work.wishbone_pkg.all;
use work.BTrain_wbgen2_pkg.all;

entity bupdown_converter is
  generic (
    g_PULSE_WIDTH_NS    : integer := 1000;
    g_MIN_PULSE_GAP_NS  : integer := 1000;
    g_CLOCK_PERIOD_NS   : integer := 16
    );
  port(
    clk_i               : in std_logic;
    rst_n_i             : in std_logic;

    ----------------------------------------------------------------
    -- Interface with BTrain FMC
    ----------------------------------------------------------------
    --received from WR network:
    rx_frame_header_i      : in  t_FrameHeader;
    rx_bframe_payloads_i   : in  t_BFramePayload;
    rx_frame_valid_p_i     : in  std_logic;
    -- configuration of polarity from the BTrainFrameTransceiver 
    -- we use configuration capabilities of the transceiver to configure
    -- the converter
    rx_cfg_pol_inv_i       : in  std_logic;
    ----------------------------------------------------------------
    -- Legacy Bup and Bdown pulses
    ----------------------------------------------------------------
    bup_o                  : out std_logic;
    bdown_o                : out std_logic
  );
end bupdown_converter;

architecture arch of bupdown_converter is

  -- Increment of the Up/Down integration counter, this is the resolution
  -- of the old BUpDown system.
  constant c_BUPDOWN_CNT_INC  : signed(31 downto 0) := to_signed(1000,32);

  -- reset value of the counter
  constant c_BUPDOWN_CNT_ZERO : signed(31 downto 0) := to_signed(0,32);

  -- states of simple state machine
  type t_tx_bupdown_fsm is (S_IDLE, S_COMPARE, S_TX_PULSE, S_TX_GAP);
  signal s_state           : t_tx_bupdown_fsm;

  -- values stored from the WR-BTrain Frame
  signal rxed_B_frame      : std_logic;
  signal rxed_B_value      : signed(31 downto 0);
  signal rxed_B_valid      : std_logic;

  -- counter to provide proper width of up/down pulse
  signal pulse_width_cnt   :  unsigned(31 downto 0);

  -- counter to ensure minimum width of the gap between up/down pulses
  signal gap_width_cnt     :  unsigned(31 downto 0);

  -- BUpDown counter that calculates the integrated value outputed
  -- using Up/Down pulses
  signal   bupdown_cnt     : signed(31 downto 0);

  signal rx_valid_active_high_p : std_logic;

  signal active_level            : std_logic;

begin

  -- to simplifie the logic, react only on active high
  rx_valid_active_high_p  <= rx_frame_valid_p_i when rx_cfg_pol_inv_i = '0' else
                             not rx_frame_valid_p_i;

  -- to simplify the code, indicate when we receive the frame in which
  -- we are interested
  rxed_B_frame <= '1' when (rx_valid_active_high_p = '1' and
                           rx_frame_header_i.frame_type   = c_ID_BkFrame) else
                 '0';

  -- process to store the B value received in the BTrain frame and remeber
  -- that we've received a valid B value. The memory is resetted when we
  -- receive the C0 signal - reset
  p_rx_B: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0') then
        rxed_B_value             <= c_BUPDOWN_CNT_ZERO;
        rxed_B_valid             <= '0';
      else
        if(rxed_B_frame = '1') then
           if(rx_frame_header_i.C0 = '1') then
             rxed_B_value        <= c_BUPDOWN_CNT_ZERO;
             rxed_B_valid        <= '0';
           else
             rxed_B_value        <= signed(rx_bframe_payloads_i.B);
             rxed_B_valid        <= '1';
           end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- process with FSM that produces the B Up/Down pulses. It compares
  -- * the internal counter which represents the integrated value of the
  --   transmitted UP/Down pulses since the reset,
  --   with
  -- * the B value received in the last frame
  --
  -- If the received value is greater than the internal counter by the
  -- integration step, the B Up pulse is produced
  --
  -- If the received value is smaller than the internal counter by the
  -- integration step, the B Down pulse is produced
  --
  -- Note that the integration step is 1000 times the resolution of the B value
  -- received in the BTrain frame
  ------------------------------------------------------------------------------

  -- just to make things more readable: define active level which is
  -- LOW  if inverting     (rx_cfg_pol_inv_i = '1')
  -- HIGH if not inverting (rx_cfg_pol_inv_i = '0') by default default,  

  active_level                <= '0' when rx_cfg_pol_inv_i = '1' else
                                 '1';

  p_tx_BupDown_fsm: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        bupdown_cnt            <= c_BUPDOWN_CNT_ZERO;
        bup_o                  <= not active_level;
        bdown_o                <= not active_level;
        s_state                <= S_IDLE;
        pulse_width_cnt        <= (others => '0');
        gap_width_cnt          <= (others => '0');
      else

        case s_state is
          ---------------------------------------------------------------------
          when S_IDLE =>  --wait for first frame after start/reset or C0 signal
          ---------------------------------------------------------------------

            bupdown_cnt     <= c_BUPDOWN_CNT_ZERO;
            bup_o           <= not active_level;
            bdown_o         <= not active_level;

            if(rxed_B_valid = '1') then
             s_state         <= S_COMPARE;
            end if;
          ---------------------------------------------------------------------
          when S_COMPARE =>  -- compare received B value with local counter
          ---------------------------------------------------------------------

            if(rxed_B_valid = '0') then -- received no frame yet, or received C0
              s_state           <= S_IDLE; -- stay in this state
            else
              if(rxed_B_value >= bupdown_cnt+c_BUPDOWN_CNT_INC) then -- Bup
                bupdown_cnt     <= bupdown_cnt+c_BUPDOWN_CNT_INC;
                s_state         <= S_TX_PULSE;
                bup_o           <=     active_level;
                bdown_o         <= not active_level;
                pulse_width_cnt <= to_unsigned(g_CLOCK_PERIOD_NS, 32);
              elsif(rxed_B_value <= bupdown_cnt-c_BUPDOWN_CNT_INC) then -- Bdown
                bupdown_cnt     <= bupdown_cnt-c_BUPDOWN_CNT_INC;
                s_state         <= S_TX_PULSE;
                bup_o           <= not active_level;
                bdown_o         <=     active_level;
                pulse_width_cnt <= to_unsigned(g_CLOCK_PERIOD_NS, 32);
              end if;
            end if;

          ---------------------------------------------------------------------
          when S_TX_PULSE =>  -- produce Up or Down pulse of proper width
          ---------------------------------------------------------------------

            if(pulse_width_cnt < to_unsigned(g_PULSE_WIDTH_NS, 32)) then
              pulse_width_cnt   <= pulse_width_cnt + to_unsigned(g_CLOCK_PERIOD_NS, 32);
            else
              s_state           <= S_TX_GAP;
              bup_o             <= not active_level;
              bdown_o           <= not active_level;
              gap_width_cnt     <= to_unsigned(3*g_CLOCK_PERIOD_NS, 32);
            end if;

          ---------------------------------------------------------------------
          when S_TX_GAP => -- produce minimum space between pulses
          ---------------------------------------------------------------------

            if(gap_width_cnt < to_unsigned(g_MIN_PULSE_GAP_NS, 32)) then
              gap_width_cnt     <= gap_width_cnt + to_unsigned(g_CLOCK_PERIOD_NS, 32);
            else
              if(rxed_B_valid = '0') then -- received no frame yet, or received C0
                s_state         <= S_IDLE; -- stay in this state
              else
                s_state         <= S_COMPARE;
              end if;
            end if;

          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------

            bup_o               <= not active_level;
            bdown_o             <= not active_level;
            s_state             <= S_IDLE;

          end case;
      end if;
    end if;

  end process;

end arch;