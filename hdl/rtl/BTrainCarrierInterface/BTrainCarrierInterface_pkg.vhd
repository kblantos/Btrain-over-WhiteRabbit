-------------------------------------------------------------------------------
-- Title      : BTrainCarrierInterface_pkg
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit
-------------------------------------------------------------------------------
-- File       : BTrainCarrierInterface_pkg.vhd
-- Author     : Maciej Lipinski
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2019-10-19
-------------------------------------------------------------------------------
-- Description:
-- Package for BTrainCarrierInterface_pkg
-------------------------------------------------------------------------------
--
-- Copyright (c) 2018 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.BTrainFrame_pkg.all;

package BTrainCarrierInterface_pkg is

  component BTrainCarrierInterface is
    port(
      clk_i                    : in std_logic;
      rst_n_i                  : in std_logic;

      -- rx interrupt
      rx_Irq_i                 : in     std_logic;

      --rx WB Master on
      rx_wb_clk_o              : out    std_logic;
      rx_wb_adr_o              : out    std_logic_vector(7 downto 0);
      rx_wb_dat_o              : out    std_logic_vector(7 downto 0);
      rx_wb_dat_i              : in     std_logic_vector(7 downto 0);
      rx_wb_cyc_o              : out    std_logic;
      rx_wb_stb_o              : out    std_logic;
      rx_wb_we_o               : out    std_logic;
      rx_wb_ack_i              : in     std_logic;
      rx_wb_err_i              : in     std_logic;
      rx_wb_rty_i              : in     std_logic;
      rx_wb_stall_i            : in     std_logic;

      -- tx WB Master
      tx_wb_clk_o                                 : out    std_logic;
      tx_wb_adr_o              : out    std_logic_vector(7 downto 0);
      tx_wb_dat_o              : out    std_logic_vector(7 downto 0);
      tx_wb_dat_i              : in     std_logic_vector(7 downto 0);
      tx_wb_cyc_o              : out    std_logic;
      tx_wb_stb_o              : out    std_logic;
      tx_wb_we_o               : out    std_logic;
      tx_wb_ack_i              : in     std_logic;
      tx_wb_err_i              : in     std_logic;
      tx_wb_rty_i              : in     std_logic;
      tx_wb_stall_i            : in     std_logic;

      -- Interface with BTrain FMC
      rx_PFieldsReadVector_i   : in std_logic_vector(c_PayloadMaxFields-1 downto 0);
      rx_Frame_typeID_i        : in std_logic_vector(c_type_ID_size-1 downto 0)  := c_ID_ALL;
      rx_FrameHeader_o         : out t_FrameHeader;
      rx_BFramePayloads_o      : out t_BFramePayload;
      rx_IFramePayloads_o      : out t_IFramePayload;
      rx_CFramePayloads_o      : out t_CFramePayload;
      rx_Frame_valid_p1_o      : out std_logic;
      rx_Frame_typeID_o        : out std_logic_vector(c_type_ID_size-1 downto 0);

      tx_ready_o               : out std_logic;
      tx_PFieldsUpdateVector_i : in  std_logic_vector(c_PayloadMaxFields-1  downto 0);
      tx_TransmitFrame_p1_i    : in std_logic := '0';
      tx_FrameHeader_i         : in t_FrameHeader   := c_FrameHeader_zero;
      tx_BFramePayloads_i      : in t_BFramePayload := c_BFramePayload_zero;
      tx_IFramePayloads_i      : in t_IFramePayload := c_IFramePayload_zero;
      tx_CFramePayloads_i      : in t_CFramePayload := c_CFramePayload_zero;

      dbg_fms_rx_o             : out std_logic_vector(3 downto 0);
      dbg_fms_tx_o             : out std_logic_vector(3 downto 0)
    );
  end component;


end BTrainCarrierInterface_pkg;

package body BTrainCarrierInterface_pkg is


end BTrainCarrierInterface_pkg;
