library ieee;

use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity clock_output is
  port (
    -- clocks (same as the desired output frequency) coming from the PLL
    -- primitive, phase-shifted by 180 degrees.
    clk_0_i   : in std_logic;
    clk_180_i : in std_logic;

    clk_out_o   : out std_logic;
    clk_out_p_o : out std_logic;
    clk_out_n_o : out std_logic
    );
end clock_output;

architecture rtl of clock_output is

signal ddr_out : std_logic;
begin

  -- U_DDR_Output: ODDR2
    -- generic map (
      -- DDR_ALIGNMENT => "NONE",
      -- INIT          => '0',
      -- SRTYPE        => "ASYNC" )
    -- port map (
      -- Q  => ddr_out,
      -- C0 => clk_0_i,
      -- C1 => clk_180_i,
      -- D0 => '1',
      -- D1 => '0',
      -- CE => '1',
      -- R=>'0',
      -- S=>'0'
      -- );

  U_Output_Buffer: OBUFDS
    generic map (
      IOSTANDARD  => "LVDS_33")
    port map (
      O  => clk_out_p_o,
      OB => clk_out_n_o,
      I  => ddr_out);

  clk_out_o <= ddr_out;
  
  ----------------------------------------------------------------------------
  -- using template from "Language Template" :
  -- VHDL -> Synthesis Constructs -> Coding Examples -> Misc -> Output Clock Forwarding Using DDR
  ----------------------------------------------------------------------------
  --  A common method for supplying an external clock from the FPGA to drive
  --  other devices on the PCB board is to use clock forwarding via a double
  --  data-rate register.  This provides an external clock with a relatively
  --  small offset delay and does not consume any additional DLL/DCM/PLL/MMCM, clock
  --  buffers or input pins.  The basic technique is to supply the input clock
  --  to an output DDR register where one value is tied to a logic 0 and the
  --  other is tied to a logic 1.  A clock can be made with the same phase
  --  relationship (plus the added offset delay) or 180 degrees out of phase by
  --  changing the 1 and 0 values to the inputs to the DDR register.  Set SRTYPE
  --  to "SYNC" to avoid possible glitches on the clock if the set/reset signals
  --  are used.  For FPGA architectures which use two separate clocks into the
  --  DDR register, you may use a simple inversion of duty-cycle is not important
  --  however for output clocks that you wish to retain the duty-cycle as much as
  --  possible, it is suggested to supply a 0 degree and 180 degree clock from a
  --  DLL/DCM/PLL/MMCM to the input clocks to the outpuit DDR component.
   ODDR_inst : ODDR
   generic map(
      DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "ASYNC") -- Reset Type ("ASYNC" or "SYNC")
   port map (
      Q  => ddr_out,   -- 1-bit DDR output
      C  => clk_0_i,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input
      D1 => '1',  -- 1-bit data input (positive edge)
      D2 => '0',  -- 1-bit data input (negative edge)
      R  => '0',    -- 1-bit reset input
      S  => '0'     -- 1-bit set input
   );
  
end rtl;

