-------------------------------------------------------------------------------
-- Title      : BTrainFrameTransceiver
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFrameTransceiver_pkg.vhd
-- Author     : Daniel Oberson
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2013-09-30
-------------------------------------------------------------------------------
-- Description:
--
-- This package defines record types, constants and module declarations that are
-- - used by WRBTrain.vhd
-- - are required and should be used in any BTrain design that uses WR for 
--   transmission
-- 
-- Importantly, this package defines 
-- - records that represent:
--   * BTrain frame header
--   * BTrain B-Frame payload
--   * BTrain I-frame payload
--   * BTrain P-frame payload
-- - constants that represent enumerate BTrain frame types
-- - constant that determines the widht of streamer's payload - common to all 
--   WRBTrain receivers/transmitters
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016-2017 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.wr_fabric_pkg.all;
use work.wishbone_pkg.all;
use work.BTrain_wbgen2_pkg.all;
use work.streamers_pkg.all;
use work.BTrainFrame_pkg.all;

package BTrainFrameTransceiver_pkg is

  constant c_tx_streamer_params_btrain: t_tx_streamer_params :=(
      data_width            => c_BTrain_streamer_data_width,
      buffer_size           => 4,
      threshold             => 2,
      max_words_per_frame   => 4,
      timeout               => 1024,
      use_ref_clk_for_data  => 0,
      escape_code_disable   => FALSE);

  constant c_rx_streamer_params_btrain: t_rx_streamer_params :=(
      data_width            => c_BTrain_streamer_data_width,
      buffer_size           => 4,
      escape_code_disable   => FALSE,
      use_ref_clk_for_data  => 0,
      expected_words_number => 0);

  -- Identical to default streamers config
  constant c_rx_streamer_cfg_btrain: t_rx_streamer_cfg :=(
    mac_local              => x"000000000000",
    mac_remote             => x"000000000000",
    ethertype              => x"dbff",
    accept_broadcasts      => '1',
    filter_remote          => '0',
    fixed_latency          => x"0000000",
    fixed_latency_timeout  => x"1000000",
    sw_reset               => '0');

  -- enable Priority tag (VID=0) with priority 4
  constant c_tx_streamer_cfg_btrain: t_tx_streamer_cfg :=(
    mac_local              => x"000000000000",
    mac_target             => x"ffffffffffff",
    ethertype              => x"dbff",
    qtag_ena               => '1',
    qtag_vid               => x"000",
    qtag_prio              =>  "110", --0x6
    sw_reset               => '0');


  component BTrainFrameTransceiver is
    generic (
      g_rx_BframeType          : std_logic_vector(c_type_ID_size-1 downto 0) := c_ID_ALL;
      --------------generic that have effect when wb_config is used---------------
      g_slave_mode             : t_wishbone_interface_mode      := CLASSIC;
      g_slave_granularity      : t_wishbone_address_granularity := BYTE;
      --------------generic that have effect when wb_config is not used-----------
      g_rx_valid_pol_inv       : std_logic                      := '0';         -- HIGH=valid
      g_tx_period_value        : std_logic_vector(31 downto 0)  := x"00000000"; -- no transmit
      g_rx_out_data_time_valid : std_logic_vector(15 downto 0)  := x"0001";     -- 1-cycle
      g_rx_out_data_time_delay : std_logic_vector(15 downto 0)  := x"0000";     -- no delay
      g_use_wb_config          : boolean                        := true;
      g_clk_rate               : integer                        := 62500000
      );
    port(
      clk_i               : in std_logic;
      rst_n_i             : in std_logic;

      ----------------------------------------------------------------
      -- Interface with wr_transmission
      ----------------------------------------------------------------
      -- tx
      tx_data_o           : out std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
      tx_valid_o          : out std_logic;
      tx_dreq_i           : in std_logic;
      tx_last_p1_o        : out std_logic;
      tx_flush_p1_o       : out std_logic;
      -- rx
      rx_data_i           : in std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
      rx_valid_i          : in std_logic;
      rx_first_p1_i       : in std_logic;
      rx_dreq_o           : out std_logic;
      rx_last_p1_i        : in std_logic;
      ----------------------------------------------------------------
      -- Interface with BTrain FMC
      ----------------------------------------------------------------
      --received from WR network:
      rx_FrameHeader_o       : out t_FrameHeader;
      rx_BFramePayloads_o    : out t_BFramePayload;
      rx_IFramePayloads_o    : out t_IFramePayload;
      rx_PFramePayloads_o    : out t_PFramePayload; -- deprecated
      rx_CFramePayloads_o    : out t_CFramePayload;
      rx_Frame_valid_pX_o    : out std_logic;
      rx_Frame_typeID_o      : out std_logic_vector(c_type_ID_size-1 downto 0);
      rx_cfg_pol_inv_o       : out std_logic;

      -- transmit to wr network:
      ready_o                : out std_logic;
      tx_TransmitFrame_p1_i  : in std_logic := '0';
      tx_FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
      tx_BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
      tx_IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
      tx_PFramePayloads_i    : in t_PFramePayload := c_PFramePayload_zero; -- deprecated
      tx_CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;
      ----------------------------------------------------------------
      -- Wishbone interface
      ----------------------------------------------------------------
      wb_slave_i               : in  t_wishbone_slave_in := cc_dummy_slave_in;
      wb_slave_o               : out t_wishbone_slave_out

    );
  end component;

  component BTrainFrameTxCtrl is
    port(
      clk_i               : in std_logic;
      rst_n_i             : in std_logic;

      ctrl_send_i         : in std_logic;
      ctrl_dbg_mode_i     : in std_logic_vector(1 downto 0);
      ctrl_dbg_f_type_i   : in std_logic_vector(3 downto 0):= (others =>'0');

      ready_o             : out std_logic;
      FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
      BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
      IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
      CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;

      tx_sent_p1_o        : out std_logic;
      tx_data_o           : out std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
      tx_valid_o          : out std_logic;
      tx_dreq_i           : in std_logic;
      tx_last_p1_o        : out std_logic := '0';
      tx_flush_p1_o       : out std_logic := '0';
      wbregs_i            : in t_BTrain_out_registers := c_BTrain_out_registers_init_value;
      wbregs_o            : out t_BTrain_in_registers
      );
  end component;

  component BTrainFrameRxCtrl is
    generic (
      g_rx_BframeType  : std_logic_vector(c_type_ID_size-1 downto 0) := c_ID_ALL
    );
    port(
      clk_i            : in std_logic; 
      rst_n_i          : in std_logic;

      rx_data_i        : in std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
      rx_valid_i       : in std_logic;
      rx_first_p1_i    : in std_logic;
      rx_last_p1_i     : in std_logic;

      FrameHeader_o    : out t_FrameHeader;
      BFramePayloads_o : out t_BFramePayload;
      IFramePayloads_o : out t_IFramePayload;
      CFramePayloads_o : out t_CFramePayload;
      rxframe_valid_p1_o: out std_logic;
      rxframe_type_o   : out std_logic_vector(c_type_ID_size-1 downto 0)
    );
  end component;

  component BTrain_wb is
    port (
      rst_n_i                                  : in     std_logic;
      clk_sys_i                                : in     std_logic;
      wb_adr_i                                 : in     std_logic_vector(3 downto 0);
      wb_dat_i                                 : in     std_logic_vector(31 downto 0);
      wb_dat_o                                 : out    std_logic_vector(31 downto 0);
      wb_cyc_i                                 : in     std_logic;
      wb_sel_i                                 : in     std_logic_vector(3 downto 0);
      wb_stb_i                                 : in     std_logic;
      wb_we_i                                  : in     std_logic;
      wb_ack_o                                 : out    std_logic;
      wb_err_o                                 : out    std_logic;
      wb_rty_o                                 : out    std_logic;
      wb_stall_o                               : out    std_logic;
      regs_i                                   : in     t_BTrain_in_registers;
      regs_o                                   : out    t_BTrain_out_registers
    );
  end component ;

  component BvalueSimGen is
    port(
      clk_i               : in std_logic;
      rst_n_i             : in std_logic;

      frame_sent_i        : in std_logic; -- increment counter
      simGen_ena_i        : in std_logic;

      wbregs_i            : in t_BTrain_out_registers;
      wbregs_o            : out t_BTrain_in_registers;
      simValue_o          : out  std_logic_vector(31 downto 0)
      );
  end component;

end BTrainFrameTransceiver_pkg;

package body BTrainFrameTransceiver_pkg is

end BTrainFrameTransceiver_pkg;
