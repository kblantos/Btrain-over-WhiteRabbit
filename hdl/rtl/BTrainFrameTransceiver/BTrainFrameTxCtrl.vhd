-------------------------------------------------------------------------------
-- Title      : BTrain Frame Transmission Control
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFrameTxCtrl.vhd
-- Author     : Maciej Lipinski <maciej.lipinski@cern.ch>
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2016-05-31
-------------------------------------------------------------------------------
-- Description: 
-- 
-- This module takes BTrain-specific data (i.e. BTrain frame header, B frame and 
-- I frame payload), packs it in an array of bits and sends it over general-purpose 
-- WR streamers. 
-- 
-- The 32-bit values are arranged in the straemer payload according to big endian
-- 
-- It has also a "debugging" mode in which it sends, as content of the frame, 
-- values of counters that are incremented at each transmission
--
-- The interface with WR streamers is a generic-purpose WR Straemer interface
-- The interface with the BTrain modules are records reflecting content of 
-- BTrain frame header and different types of payloads.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016 CERN/BE-CO-HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;

use work.BTrainFrameTransceiver_pkg.all;
use work.BTrain_wbgen2_pkg.all;
use work.BTrainFrame_pkg.all;

entity BTrainFrameTxCtrl is
  generic (
    -- minimum allowed period between subsequent requests to send stuff
    -- this is to avoid the streamers/wrpc/network from being overwhelmed 
    -- by the application
    g_max_tx_period     : integer:=50 -- 800ns which gives 1.25MHz
          );
  port(
    clk_i               : in std_logic;
    rst_n_i             : in std_logic;

    ctrl_send_i         : in std_logic;
    ctrl_dbg_mode_i     : in std_logic_vector(1 downto 0);
    ctrl_dbg_f_type_i   : in std_logic_vector(3 downto 0):= (others =>'0');

    -- interface to BTrain
    ready_o             : out std_logic; -- if ignored, any tx request is gracefully ignored
    FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
    BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
    IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
    CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;

    -- interface to WR streamers
    tx_sent_p1_o        : out std_logic; --ML not usefull?
    tx_data_o           : out std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
    tx_valid_o          : out std_logic;
    tx_dreq_i           : in std_logic;
    tx_last_p1_o        : out std_logic := '0';
    tx_flush_p1_o       : out std_logic := '0';
    wbregs_i            : in t_BTrain_out_registers := c_BTrain_out_registers_init_value;
    wbregs_o            : out t_BTrain_in_registers
    );
end BTrainFrameTxCtrl;

architecture Behavioral of BTrainFrameTxCtrl is

  type   t_tx_fms       is (S_WAIT_TX_REQ, S_WAIT_TX_RDY, S_TX_PAYLOAD, S_TX_FINISH,
                            S_INTER_TX_GAP);
  constant c_str_words_max  : integer := 2;
  constant c_tx_period_range: unsigned(31 downto 0) := (others =>'1');
  signal s_tx_state     : t_tx_fms;
  signal FrameHeader    : t_FrameHeader;
  signal Btype_payload  : t_BFramePayload;
  signal Itype_payload  : t_IFramePayload;
  signal Ctype_payload  : t_CFramePayload;
  signal dbg_counter    : std_logic_vector(24-1 downto 0);
  signal tx_period_cnt  : unsigned(31 downto 0);
  signal tx_sent_p1     : std_logic;
  signal simValue       : std_logic_vector(31 downto 0);
  signal simEnabled     : std_logic;
  signal frame_type_d   : std_logic_vector(7 downto 0);
  signal frame_type     : std_logic_vector(7 downto 0);
  signal header_d       : std_logic_vector(c_header_size -1 downto 0);
  signal payload_d      : std_logic_vector(c_payload_size-1 downto 0);
  signal ctrl_dbg_mode  : std_logic_vector(1 downto 0);
  signal ctrl_dbg_f_type: std_logic_vector(3 downto 0);

begin
  -------------------------------------------------------------------------------------------
  --                       choose the payload of the Btrain frame
  --The content depends on the debugging mode:
  -- 0: debugging disabled (by default)
  -- 1: use counter that is incremented by each transmission. see the function for details
  --    synB  := x"50" & dbg_counter; 
  --    simB  := x"40" & dbg_counter;
  --    measB := x"30" & dbg_counter;
  --    oldB  := x"20" & dbg_counter;
  --    Bdot  := x"10" & dbg_counter;
  --    B     := x"00" & dbg_counter;
  -- 2: use a dedicated BvalueSimGen module that can provide configurable waveforms
  --    synB  := x"01234567"; 
  --    simB  := x"DEADBEEF";
  --    measB := x"00000003";
  --    oldB  := x"00000002";
  --    Bdot  := x"00000001";
  --    B     := simValue; 
  -- 3: use constant dummy values, see the function for details
  --    B     => x"deafbeef",
  --    Bdot  => x"cafecafe",
  --    oldB  => x"87654321",
  --    measB => x"deedbeaf",
  --    simB  => x"bad0babe",
  --    synB  => x"00000000"
  -------------------------------------------------------------------------------------------

  p_dbg_config_reg: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0') then
        ctrl_dbg_f_type  <= (others=>'0');
        ctrl_dbg_mode    <= (others => '0');
      else
        ctrl_dbg_f_type <= ctrl_dbg_f_type_i;
        ctrl_dbg_mode   <= ctrl_dbg_mode_i;
      end if;
    end if;
  end process;

  --debug or not (three debugging modes)
  Btype_payload <= f_B_frame_dbg_cnt(dbg_counter) when ctrl_dbg_mode="01" else
                   f_B_frame_dbg_sim(simValue)    when ctrl_dbg_mode="10" else
                   c_BFramePayload_dummy          when ctrl_dbg_mode="11" else
                   BFramePayloads_i;

  Itype_payload <= f_I_frame_dbg_cnt(dbg_counter) when ctrl_dbg_mode="01" else
                   f_I_frame_dbg_sim(simValue)    when ctrl_dbg_mode="10" else
                   c_IFramePayload_dummy          when ctrl_dbg_mode="11" else
                   IFramePayloads_i;

  Ctype_payload <= f_C_frame_dbg_cnt(dbg_counter) when ctrl_dbg_mode="01" else
                   f_C_frame_dbg_sim(simValue)    when ctrl_dbg_mode="10" else
                   c_CFramePayload_dummy          when ctrl_dbg_mode="11" else
                   CFramePayloads_i;

  FrameHeader   <= FrameHeader_i                  when ctrl_dbg_mode  ="00" else
                   f_header_dbg(c_ID_BkFrame)     when ctrl_dbg_f_type=x"1" else
                   f_header_dbg(c_ID_ImFrame)     when ctrl_dbg_f_type=x"2" else
                   f_header_dbg(c_ID_CdFrame)     when ctrl_dbg_f_type=x"3" else
                   c_FrameHeader_dummy;

  frame_type    <= FrameHeader.frame_type;


  -------------------------------------------------------------------------------------------
  -- increment debugging counter
  -------------------------------------------------------------------------------------------
  p_counters: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0') then
        dbg_counter     <= (others=>'0');
        tx_period_cnt   <= (others => '0');
      else

        -------------------------------------------------------------------------------------
        -- debug counter, it gives value to the BTrain values sent in debug mode
        -------------------------------------------------------------------------------------
        if(s_tx_state = S_TX_FINISH) then
          dbg_counter       <= std_logic_vector(signed(dbg_counter) + 1);
        end if;

        -------------------------------------------------------------------------------------
        -- counter that is used to evaluate the minimam period of transmission.
        -- if the application tries to send data more often, such request will be ignored
        -------------------------------------------------------------------------------------
        if(s_tx_state = S_WAIT_TX_REQ) then
          tx_period_cnt <= (others => '0');
        elsif(tx_period_cnt /= c_tx_period_range) then -- stop counting when out-of-range
          tx_period_cnt <= tx_period_cnt + 1;
        end if;

      end if;
    end if;
  end process;

  -------------------------------------------------------------------------------------------
  -- assemble and send BTrain frame
  -------------------------------------------------------------------------------------------
  tx_data_o(c_header_size               -1 downto 0)             <= header_d;
  tx_data_o(c_payload_size+c_header_size-1 downto c_header_size) <= payload_d;

  -- read or not to accept data (this output signal is new, some applications may ingore
  -- this output signal)
  ready_o <= '1' when (tx_dreq_i = '1' and s_tx_state = S_WAIT_TX_REQ) else
             '0';

  -- state machine that receives the streamer word(s) and translates into BTrain content
  p_tx_fsm: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0') then
        tx_sent_p1      <= '0';
        tx_valid_o      <= '0';
        tx_flush_p1_o   <= '0';
        tx_last_p1_o    <= '0';
        s_tx_state      <= S_WAIT_TX_REQ;
        frame_type_d    <= (others =>'0');
        header_d        <= (others =>'0');
        payload_d       <= (others =>'0');
      else

        case s_tx_state is 
          ---------------------------------------------------------------------
          when S_WAIT_TX_REQ =>  -- wait for request to send data
          ---------------------------------------------------------------------

            if(ctrl_send_i ='1' and tx_dreq_i = '1') then

              -- copy the data to the output
              case frame_type is
                when c_ID_BkFrame =>
                  header_d     <= f_serialize_header(FrameHeader);
                  payload_d    <= f_bigEndianess(f_serialize_Btype_payload(Btype_payload));
                  s_tx_state   <= S_TX_PAYLOAD;
                when c_ID_ImFrame =>
                  header_d     <= f_serialize_header(FrameHeader);
                  payload_d    <= f_bigEndianess(f_serialize_Itype_payload(Itype_payload));
                  s_tx_state   <= S_TX_PAYLOAD;
                when c_ID_CdFrame =>
                  header_d     <= f_serialize_header(FrameHeader);
                  payload_d    <= f_bigEndianess(f_serialize_Ctype_payload(Ctype_payload));
                  s_tx_state   <= S_TX_PAYLOAD;
                when others =>
                  header_d     <= (others =>'0');
                  payload_d    <= (others =>'0');
                  s_tx_state   <= S_WAIT_TX_REQ;
              end case;

              frame_type_d <= frame_type;

            end if;

            tx_valid_o      <= '0';
            tx_sent_p1      <= '0';
            tx_last_p1_o    <= '0';
            tx_flush_p1_o   <= '0';
          ---------------------------------------------------------------------
          when S_WAIT_TX_RDY => -- if request happens when not ready, wait
          ---------------------------------------------------------------------

            if (tx_dreq_i = '1') then -- wait endlessly to to finish sending
              tx_valid_o      <= '1';
              tx_sent_p1      <= '0';
              tx_flush_p1_o   <= '0';
              s_tx_state      <= S_TX_FINISH;
              tx_last_p1_o    <= '1';
            end if;

          ---------------------------------------------------------------------
          when S_TX_PAYLOAD =>  -- wait for request to send data
          ---------------------------------------------------------------------

            if(tx_dreq_i = '1' ) then
              tx_valid_o    <= '1';
              tx_sent_p1    <= '0';
              tx_flush_p1_o <= '0';
              s_tx_state    <= S_TX_FINISH;
              tx_last_p1_o  <= '1';
            elsif(tx_dreq_i = '0' ) then
              tx_valid_o    <= '0';
              tx_sent_p1    <= '0';
              tx_last_p1_o  <= '0';
              tx_flush_p1_o <= '0';
              s_tx_state    <= S_WAIT_TX_RDY;
            end if;

          ---------------------------------------------------------------------
          when  S_TX_FINISH => -- request sending (flush)
          ---------------------------------------------------------------------

            tx_valid_o      <= '0';
            tx_sent_p1      <= '1';
            tx_last_p1_o    <= '0';
            tx_flush_p1_o   <= '1';
            s_tx_state      <= S_INTER_TX_GAP;
          ---------------------------------------------------------------------
          when S_INTER_TX_GAP => -- wait for the data to be sent before new req
          ---------------------------------------------------------------------

            tx_valid_o      <= '0';
            tx_sent_p1      <= '0';
            tx_last_p1_o    <= '0';
            tx_flush_p1_o   <= '0';
            if(tx_period_cnt >= to_unsigned(g_max_tx_period-3,tx_period_cnt'length)) then
              s_tx_state    <= S_WAIT_TX_REQ;
            end if;

          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------
            s_tx_state      <= S_WAIT_TX_REQ;

        end case; 
      end if;
    end if;
  end process;

  tx_sent_p1_o <= tx_sent_p1;
  simEnabled   <= '1' when ctrl_dbg_mode_i="10" else '0';

  -------------------------------------------------------------------------------------------
  -- a module that can be programmed via WB to provide "dummy" BTrain waveforms.
  -------------------------------------------------------------------------------------------
  cmp_simBvalGEN: BvalueSimGen
    port map(
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      frame_sent_i   => tx_sent_p1,
      simGen_ena_i   => simEnabled,
      wbregs_i       => wbregs_i,
      wbregs_o       => wbregs_o,
      simValue_o     => simValue
    );

end Behavioral;

