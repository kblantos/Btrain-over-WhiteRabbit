-------------------------------------------------------------------------------
-- Title      : BTrain Frame Reception Control
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFrameRxCtrl.vhd
-- Author     : Maciej Lipinski <maciej.lipinski@cern.ch>
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2016-05-31
-------------------------------------------------------------------------------
-- Description:
--
-- This module receives an array of bits from the WR rx streamer (Streamer payload),
-- it decodes the bits into BTrain frame header and B/I/C frame payload,
-- according to the specification, see:
-- https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/wr-btrain-frame-format
-- 
-- The interface with WR streamers is a generic-purpose WR Straemer interface
-- The interface with the BTrain modules are records reflecting content of 
-- BTrain frame header and different types of payloads.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016 CERN/BE-CO-HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;

entity BTrainFrameRxCtrl is
  generic (
    -- This generic can be set to c_ID_ALL, c_ID_BkFrame, or c_ID_ImFrame (currently 3 value)
    -- The value is used to decide which type of BTrain frames should be received. If set to:
    -- c_ID_ALL     : all types of frames are received
    -- c_ID_BkFrame : only the B type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    -- c_ID_ImFrame : only the I type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    -- c_ID_CdFrame:  only the C type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    g_rx_BframeType  : std_logic_vector(c_type_ID_size-1 downto 0) := c_ID_ALL
  );
  port(
    clk_i            : in std_logic; 
    rst_n_i          : in std_logic;

    -- interface to WR streamers
    rx_data_i        : in std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
    rx_valid_i       : in std_logic;
    rx_first_p1_i    : in std_logic;
    rx_last_p1_i     : in std_logic;

    -- interface to BTrain
    FrameHeader_o    : out t_FrameHeader;
    BFramePayloads_o : out t_BFramePayload;
    IFramePayloads_o : out t_IFramePayload;
    CFramePayloads_o : out t_CFramePayload;

    rxframe_valid_p1_o: out std_logic;
    rxframe_type_o   : out std_logic_vector(c_type_ID_size-1 downto 0)
  );
end BTrainFrameRxCtrl;

architecture Behavioral of BTrainFrameRxCtrl is
  signal header               : std_logic_vector(c_header_size -1 downto 0);
  signal payload              : std_logic_vector(c_payload_size-1 downto 0);
  signal header_d             : std_logic_vector(c_header_size -1 downto 0);
  signal payload_d            : std_logic_vector(c_payload_size-1 downto 0);
  signal frame_type           : std_logic_vector(c_type_ID_size-1 downto 0);
  signal frame_type_d         : std_logic_vector(c_type_ID_size-1 downto 0);
begin

  -- divide the received BTrain frame into header and payload, extracte frame type from the
  -- header. These signals are used in the state machin
  header           <= rx_data_i(c_header_size               -1 downto 0);
  payload          <= rx_data_i(c_header_size+c_payload_size-1 downto c_header_size);
  frame_type       <= f_deserialize_header(header).frame_type when (rx_first_p1_i = '1' and rx_valid_i='1') else
                      frame_type_d;

  process(clk_i)
    begin
      if rising_edge(clk_i) then 
         if (rst_n_i='0') then
           rxframe_valid_p1_o     <= '0';
           frame_type_d           <= (others => '0');
           header_d               <= (others => '0');
           payload_d              <= (others => '0');
         else
           -- check acceptance criteria of incoming valid data. Data is accepted in two cases:
           -- 1) module is configured to accepted of frame IDs, or
           -- 2) module is configured to accept a particular frame time and this is what is
           --    receoved
           if(rx_valid_i='1' and (g_rx_BframeType=c_ID_ALL or g_rx_BframeType=frame_type)) then

              -- If this is the first word, remember frame type and based on the type
              -- (in the future, frames with more word can be defined... this will be
              --  the place to recognize how many words needs to be received)
              if(rx_first_p1_i = '1') then
                frame_type_d <= frame_type;
                header_d     <= header; -- save the header for any frame as it is the
                                        -- same for all BTrain frame payloads
              end if;

              -- For the frame types defined so far, there is only one word
              -- (which is fist and last). If we received proper type, i.e. known
              -- to this implementation/version, validate the received data, 
              -- otherwise we ignore the frame (in the future,
              -- some other frames might be received, so be future-proof
              if (rx_last_p1_i = '1' and rx_first_p1_i = '1' and
                 (frame_type = c_ID_BkFrame or 
                  frame_type = c_ID_ImFrame or 
                  frame_type = c_ID_CdFrame)) then
                -- save the payload only if you now its format
                payload_d          <= payload;
                rxframe_valid_p1_o <= '1';
              else
                rxframe_valid_p1_o <= '0';
              end if;
            else -- no valid word, clear outputs and counts
              rxframe_valid_p1_o <= '0';
              header_d           <= (others => '0');
              payload_d          <= (others => '0');
            end if;
         end if;
       end if;
  end process;

  -- decode received data into appropriate format
  BFramePayloads_o <= f_deserialize_Btype_payload(f_bigEndianess(payload_d))
                      when (frame_type_d=c_ID_BkFrame) else c_BFramePayload_zero;
  IFramePayloads_o <= f_deserialize_Itype_payload(f_bigEndianess(payload_d)) 
                      when (frame_type_d=c_ID_ImFrame) else c_IFramePayload_zero ;
  CFramePayloads_o <= f_deserialize_Ctype_payload(f_bigEndianess(payload_d)) 
                      when (frame_type_d=c_ID_CdFrame) else c_CFramePayload_zero;
  FrameHeader_o    <= f_deserialize_header(header_d);
  rxframe_type_o   <= frame_type_d;

end Behavioral;