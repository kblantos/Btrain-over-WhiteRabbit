-------------------------------------------------------------------------------
-- Title      : BTrainFrameTransceiver
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFrameTransceiver.vhd
-- Author     : Daniel Oberson
-- Company    : CERN
-- Created    : 2013-09-30
-- Platform   : FPGA-generics
-- Standard   : VHDL
----------------------------------------------------------------------------------
-- Description:
--
-- This module contains the logic/modules that provide interface between:
-- 1) General-purpose WR-Streamers provided in wr-cores, and
-- 2) BTrain-specific modules that implement BTrain logic (e.g. BTrainFMC)
-- 
-- It is intended to be re-used by all the WR-BTrain implementations (as an
-- IP core).
-- 
-- In particular, the submodules of this module are responsible for
-- @tx: construction of different types of BTrain frames (e.g. B-frame, 
--       I-frame) according to the specification, see
-- @rx: interpretation of the BTrain frames and interpretiation of their
--      fields into BTrain-specific values
--
-- see https://wikis.cern.ch/display/HT/WR+BTrain+New+Frame+Format
--
-- It has the following main components
-- 1) rxCtrlBTrain  - receives Streamer's payload and interprets its fiels
--                    into BTrain-specific values
-- 2) txCtrlBTrain  - assembles Streamer's payload according to the specified
--                    frame type
-- 3) SynchroGen    - triggers transmission of Streamer frames at a rate
--                    specified in WB configuration reg 
-- 4) WRBTrain_wb   - set of wishbone registers that provide control, status, 
--                    configuration and debugging of the above modules
--
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016-2017 CERN TE/MSC/MM+BE/CO/HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.BTrainFrameTransceiver_pkg.all;
use work.wishbone_pkg.all;
use IEEE.NUMERIC_STD.ALL; -- to_unsigned
use work.BTrain_wbgen2_pkg.all;
use work.BTrainFrame_pkg.all;

entity BTrainFrameTransceiver is
  generic (
    -- This generic can be set to c_ID_ALL, c_ID_BkFrame, c_ID_PFWFrame, or c_ID_ImFrame 
    -- (currently 4 value)
    -- The value is used to decide which type of BTrain frames should be received. If set to:
    -- c_ID_ALL     : all types of frames are received
    -- c_ID_BkFrame : only the B type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    -- c_ID_ImFrame : only the I type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    -- c_ID_CdFrame:  only the C type of BTrain frames is exposed to the at the output of
    --                this module, the other frames are ignored.
    g_rx_BframeType          : std_logic_vector(c_type_ID_size-1 downto 0) := c_ID_ALL;
    -----------------------------------------------------------------------------------------
    --------------------generic that have effect when wb_config is used-----------------------
    g_slave_mode             : t_wishbone_interface_mode      := CLASSIC;
    g_slave_granularity      : t_wishbone_address_granularity := BYTE;
    -------------------default configuration of transmission and reception -------------------
    ---------------(the same configuration is available through WB otherwise)-----------------
    -- Invert RX valid polarity (valid_pol_inv)
    --   0: normal polarity (active high)
    --   1: inverted polarity (active low)
    g_rx_valid_pol_inv       : std_logic                      := '0';         -- HIGH=valid
    -- Transmission period (tx_period_value)
    --   Period in clock cylces (16ns). When the value is not zero, frames are sent with
    --   that period.
    g_tx_period_value        : std_logic_vector(31 downto 0)  := x"00000000"; -- 0x0 :do not tx
    -- Output data valid period (out_rx_data_time_valid)
    --    Period in clock cylces (16ns) during which data is valid. By default it is 1 cycle 
    --    strobe. Two special values
    --    * 0x0000: output data disabled
    --    * 0xFFFF: output data continuously valid until next update 
    g_rx_out_data_time_valid : std_logic_vector(15 downto 0)  := x"0001";     -- 1-cycle
    -- Output data delay (out_rx_data_time_delay)
    --    Delay in clock cylces (16ns) between receiving data and making it valid. By default
    --    it is 0 cycle
    g_rx_out_data_time_delay : std_logic_vector(15 downto 0)  := x"0000";     -- no delay;
    --------------- WB registers to override the default configuration values----------------
    -- in some WRBtran applications, there might be no WB-access to the configuration from 
    -- the host. In such case, the WB access is not needed and can be disabled with the
    -- generic below. In such case, only the default values (set by generics) are used.
    -- NOTE that WB access provides also access to debugging facilities which are not 
    -- accessible/configurable via generics. 
    -- The values of the below generic  g_use_wb_config have the following meaning:
    --  *true - configuration of WRBTrain can be accessed via Wishbone and can be used to override
    --          default values that are set via generics
    --  *false- the WB Slave is not instantiated, default values from generics are used
    g_use_wb_config          : boolean                        := true;
    -- rate of the clk_i in Hz. There are two options possible
    -- 1) g_clk_rate := 62500000 -> this one is default and probably what you want
    -- 2) g_clk_rate :=125000000 -> used with fixed-latency mode of the wr_streamers
    g_clk_rate               : integer                        := 62500000
    );
  port(
    clk_i               : in std_logic;
    rst_n_i             : in std_logic;

    ----------------------------------------------------------------
    -- Interface with wr_transmission
    ----------------------------------------------------------------
    -- tx
    tx_data_o           : out std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
    tx_valid_o          : out std_logic;
    tx_dreq_i           : in std_logic;
    tx_last_p1_o        : out std_logic;
    tx_flush_p1_o       : out std_logic;
    -- rx
    rx_data_i           : in std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
    rx_valid_i          : in std_logic;
    rx_first_p1_i       : in std_logic;
    rx_dreq_o           : out std_logic;
    rx_last_p1_i        : in std_logic;
    ----------------------------------------------------------------
    -- Interface with BTrain FMC
    ----------------------------------------------------------------
    --received from WR network:
    rx_FrameHeader_o       : out t_FrameHeader;
    rx_BFramePayloads_o    : out t_BFramePayload;
    rx_IFramePayloads_o    : out t_IFramePayload;
    rx_PFramePayloads_o    : out t_PFramePayload; -- depracated
    rx_CFramePayloads_o    : out t_CFramePayload;
    rx_Frame_valid_pX_o    : out std_logic;
    rx_Frame_typeID_o      : out std_logic_vector(c_type_ID_size-1 downto 0);
    rx_cfg_pol_inv_o       : out std_logic;
    -- transmit to wr network:
    ready_o                : out std_logic; -- if ignored, any tx request is gracefully ignored
    tx_TransmitFrame_p1_i  : in std_logic := '0';
    tx_FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
    tx_BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
    tx_IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
    tx_PFramePayloads_i    : in t_PFramePayload := c_PFramePayload_zero; -- depracated
    tx_CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;
    ----------------------------------------------------------------
    -- Wishbone interface (connect only if g_use_wb_config=true,
    -- otherwise you will run into problems
    ----------------------------------------------------------------
    wb_slave_i               : in  t_wishbone_slave_in := cc_dummy_slave_in;
    wb_slave_o               : out t_wishbone_slave_out

  );
end BTrainFrameTransceiver;

architecture Behavioral of BTrainFrameTransceiver is

  constant c_cnt_width     : integer := 26;
  constant c_reset_value   : unsigned(c_cnt_width-1 downto 0) := to_unsigned(1,c_cnt_width);

  signal send_BTrain_frame : std_logic;
  signal send_tick_p       : std_logic;
  signal wb_in_tx_ctrl     : t_BTrain_in_registers;
  signal wb_in             : t_BTrain_in_registers;
  signal wb_out            : t_BTrain_out_registers;
  signal rx_Frame_valid_p1 : std_logic;
  signal rx_FrameHeader    : t_FrameHeader;
  signal rx_BFramePayloads : t_BFramePayload;
  signal rx_IFramePayloads : t_IFramePayload;
  signal rx_CFramePayloads : t_CFramePayload;

  type   t_rx_fms       is (S_WAIT_RX_DATA, S_DELAY_OUTPUT, S_DATA_VALID, S_CONTINUOUS);
  signal s_out_state       : t_rx_fms;
  signal output_cnt        : unsigned(15 downto 0);
  signal rx_data_time_valid   : unsigned(15 downto 0);
  signal rx_data_time_valid_wb: unsigned(15 downto 0);
  signal rx_data_time_delay   : unsigned(15 downto 0);
  signal rx_data_time_delay_wb: unsigned(15 downto 0);

  signal rx_valid_pol_inv     : std_logic;
  signal tx_period_value      : std_logic_vector(31 downto 0);
  signal tx_period_value_wb   : std_logic_vector(31 downto 0);

  signal sample_rate_cnt    : unsigned(c_cnt_width-1 downto 0);

  signal tx_data          : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal tx_valid         : std_logic; 
begin
  rx_dreq_o <='1';
  cmp_rxCtrl : BTrainFrameRxCtrl 
    generic map(
      g_rx_BframeType     => g_rx_BframeType
    )
    port map(
      clk_i               => clk_i,
      rst_n_i             => rst_n_i,

      rx_data_i           => rx_data_i,
      rx_valid_i          => rx_valid_i,
      rx_first_p1_i       => rx_first_p1_i,
      rx_last_p1_i        => rx_last_p1_i,

      FrameHeader_o       => rx_FrameHeader,
      BFramePayloads_o    => rx_BFramePayloads,
      IFramePayloads_o    => rx_IFramePayloads,
      CFramePayloads_o    => rx_CFramePayloads,
      rxframe_valid_p1_o  => rx_Frame_valid_p1,
      rxframe_type_o      => rx_Frame_typeID_o
    );

  -------------------------------------------------------------------------------------------
  -- A simple counter that provides a single-clock tick every
  -- tx_period_value number of clock cycles. It is used to trigger periodically
  -- the transmission of BTrain frames
  p_tx_tick_gen: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0' or unsigned(tx_period_value(c_cnt_width-1 downto 0)) = 0) then
        sample_rate_cnt     <= c_reset_value;
        send_tick_p         <= '0';
      else
        if(sample_rate_cnt < unsigned(tx_period_value(c_cnt_width-1 downto 0))) then
          send_tick_p       <= '0';
          sample_rate_cnt   <= unsigned(sample_rate_cnt) + 1;
        else
          send_tick_p       <= '1';
          sample_rate_cnt   <= c_reset_value;
        end if;
      end if;
    end if;
  end process;

  send_btrain_frame <= '1' when (send_tick_p                   = '1' or
                                 wb_out.btrain_scr_tx_single_o = '1' or
                                 tx_TransmitFrame_p1_i         = '1') else
                       '0';

  cmp_txCtrl : BTrainFrameTxCtrl
    port map(
      clk_i               => clk_i,
      rst_n_i             => rst_n_i,

      ctrl_send_i         => send_btrain_frame,
      ctrl_dbg_mode_i     => wb_out.btrain_scr_tx_dbg_o,
      ctrl_dbg_f_type_i   => wb_out.btrain_scr_tx_dbg_ftype_o,

      ready_o             => ready_o,
      FrameHeader_i       => tx_FrameHeader_i,
      BFramePayloads_i    => tx_BFramePayloads_i,
      IFramePayloads_i    => tx_IFramePayloads_i,
      CFramePayloads_i    => tx_CFramePayloads_i,

      tx_sent_p1_o        => open,
      tx_data_o           => tx_data,
      tx_valid_o          => tx_valid,
      tx_dreq_i           => tx_dreq_i,
      tx_last_p1_o        => tx_last_p1_o,
      tx_flush_p1_o       => tx_flush_p1_o,
      wbregs_i            => wb_out,
      wbregs_o            => wb_in_tx_ctrl
      );

  tx_data_o  <= tx_data;
  tx_valid_o <= tx_valid;

  gen_wb_config: if(g_use_wb_config=true) generate
    signal wb_regs_slave_in  : t_wishbone_slave_in;
    signal wb_regs_slave_out : t_wishbone_slave_out;
  begin
    cmp_wb_adapter : wb_slave_adapter
      generic map (
        g_master_use_struct  => true,
        g_master_mode        => CLASSIC,
        g_master_granularity => WORD,
        g_slave_use_struct   => true,
        g_slave_mode         => g_slave_mode,
        g_slave_granularity  => g_slave_granularity)
      port map (
        clk_sys_i  => clk_i,
        rst_n_i    => rst_n_i,
        slave_i    => wb_slave_i,
        slave_o    => wb_slave_o,
        master_i   => wb_regs_slave_out,
        master_o   => wb_regs_slave_in);

    -- drive unused WB signals
    wb_regs_slave_out.rty <= '0';
    wb_regs_slave_out.err <= '0';

    cmp_wb_regs: BTrain_wb
      port map(
        rst_n_i            => rst_n_i,
        clk_sys_i          => clk_i,
        wb_adr_i           => wb_regs_slave_in.adr(3 downto 0),
        wb_dat_i           => wb_regs_slave_in.dat,
        wb_dat_o           => wb_regs_slave_out.dat,
        wb_cyc_i           => wb_regs_slave_in.cyc,
        wb_sel_i           => wb_regs_slave_in.sel(3 downto 0),
        wb_stb_i           => wb_regs_slave_in.stb,
        wb_we_i            => wb_regs_slave_in.we,
        wb_ack_o           => wb_regs_slave_out.ack,
        wb_stall_o         => wb_regs_slave_out.stall,
        regs_i             => wb_in,
        regs_o             => wb_out
      );
    wb_in.btrain_scr_dummy_i         <= x"CAFE";
    wb_in.btrain_bsim_scyc_len_val_i <= wb_in_tx_ctrl.btrain_bsim_scyc_len_val_i;
    wb_in.btrain_bsim_scyc_inc_val_i <= wb_in_tx_ctrl.btrain_bsim_scyc_inc_val_i;
    wb_in.btrain_simb_ctrl_cmaxlen_i <= wb_in_tx_ctrl.btrain_simb_ctrl_cmaxlen_i;

    gen_btrain_debug: if(c_BTrain_streamer_data_width > 47) generate
      -- this process allows to snoop the first value of the rx and tx BTrain Frames,
      -- regardless of the frame type.
      p_B_or_I_value: process(clk_i)
      begin
        if rising_edge(clk_i) then
          if rst_n_i = '0' then
            wb_in.btrain_dbg_rx_b_or_i_i <= (others =>'0');
            wb_in.btrain_dbg_tx_b_or_i_i <= (others =>'0');
          else
            if(rx_valid_i = '1') then
              wb_in.btrain_dbg_rx_b_or_i_i <= rx_data_i(15+16 downto 0+16) &
                                              rx_data_i(31+16 downto 16+16);
            end if;
            if(tx_valid = '1') then
              wb_in.btrain_dbg_tx_b_or_i_i <= tx_data(15+16 downto 0+16) &
                                              tx_data(31+16 downto 16+16);
            end if;
          end if;
        end if;
      end process;
    end generate gen_btrain_debug;
  end generate gen_wb_config; 
  gen_no_wb_config: if(g_use_wb_config=false) generate
    wb_out <= c_BTrain_out_registers_init_value;
  end generate gen_no_wb_config;

  tx_period_value_wb    <= wb_out.btrain_tx_period_value_o                  when wb_out.btrain_scr_tx_or_config_o = '1' else
                           g_tx_period_value;
  rx_valid_pol_inv   <= wb_out.btrain_scr_rx_valid_pol_inv_o             when wb_out.btrain_scr_rx_or_config_o = '1' else
                        g_rx_valid_pol_inv;
  rx_data_time_valid_wb <= unsigned(wb_out.btrain_rx_out_data_time_valid_o) when wb_out.btrain_scr_rx_or_config_o = '1' else
                           unsigned(g_rx_out_data_time_valid);
  rx_data_time_delay_wb <= unsigned(wb_out.btrain_rx_out_data_time_delay_o) when wb_out.btrain_scr_rx_or_config_o = '1' else
                           unsigned(g_rx_out_data_time_delay);

  rx_cfg_pol_inv_o   <= rx_valid_pol_inv;

  ------------------------------------------------------------------------------------------
  -- Include the clk_rate factor. In some designs, the clock used for this module is 125MHz.
  -- In most of the designs it is 62.5MHz. Thus, the unit used in software is 16ns, assuming
  -- clk_i is 62.5MHz. If the clk_i is 125MHz, multiply the input data from SW that indicate
  -- time period by 2.
  -- TODO: add WB register that indicates clk_rate. Modify SW to read it and act acrodingly.
  ------------------------------------------------------------------------------------------
  assert (g_clk_rate = 62500000 or g_clk_rate = 125000000)
    report "g_clk_rate must be either 62500000 or 125000000" severity error;

  gen_clk_62_5MHz: if (g_clk_rate = 62500000) generate
    tx_period_value     <= tx_period_value_wb;
    rx_data_time_valid  <= rx_data_time_valid_wb;
    rx_data_time_delay  <= rx_data_time_delay_wb;
  end generate gen_clk_62_5MHz;
  gen_clk_125MHz: if (g_clk_rate = 125000000) generate
    tx_period_value     <=    tx_period_value_wb(30 downto 0) & '0';
    rx_data_time_valid  <= rx_data_time_valid_wb(14 downto 0) & '0';
    rx_data_time_delay  <= rx_data_time_delay_wb(14 downto 0) & '0';
  end generate gen_clk_125MHz;
  -------------------------------------------------------------------------------------------
  --                           WR-BTrain reception                                         --
  -- output stage for the reception with configurable width of the valid pulse via WB reg  --
  -------------------------------------------------------------------------------------------

  p_rx_valid: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0' or rx_data_time_valid = 0) then
        output_cnt             <= (others => '0');
        rx_FrameHeader_o       <= c_FrameHeader_zero;
        rx_BFramePayloads_o    <= c_BFramePayload_zero;
        rx_IFramePayloads_o    <= c_IFramePayload_zero;
        rx_CFramePayloads_o    <= c_CFramePayload_zero;
        rx_Frame_valid_pX_o    <= rx_valid_pol_inv;
        s_out_state            <= S_WAIT_RX_DATA;
      else

        case s_out_state is 
          ---------------------------------------------------------------------
          when S_WAIT_RX_DATA =>  -- wait for data received by rx streamer
          ---------------------------------------------------------------------
           if(rx_Frame_valid_p1 = '1') then
                rx_FrameHeader_o       <= rx_FrameHeader;
                rx_BFramePayloads_o    <= rx_BFramePayloads;
                rx_IFramePayloads_o    <= rx_IFramePayloads;
                rx_CFramePayloads_o    <= rx_CFramePayloads;
              if(rx_data_time_delay = 0) then -- do not delay the output
                output_cnt             <= rx_data_time_valid;
                rx_Frame_valid_pX_o    <= not rx_valid_pol_inv;
                s_out_state            <= S_DATA_VALID;
              else                         -- delay the output
                output_cnt             <= rx_data_time_delay;
                rx_Frame_valid_pX_o    <= rx_valid_pol_inv;
                s_out_state            <= S_DELAY_OUTPUT;
              end if;
            end if;
          ---------------------------------------------------------------------
          when S_DELAY_OUTPUT =>  -- delay the output of valid data
          ---------------------------------------------------------------------
            if(output_cnt = 1) then
              output_cnt               <= rx_data_time_valid;
              rx_Frame_valid_pX_o      <= not rx_valid_pol_inv;
              s_out_state              <= S_DATA_VALID;
            else
              output_cnt               <= output_cnt - 1;
            end if;
          ---------------------------------------------------------------------
          when S_DATA_VALID =>  -- output valid data
          ---------------------------------------------------------------------
            if(rx_data_time_valid = x"FFFF") then -- continuous mode, so keep the data until new arrives
              output_cnt               <= (others => '0');
              s_out_state              <= S_WAIT_RX_DATA;
            elsif(output_cnt = 1) then
              output_cnt               <= (others => '0');
              rx_Frame_valid_pX_o      <= rx_valid_pol_inv;
              s_out_state              <= S_WAIT_RX_DATA;
            else
              output_cnt               <= output_cnt - 1;
            end if;
          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------
            output_cnt                 <= (others => '0');
            rx_FrameHeader_o           <= c_FrameHeader_zero;
            rx_BFramePayloads_o        <= c_BFramePayload_zero;
            rx_IFramePayloads_o        <= c_IFramePayload_zero;
            rx_CFramePayloads_o        <= c_CFramePayload_zero;
            rx_Frame_valid_pX_o        <= rx_valid_pol_inv;
            s_out_state                <= S_WAIT_RX_DATA;
        end case; 
      end if;
    end if;
  end process;

  rx_PFramePayloads_o <= c_PFramePayload_zero; -- depracated
--   wb_in.btrain_scr_dummy_i <= x"CAFE";

end Behavioral;

