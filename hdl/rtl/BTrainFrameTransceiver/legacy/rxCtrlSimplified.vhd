-------------------------------------------------------------------------------
-- Title      : BTrain frame rx logic
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : rxCtrSimplified.vhd
-- Author     : Maciej Lipinski <maciej.lipinski@cern.ch>
-- Company    : CERN BE-CO-HT
-- Platform   : FPGA-generic
-- Standard   : VHDL'93
-- Created    : 2016-03-21
-------------------------------------------------------------------------------
-- Description:
-- 
-- This module is a wrapper of a module that receives and interprets a B-brain frame. 

-- This module exists to maintaine backward-compatibility of interfaces for the clients
-- that use the old version of the rx ctrl.
----------------------------------------------------------------------------------
--
-- Copyright (c) 2016-2017 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.BTrain_legacy_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;

entity rxCtrlSimplified is
  generic (
    g_data_width : integer:=c_BTrain_streamer_data_width -- it must be set to
                                                         -- c_BTrain_streamer_data_width,
                                                         -- the generic is left for backwrad
                                                         -- compatibility
   );
  port(
    clk_i   : in std_logic; 
    reset_i : in std_logic;

    rx_data_i  : in std_logic_vector(g_data_width-1 downto 0);
    rx_valid_i : in std_logic;
    rx_first_i : in std_logic;
    rx_last_i  : in std_logic;

    rxframe_ctrl_o   : out btrainFrameCtrl;
    Irxframe_value_o : out ImFrameValue;
    Brxframe_value_o : out btrainFrameValue;
    rxframe_valid_o  : out std_logic
  );
end rxCtrlSimplified;

architecture Behavioral of rxCtrlSimplified is

  signal FrameHeader    : t_FrameHeader;
  signal BFramePayloads : t_BFramePayload;
  signal IFramePayloads : t_IFramePayload;

begin

  assert (g_data_width=c_BTrain_streamer_data_width)
    report "g_data_width not equal to c_BTrain_streamer_data_width and it must be so "
    severity error;

  rxframe_ctrl_o.bit_14to15       <= FrameHeader.bit_14to15;
  rxframe_ctrl_o.d_low_marker_bit <= FrameHeader.d_low_marker;
  rxframe_ctrl_o.f_low_marker_bit <= FrameHeader.f_low_marker;
  rxframe_ctrl_o.zero_cycle_bit   <= FrameHeader.zero_cycle;
  rxframe_ctrl_o.C0_bit           <= FrameHeader.C0;
  rxframe_ctrl_o.error_bit        <= FrameHeader.error;
  rxframe_ctrl_o.sim_eff_bit      <= FrameHeader.sim_eff;
  rxframe_ctrl_o.frame_type       <= FrameHeader.frame_type;
  
  Brxframe_value_o.B              <= BFramePayloads.B;
  Brxframe_value_o.Bdot           <= BFramePayloads.Bdot;
  Brxframe_value_o.oldB           <= BFramePayloads.oldB;
  Brxframe_value_o.measB          <= BFramePayloads.measB;
  Brxframe_value_o.simB           <= BFramePayloads.simB;
  Brxframe_value_o.synB           <= BFramePayloads.synB;

  Irxframe_value_o.I              <= IFramePayloads.I;

  U_RX_CTRL: BTrainFrameRxCtrl 
    port map(
      clk_i            => clk_i,
      rst_n_i          => reset_i,
      rx_data_i        => rx_data_i,
      rx_valid_i       => rx_valid_i,
      rx_first_p1_i    => rx_first_i,
      rx_last_p1_i     => rx_last_i,
      FrameHeader_o    => FrameHeader,
      BFramePayloads_o => BFramePayloads,
      IFramePayloads_o => IFramePayloads,
      rxframe_valid_p1_o  => rxframe_valid_o
    );

end Behavioral;