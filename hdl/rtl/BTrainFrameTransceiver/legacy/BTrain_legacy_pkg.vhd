-------------------------------------------------------------------------------
-- Title      : BTrain legacy over White Rabbit
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrain_legacy_pkg.vhd
-- Author     : Maciej Lipinski <maciej.lipinski@cern.ch>
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2017-04-18
-------------------------------------------------------------------------------
-- Description:
--
-- It is a package with declaration of legacy modules that are in the repo
-- just because there might be some applications that use these. If these
-- applications wanted to "smoothly" upgrade to new (debugged) code, it should
-- be possible using this legacy files
-- 
-- NOTE and BEWARE: this package and the modules shall not be used in
-- new implementations. They are deprecated and their support will cease
-- as soon as reasonable.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2017 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.BTrainFrameTransceiver_pkg.all;

package BTrain_legacy_pkg is

  component SynchroGen is
  generic (
    g_counter_width      : integer := 26
    );
    port(
      clk_i                : in std_logic; 
      rst_n_i              : in std_logic;
      reg_sample_period_i  : in std_logic_vector(25 downto 0);
      send_tick_p_o        : out std_logic
    );
  end component;

  type btrainFrameCtrl is record
    bit_14to15       : std_logic_vector(1 downto 0); --not used
    d_low_marker_bit : std_logic;                    --low marker Defocus associated to the sent value 
    f_low_marker_bit : std_logic;                    --low marker Focus associated to the sent value 
    zero_cycle_bit   : std_logic;                    --zero signal associated to the sent value 
    C0_bit           : std_logic;                    --C0 signal associated to the sent value 
    error_bit        : std_logic;                    --Error detected before sending bit
    sim_eff_bit      : std_logic;                    --Simulation or Effectiv bit
    frame_type       : std_logic_vector(7 downto 0); --type => 0x42 for B field frame
  end record btrainFrameCtrl;

  type btrainFrameValue is record
    B     : std_logic_vector(31 downto 0);
    Bdot  : std_logic_vector(31 downto 0);
    oldB  : std_logic_vector(31 downto 0);
    measB : std_logic_vector(31 downto 0);
    simB  : std_logic_vector(31 downto 0);
    synB  : std_logic_vector(31 downto 0);
  end record btrainFrameValue;
  
  type ImFrameValue is record
    I     : std_logic_vector(31 downto 0);
  end record ImFrameValue;

  component txCtrlSimplified is
    generic (
      g_data_width : integer:=c_BTrain_streamer_data_width
    );
    port(
      clk_i          : in std_logic;
      reset_i        : in std_logic;
      tx_B_frame_i   : in btrainFrameValue;
      tx_I_frame_i   : in ImFrameValue;
      tx_sync_i      : in std_logic;
      tx_sent_o      : out std_logic;
      force_send_i   : in std_logic;
      send_cnt_i     : in std_logic;
      simeff_bit_i   : in std_logic;
      error_bit_i    : in std_logic;
      d_low_marker_i : in std_logic;
      f_low_marker_i : in std_logic;
      C0_i           : in std_logic;
      zero_cycle_i   : in std_logic;
      frame_ID_i     : in std_logic_vector(7 downto 0);
      tx_data_o      : out std_logic_vector(g_data_width-1 downto 0);
      tx_valid_o     : out std_logic;
      tx_dreq_i      : in std_logic;
      tx_last_o      : out std_logic := '0';
      tx_flush_o     : out std_logic := '0'	
      );
  end component;

  component rxCtrlSimplified is
    generic (
      g_data_width     : integer:=c_BTrain_streamer_data_width
    );
    port(
      clk_i            : in std_logic; 
      reset_i          : in std_logic;

      rx_data_i        : in std_logic_vector(g_data_width-1 downto 0);
      rx_valid_i       : in std_logic;
      rx_first_i       : in std_logic;
      rx_last_i        : in std_logic;

      rxframe_ctrl_o   : out btrainFrameCtrl;
      Irxframe_value_o : out ImFrameValue;
      Brxframe_value_o : out btrainFrameValue;
      rxframe_valid_o  : out std_logic
    );
  end component;

end BTrain_legacy_pkg;

package body BTrain_legacy_pkg is

end BTrain_legacy_pkg;