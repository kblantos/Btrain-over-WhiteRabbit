-------------------------------------------------------------------------------
-- Title      : BTrain frame tx logic
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : txCtrlSimplified.vhd
-- Author     : Maciej Lipinski <maciej.lipinski@cern.ch>
-- Company    : CERN BE-CO-HT
-- Platform   : FPGA-generic
-- Standard   : VHDL'93
-- Created    : 2016-03-21
----------------------------------------------------------------------------------
-- Description:
-- 
-- This module is a wrapper of a module that constructs a B-brain frame and sends 
-- it using steamers as a single word. The width of the word is the size of the 
-- biggest BTrain frame (defined by c_STREAMER_DATA_WIDTH).
-- 
-- This module exists to maintaine backward-compatibility of interfaces for the clients
-- that use the old version of the tx ctrl.
----------------------------------------------------------------------------------
--
-- Copyright (c) 2016-2017 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;

use work.BTrainFrameTransceiver_pkg.all;
use work.wrcore_pkg.all;
use work.wr_fabric_pkg.all;
use work.wishbone_pkg.all;

entity txCtrlSimplified is
  generic (
    g_data_width : integer:=c_BTrain_streamer_data_width -- it must be set to
                                                         -- c_BTrain_streamer_data_width,
                                                         -- the generic is left for backwrad
                                                         -- compatibility
          );
  port(
    clk_i    : in std_logic;
    reset_i  : in std_logic;
    -- Input frame for BTrainWR
    tx_B_frame_i   : in btrainFrameValue;
    tx_I_frame_i   : in ImFrameValue;
    tx_sync_i      : in std_logic;
    tx_sent_o      : out std_logic;
    force_send_i   : in std_logic;
    send_cnt_i     : in std_logic;
    simeff_bit_i   : in std_logic;
    error_bit_i    : in std_logic;
    d_low_marker_i : in std_logic;
    f_low_marker_i : in std_logic;
    C0_i           : in std_logic;
    zero_cycle_i   : in std_logic;
    frame_ID_i	   : in std_logic_vector(7 downto 0);

    -- Data word to be sent.
    tx_data_o  : out std_logic_vector(g_data_width-1 downto 0);
    -- 1 indicates that the tx_data_o contains a valid data word.
    tx_valid_o : out std_logic;
    -- Synchronous data request: if active, the user may send a data word in
    -- the following clock cycle.
    tx_dreq_i  : in std_logic;
    -- Used to indicate the last data word in a larger block of samples.
    tx_last_o  : out std_logic := '0';
    -- Flush command. When asserted, the streamer will immediatly send out all
    -- the data that is stored in its TX buffer, ignoring g_tx_timeout.
    tx_flush_o : out std_logic := '0'	
    );
end txCtrlSimplified;

architecture Behavioral of txCtrlSimplified is

  signal FrameHeader    : t_FrameHeader;
  signal BFramePayloads : t_BFramePayload;
  signal IFramePayloads : t_IFramePayload;
  signal ctrl_send      : std_logic;
  signal ctrl_dbg_mode  : std_logic_vector(1 downto 0);

begin

  assert (g_data_width=c_BTrain_streamer_data_width)
    report "g_data_width not equal to c_BTrain_streamer_data_width and it must be so "
    severity error;

  FrameHeader.bit_14to15   <= (others => '0');
  FrameHeader.d_low_marker <= d_low_marker_i;
  FrameHeader.f_low_marker <= f_low_marker_i;
  FrameHeader.zero_cycle   <= zero_cycle_i;
  FrameHeader.C0           <= C0_i;
  FrameHeader.error        <= error_bit_i;
  FrameHeader.sim_eff      <= simeff_bit_i;
  FrameHeader.frame_type   <= frame_ID_i;

  BFramePayloads.B         <= tx_B_frame_i.B;
  BFramePayloads.Bdot      <= tx_B_frame_i.Bdot;
  BFramePayloads.oldB      <= tx_B_frame_i.oldB;
  BFramePayloads.measB     <= tx_B_frame_i.measB;
  BFramePayloads.simB      <= tx_B_frame_i.simB;
  BFramePayloads.synB      <= tx_B_frame_i.synB;

  IFramePayloads.I         <= tx_I_frame_i.I;

  ctrl_send                <= tx_sync_i or force_send_i;
  ctrl_dbg_mode            <= '0' & send_cnt_i;

  U_TX_CTRL: BTrainFrameTxCtrl 
    port map(
      clk_i               => clk_i,
      rst_n_i             => reset_i,

      ctrl_send_i         => ctrl_send,
      ctrl_dbg_mode_i     => ctrl_dbg_mode,

      FrameHeader_i       => FrameHeader,
      BFramePayloads_i    => BFramePayloads,
      IFramePayloads_i    => IFramePayloads,

      tx_sent_p1_o        => tx_sent_o,
      tx_data_o           => tx_data_o,
      tx_valid_o          => tx_valid_o,
      tx_dreq_i           => tx_dreq_i,
      tx_last_p1_o        => tx_last_o,
      tx_flush_p1_o       => tx_flush_o
      );

end Behavioral;

