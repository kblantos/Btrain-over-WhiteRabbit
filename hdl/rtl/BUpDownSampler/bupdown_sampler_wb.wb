  -- -*- Mode: LUA; tab-width: 2 -*-

peripheral {
  name = "WB control and data access of bupdown sampler ";
  description = "-----------------------------------------------------------------\
  Wishbone registers to manage/access module to compare old BTrain \
  system with BTrain over WR and the bupdown converter             \
  -----------------------------------------------------------------\
  Copyright (c) 2018 CERN/BE-CO-HT                                 \
  -----------------------------------------------------------------\
  GNU LESSER GENERAL PUBLIC LICENSE                                \
  -----------------------------------------------------------------\
                                                                   \
  This source file is free software; you can redistribute it       \
  and/or modify it under the terms of the GNU Lesser General       \
  Public License as published by the Free Software Foundation;     \
  either version 2.1 of the License, or (at your option) any       \
  later version.                                                   \
                                                                   \
  This source is distributed in the hope that it will be           \
  useful, but WITHOUT ANY WARRANTY; without even the implied       \
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR          \
  PURPOSE.  See the GNU Lesser General Public License for more     \
  details                                                          \
                                                                   \
  You should have received a copy of the GNU Lesser General        \
  Public License along with this source; if not, download it       \
  from http://www.gnu.org/licenses/lgpl-2.1.html                   \
  -----------------------------------------------------------------";
  prefix = "bupdown_sampler";
  hdl_entity = "bupdown_sampler_wb";
  version = 1;

  reg {
    name = "Stat and ctrl register";
    prefix = "SCR";

    field {
      name = "Start acquisition";
      prefix = "acq_start";
      description = "Start acquisition";
      type = MONOSTABLE;
    };
    field {
      name = "Stop acquisition";
      prefix = "acq_stop";
      description = "Stop acquisition";
      type = MONOSTABLE;
    };
    field {
      name = "C0 reset";
      prefix = "C0_reset";
      description = "C0 reset counters";
      type = MONOSTABLE;
    };
    field {
      name = "Acquisition mode";
      prefix = "acq_mode";
      description = "Acquisition mode:\
                    0: single \
                    1: circular buffer \
                    2: start at C0 active";
      type = SLV;
      size = 2;
      align = 4;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "C0 mode";
      prefix = "C0_mode";
      description = "C0 mode:\
                    0: C0_old: from timing system \
                    1: C0_wr: from BTrain frame";
      type = SLV;
      size = 2;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "Acquisition status";
      prefix = "acq_status";
      description = "Status:\
                    0: not in progress \
                    1: ongoing";
      type = SLV;
      size = 4;
      align = 8;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
    field {
      name = "Input Load";
      prefix = "in_load_disabled";
      description = "Input load disable:\
                    0: Input load not disabled (i.e by default enabled) \
                    1: Input load disabled";
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
      type = BIT;
    };
    field {
      name = "Dummy";
      prefix = "DUMMY";
      description = "dummy";
      type = SLV;
      size = 16;
      align =16;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "Memory status register";
    prefix = "msr";

    field {
      name = "Last address";
      prefix = "add_last";
      description = "Last address at which data is stored in memory";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "Memory config register";
    prefix = "mcr";

    field {
      name = "Sampling period";
      prefix = "s_period";
      description = "Configure the sampling period, i.e. whether to record each \
                     sample or only every X sample";
      type = SLV;
      size = 16;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };

  ram {
    name = "Memory for samples";
    prefix = "mem";
    size = 16384; -- 65536
    width = 32;
    access_bus = READ_WRITE;
    access_dev = READ_WRITE;
  };
};
