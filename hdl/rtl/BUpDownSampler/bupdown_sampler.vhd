--------------------------------------------------------------------------------
-- CERN
-- BTrain-over-WhiteRabbit
-- https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
--------------------------------------------------------------------------------
--
-- unit name:     bupdown_sampler.vhd
--
-- description:
--
--
--------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN BE/CO/HT
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- to_unsigned

library work;
use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;
use work.wishbone_pkg.all;
use work.gencores_pkg.all;
use work.bupdown_sampler_wbgen2_pkg.all;

entity bupdown_sampler is
  generic (
    --------------generic that have effect when wb_config is used---------------
    g_slave_mode           : t_wishbone_interface_mode      := CLASSIC;
    g_slave_granularity    : t_wishbone_address_granularity := BYTE);
  port (
    clk_i                  : in std_logic;
    rst_n_i                : in std_logic;

    ----------------------------------------------------------------
    -- Interface with BTrain FMC
    ----------------------------------------------------------------
    --received from WR network:
    rx_frame_header_i      : in  t_FrameHeader;
    rx_bframe_payloads_i   : in  t_BFramePayload;
    rx_frame_valid_p_i     : in  std_logic;

    -- configuration of inputs (based on and aligned with the configuration
    -- of the BTrainFrameTransceiver and bupdown_converter
    rx_cfg_pol_inv_i       : in  std_logic;
    ----------------------------------------------------------------
    -- Signals from legacy system: bup, bdown and C0 pulses
    ----------------------------------------------------------------
    C0_old_a_i             : in  std_logic;
    bup_old_a_i            : in  std_logic;
    bdown_old_a_i          : in  std_logic;
    ----------------------------------------------------------------
    -- Signals converted from BTrain over WR system: bup, bdown pulses
    ----------------------------------------------------------------
    bup_wr_a_i             : in  std_logic;
    bdown_wr_a_i           : in  std_logic;

    ----------------------------------------------------------------
    -- Config input stage (i.e. allow disabling input load
    ----------------------------------------------------------------
    input_load_disabled_o  : out std_logic;
    ----------------------------------------------------------------
    -- Wishbone interface
    ----------------------------------------------------------------
    wb_slave_i             : in  t_wishbone_slave_in := cc_dummy_slave_in;
    wb_slave_o             : out t_wishbone_slave_out);
end bupdown_sampler;

architecture arch of bupdown_sampler is

  constant c_ASYNC_INPUT_NUM : integer := 5;
  constant c_BUPDOWN_CNT_INC : signed(31 downto 0) := to_signed(1000,32);
  constant c_ADDR_WIDTH      : integer := 14;   -- size: 16384 words
  constant c_ADDR_MAX        : integer := 16383;

  signal async_inputs        : std_logic_vector(c_ASYNC_INPUT_NUM-1 downto 0);
  signal sync_inputs_p       : std_logic_vector(c_ASYNC_INPUT_NUM-1 downto 0);
  signal sync_inputs         : std_logic_vector(c_ASYNC_INPUT_NUM-1 downto 0);
  signal C0_old              : std_logic;
  signal C0_old_latch        : std_logic;
  signal bup_old_p           : std_logic;
  signal bdown_old_p         : std_logic;
  signal bup_wr_p            : std_logic;
  signal bdown_wr_p          : std_logic;
  signal bvalue_d            : std_logic_vector(31 downto 0);
  signal C0_d                : std_logic;
  signal C0_d_latch          : std_logic;
  signal C0_active           : std_logic;

  signal cnt_old             : std_logic_vector(31 downto 0);
  signal cnt_wr              : std_logic_vector(31 downto 0);


  signal rxed_B_frame        : std_logic;
  type t_store_samples_fsm   is (S_IDLE,
                                 S_WAIT_BFRAME,
                                 S_WAIT_C0,
                                 S_STORE_W0,
                                 S_STORE_W1,
                                 S_STORE_W2,
                                 S_STORE_END);
  signal s_state             : t_store_samples_fsm;

  signal wb_in               : t_bupdown_sampler_in_registers;
  signal wb_out              : t_bupdown_sampler_out_registers;
  signal wb_regs_slave_in    : t_wishbone_slave_in;
  signal wb_regs_slave_out   : t_wishbone_slave_out;

  signal wb_mem_addr_in      : std_logic_vector(c_ADDR_WIDTH-1 downto 0);
  signal wb_mem_data_out     : std_logic_vector(31 downto 0);
  signal wb_mem_data_in      : std_logic_vector(31 downto 0);
  signal wb_mem_wr_in        : std_logic;

  signal wb_mem_w1           : std_logic_vector(31 downto 0);
  signal wb_mem_w2           : std_logic_vector(31 downto 0);

  signal period_cnt          : unsigned(15 downto 0);

  -- rx_valid that is always active high -> this one is used in the internal
  -- logic
  signal rx_valid_active_high_p : std_logic;

  -- configuration of the input stage
  signal config_active       : std_logic_vector(c_ASYNC_INPUT_NUM-1 downto 0);
begin

  wb_in.msr_add_last_i <= std_logic_vector(to_unsigned(c_ADDR_MAX, 32));

  ------------------------------------------------------------------------------
  -- Input stage for bup/bdown pulses, identical for old and WR on purpose
  -- synchronize and deglich old pulses that are asynchronous
  ------------------------------------------------------------------------------
  async_inputs(0)    <= C0_old_a_i;
  async_inputs(1)    <= bup_old_a_i;
  async_inputs(2)    <= bdown_old_a_i;
  async_inputs(3)    <= bup_wr_a_i;
  async_inputs(4)    <= bdown_wr_a_i;

  -- configuration of the input stage:
  -- if (rx_cfg_pol_inv_i = '0') => config_active = "11111": active HIGH
  -- if (rx_cfg_pol_inv_i = '1') => config_active = "00000": active LOW
  config_active      <= (others => '1') when rx_cfg_pol_inv_i = '0' else
                        (others => '0');

  cmp_async_input_stage: gc_async_signals_input_stage
    generic map (
      g_signal_num           => c_ASYNC_INPUT_NUM,
      g_extended_pulse_width => 0,
      g_dglitch_filter_len   => 2)
    port map (
      clk_i           => clk_i,
      rst_n_i         => rst_n_i,
      signals_a_i     => async_inputs,
      config_active_i => config_active,
      signals_o       => sync_inputs,
      signals_p1_o    => sync_inputs_p);

   -- take full C0 signal so that the counter is not increamented while
   -- reset even if there are bup/bdown pulses comming (remember that C0
   -- is produced by timing receiver while pulses are provided by WR/old
   -- BTrain)
   -- the CO_old signal is always active high
   C0_old             <=     sync_inputs(0) when rx_cfg_pol_inv_i = '0' else
                         not sync_inputs(0);

   -- take single-clock pulses on rising edge to simplify the state machine
   bup_old_p          <= sync_inputs_p(1);
   bdown_old_p        <= sync_inputs_p(2);
   bup_wr_p           <= sync_inputs_p(3);
   bdown_wr_p         <= sync_inputs_p(4);

  ------------------------------------------------------------------------------
  -- count old and WR pulses with common reset on C0
  ------------------------------------------------------------------------------
  C0_active      <= C0_old when wb_out.scr_c0_mode_o = "00"  else
                    C0_d   when wb_out.scr_c0_mode_o = "01"  else
                    '0';

  p_pulses_counters : process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' then
        cnt_old  <= (others => '0');
        cnt_wr   <= (others => '0');
      else
        -- count old B
        if (C0_active = '1' or  wb_out.scr_c0_reset_o = '1') then
          cnt_old <= (others => '0');
        elsif(bup_old_p = '1' and bdown_old_p = '0') then
          cnt_old <= std_logic_vector(signed(cnt_old) + c_BUPDOWN_CNT_INC);
        elsif(bup_old_p = '0' and bdown_old_p = '1') then
          cnt_old <= std_logic_vector(signed(cnt_old) - c_BUPDOWN_CNT_INC);
        end if;
        -- count wr B
        if (C0_active = '1' or  wb_out.scr_c0_reset_o = '1') then
          cnt_wr <= (others => '0');
        elsif(bup_wr_p = '1' and bdown_wr_p = '0') then
          cnt_wr <= std_logic_vector(signed(cnt_wr) + c_BUPDOWN_CNT_INC);
        elsif(bup_wr_p = '0' and bdown_wr_p = '1') then
          cnt_wr <= std_logic_vector(signed(cnt_wr) - c_BUPDOWN_CNT_INC);
        end if;
      end if;
    end if;
  end process;
  ------------------------------------------------------------------------------
  -- store previously received info from BTrain frames
  ------------------------------------------------------------------------------

  rx_valid_active_high_p  <= rx_frame_valid_p_i when rx_cfg_pol_inv_i = '0' else
                             not rx_frame_valid_p_i;

  -- to simplify the code, indicate when we receive the frame in which
  -- we are interested
  rxed_B_frame <= '1' when (rx_valid_active_high_p = '1' and
                           rx_frame_header_i.frame_type   = c_ID_BkFrame) else
                  '0';
  p_reg_bvalue_and_C0 : process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' then
        bvalue_d         <= (others => '0');
        C0_d             <= '0';
      else
        if rxed_B_frame = '1' then
          bvalue_d         <= rx_bframe_payloads_i.B;
          C0_d             <= rx_frame_header_i.C0;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- store in a round buffer:
  -- * values of old/wr pulse counters
  -- * the previously received Bvalue from BTrain frame
  -- * C0 input
  -- * C0 from btrain frame
  --
  -- Memory map
  -- i+0: bvalue_d
  -- i+1: cnt_old (31 downto 1) & C0_old;
  -- i+2: cnt_wr  (31 downto 1) & C0_d;
  --
  -- the storing is done when new BTrain frame is received (every 4us)
  ------------------------------------------------------------------------------

  p_store_samples : process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' then
        wb_mem_addr_in  <= (others => '0');
        wb_mem_data_in  <= (others => '0');
        wb_mem_wr_in    <= '0';
        wb_mem_w1       <= (others => '0');
        wb_mem_w2       <= (others => '0');
        s_state         <= S_IDLE;
        C0_old_latch    <= '0';
        C0_d_latch      <= '0';
        period_cnt      <= (others => '0');
      else

        case s_state is
          ---------------------------------------------------------------------
          when S_IDLE =>  --
          ---------------------------------------------------------------------
            if wb_out.scr_acq_start_o = '1' then
              wb_mem_addr_in  <= (others => '0');
              period_cnt      <= unsigned(wb_out.mcr_s_period_o);
              if wb_out.scr_acq_mode_o = "10" then -- start at C0 active
                s_state         <= S_WAIT_C0;
              else
                s_state         <= S_WAIT_BFRAME;
              end if;
            end if;
          ---------------------------------------------------------------------
          when S_WAIT_C0 =>  --
          ---------------------------------------------------------------------
            if C0_active = '1' or wb_out.scr_c0_reset_o = '1' then
              s_state         <= S_WAIT_BFRAME;
              C0_old_latch    <= C0_old;
              C0_d_latch      <= C0_d;
            end if;
          ---------------------------------------------------------------------
          when S_WAIT_BFRAME =>  --
          ---------------------------------------------------------------------
            if wb_out.scr_acq_stop_o = '1' then
              s_state         <= S_IDLE;
            elsif (period_cnt = 0 or period_cnt = 1) and rxed_B_frame = '1' then

              -- Store in memory Bvalue received in the Btrain frame  that
              -- preceeded the BTrain frame that initiated this write.
              -- This is to allow time for generating the bup/bdown pulses
              -- and then counting them
              wb_mem_data_in  <= bvalue_d;

              -- Save counter values to have consistent snapshot, they will be
              -- stored to consecutive addresses. Since the are incremented
              -- by scalling factor, their initial bits are always zero. We
              -- use these spare bits to store the value of C0 big from the
              -- old system and from WR.
              wb_mem_w1       <= cnt_old (31 downto 1) & (C0_old or C0_old_latch);
              wb_mem_w2       <= cnt_wr  (31 downto 1) & (C0_d   or C0_d_latch);
              wb_mem_wr_in    <= '1';
              s_state         <= S_STORE_W0;
              C0_old_latch    <= '0';
              C0_d_latch      <= '0';

             elsif period_cnt > 1 and rxed_B_frame = '1' then -- skip the sample

              period_cnt      <= period_cnt - 1;

            end if;
          ---------------------------------------------------------------------
          when S_STORE_W0 =>  -- when in this state we store W0 and prepare W1
          ---------------------------------------------------------------------
            wb_mem_data_in    <= wb_mem_w1; -- cnt_old
            wb_mem_addr_in    <= std_logic_vector(unsigned(wb_mem_addr_in) + 1);
            wb_mem_wr_in      <= '1';
            s_state           <= S_STORE_W1;
          ---------------------------------------------------------------------
          when S_STORE_W1 =>  -- when in this state we store W1 and prepare W2
          ---------------------------------------------------------------------
            wb_mem_data_in    <= wb_mem_w2; -- cnt_wr
            wb_mem_addr_in    <= std_logic_vector(unsigned(wb_mem_addr_in) + 1);
            wb_mem_wr_in      <= '1';
            s_state           <= S_STORE_W2;
          ---------------------------------------------------------------------
          when S_STORE_W2 =>  -- when in this state we store W1 and prepare W2
          ---------------------------------------------------------------------
            wb_mem_addr_in    <= std_logic_vector(unsigned(wb_mem_addr_in) + 1);
            wb_mem_wr_in      <= '0';
            s_state           <= S_STORE_END;
          ---------------------------------------------------------------------
          when S_STORE_END =>  --
          ---------------------------------------------------------------------
            wb_mem_wr_in      <= '0';

            if wb_mem_addr_in < std_logic_vector(to_unsigned(c_ADDR_MAX, c_ADDR_WIDTH)) then
              period_cnt      <= unsigned(wb_out.mcr_s_period_o);
              s_state         <= S_WAIT_BFRAME;
            -- do not continue: either single acuqisition or start at C0 active
            elsif wb_out.scr_acq_mode_o = "00" or wb_out.scr_acq_mode_o = "10" then
              s_state         <= S_IDLE;
            elsif wb_out.scr_acq_mode_o = "01" then  -- round buffer
              period_cnt      <= unsigned(wb_out.mcr_s_period_o);
              s_state         <= S_WAIT_BFRAME;
              wb_mem_addr_in  <= (others => '0');
            else
              s_state         <= S_IDLE;
            end if;
          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------
            s_state             <= S_IDLE;
        end case;
      end if;
    end if;
  end process;
  wb_in.scr_dummy_i      <= x"CAFE";
  wb_in.scr_acq_status_i <=  "0001" when s_state /= S_IDLE else
                             "0000";

  input_load_disabled_o  <= wb_out.scr_in_load_disabled_o;
  ------------------------------------------------------------------------------
  -- Wishbone slave
  ------------------------------------------------------------------------------
  cmp_wishbone_slave: entity work.bupdown_sampler_wb
    port map (
      rst_n_i                           => rst_n_i,
      clk_sys_i                         => clk_i,
      wb_adr_i                          => wb_regs_slave_in.adr(14 downto 0),
      wb_dat_i                          => wb_regs_slave_in.dat,
      wb_dat_o                          => wb_regs_slave_out.dat,
      wb_cyc_i                          => wb_regs_slave_in.cyc,
      wb_sel_i                          => wb_regs_slave_in.sel(3 downto 0),
      wb_stb_i                          => wb_regs_slave_in.stb,
      wb_we_i                           => wb_regs_slave_in.we,
      wb_ack_o                          => wb_regs_slave_out.ack,
      wb_stall_o                        => wb_regs_slave_out.stall,
      bupdown_sampler_mem_addr_i        => wb_mem_addr_in,
      bupdown_sampler_mem_data_o        => wb_mem_data_out,
      bupdown_sampler_mem_rd_i          => '0',
      bupdown_sampler_mem_data_i        => wb_mem_data_in,
      bupdown_sampler_mem_wr_i          => wb_mem_wr_in,
      regs_i                            => wb_in,
      regs_o                            => wb_out);

  cmp_wb_adapter : wb_slave_adapter
    generic map (
      g_master_use_struct  => true,
      g_master_mode        => CLASSIC,
      g_master_granularity => WORD,
      g_slave_use_struct   => true,
      g_slave_mode         => g_slave_mode,
      g_slave_granularity  => g_slave_granularity)
    port map (
      clk_sys_i  => clk_i,
      rst_n_i    => rst_n_i,
      slave_i    => wb_slave_i,
      slave_o    => wb_slave_o,
      master_i   => wb_regs_slave_out,
      master_o   => wb_regs_slave_in);

    -- drive unused WB signals
  wb_regs_slave_out.rty <= '0';
  wb_regs_slave_out.err <= '0';
end arch;