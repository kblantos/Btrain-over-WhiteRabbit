library ieee;

use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity clock_output is
  port (
    -- clocks (same as the desired output frequency) coming from the PLL
    -- primitive, phase-shifted by 180 degrees.
    clk_0_i   : in std_logic;
    clk_180_i : in std_logic;

    clk_out_o   : out std_logic;
    clk_out_p_o : out std_logic;
    clk_out_n_o : out std_logic
    );
end clock_output;

architecture rtl of clock_output is

signal ddr_out : std_logic;
begin

  U_DDR_Output: ODDR2
    generic map (
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "ASYNC" )
    port map (
      Q  => ddr_out,
      C0 => clk_0_i,
      C1 => clk_180_i,
      D0 => '1',
      D1 => '0',
      CE => '1',
      R=>'0',
      S=>'0'
      );

  U_Output_Buffer: OBUFDS
    generic map (
      IOSTANDARD  => "LVDS_33")
    port map (
      O  => clk_out_p_o,
      OB => clk_out_n_o,
      I  => ddr_out);

  clk_out_o <= ddr_out;
end rtl;

