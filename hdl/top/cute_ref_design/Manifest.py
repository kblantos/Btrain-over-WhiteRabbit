fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl/",
        "../../ip_cores/wr-cores",
        "../../ip_cores/wr-cores/board/cute",
        "../../ip_cores/general-cores",
        "../../ip_cores/wr-cores//platform/xilinx/chipscope"
    ],
}

files = [
    "cute_btrain_ref_top.vhd",
    "clock_output.vhd"
]


#modules = {
    #"local" : [
        #"../../",
        #"../../board/cute",
    #],
    #"git" : [
        #"git://ohwr.org/hdl-core-lib/general-cores.git",
        #"git://ohwr.org/hdl-core-lib/etherbone-core.git",
    #],
#}
