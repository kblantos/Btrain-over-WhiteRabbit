fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl/",
        "../../ip_cores/wr-cores",
        "../../ip_cores/wr-cores/board/spec",
        "../../ip_cores/general-cores",
        "../../ip_cores/gn4124-core",
        "../../ip_cores/vme64x-core",
        "../../ip_cores/etherbone-core",
    ],
}

files = [
    "spec_btrain_ref_top.vhd"
]
