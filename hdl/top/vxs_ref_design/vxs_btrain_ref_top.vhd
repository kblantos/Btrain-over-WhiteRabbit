-------------------------------------------------------------------------------
-- CERN
-- BTrain over White Rabbit
-- https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- vxs_btrain_ref_top.vhd
-------------------------------------------------------------------------------
--
-- Description: Top-level file for the WRPC reference design on the VXS switch.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.wr_board_pkg.all;
use work.wr_vxs_pkg.all;
use work.gn4124_core_pkg.all;
use work.streamers_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity vxs_btrain_ref_top is
  generic (
    g_dpram_initf : string  := "../../ip_cores/wr-cores/bin/wrpc/wrc_phy8.bram";
--     g_dpram_initf : string  := "../../../../binaries/wrc-VXS-support-v8.bram";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation : integer := 0
  );
  port ( -- port description taken from EDA-02299-V2-VXS-Switch project
        ---------------------------------------------------------------
        -- WR-related IOs
        ---------------------------------------------------------------
        VCXO_MHZ20        : in    std_logic; -- clk_20m_vcxo_i
        VCXO_MHZ125_N     : in    std_logic; -- clk_125m_pllref_p_i
        VCXO_MHZ125_P     : in    std_logic; -- clk_125m_pllref_n_i
        GTP118_CLK_N      : in    std_logic; -- clk_125m_gtp_n_i
        GTP118_CLK_P      : in    std_logic; -- clk_125m_gtp_p_i
        DAC_PLL_SCLK      : out   std_logic; -- plldac_sclk_o
        DAC_PLL_DIN       : out   std_logic; -- plldac_din_o
        DAC_PLL20_SYNC_N  : out   std_logic; -- pll20dac_cs_n_o
        DAC_PLL125_SYNC_N : out   std_logic; -- pll25dac_cs_n_o

        DQ_WR             : inout std_logic; -- 1-wire
        UART_RXD          : in    std_logic; -- uart_rxd_i
        UART_TXD          : out   std_logic; -- uart_txd_o

        -- WR-dedicated SFP
        SFP2FPGA_P        : in std_logic; -- sfp_rxp_i
        SFP2FPGA_N        : in std_logic; -- sfp_rxn_i
        FPGA2SFP_N        : out std_logic;-- sfp_txn_o
        FPGA2SFP_P        : out std_logic;-- sfp_txp_o
        SFP3_PRSNT_N      : in std_logic;
        SFP3_LOS          : in std_logic;
        SFP3_SCL          : inout std_logic;
        SFP3_TX_DIS       : out std_logic;
        SFP3_SDA          : inout std_logic;

        --- backup SFP routred via switch FPGA (unused)
        FPGA2VXS2_N       : out std_logic;
        FPGA2VXS2_P       : out std_logic;
        VXS2FPGA2_N       : in std_logic;
        VXS2FPGA2_P       : in std_logic;
        SFP2_LOS          : in std_logic;
        SFP2_TX_DIS       : out std_logic;
        SFP2_SCL          : inout std_logic;
        SFP2_SDA          : inout std_logic;
        SFP2_PRSNT_N      : in std_logic;

        -- IOs
        FPGA_IO_Z1        : out std_logic;
        FPGA_IO_Z2        : out std_logic;
        FPGA_IO_E1_N      : out std_logic;
        FPGA_IO_E2_N      : out std_logic;
        FPGA_IO_OUT1      : out std_logic;
        FPGA_IO_OUT2      : out std_logic;
        FPGA_IO_IN1       : in std_logic;
        FPGA_IO_IN2       : in std_logic;

        -- control of SFP's leds
        LED_SFP_SDA       : inout std_logic;
        LED_SFP_SCL       : inout std_logic;
        POR               : in std_logic;


        LED_CLK_R         : out std_logic;
        LED_CLK_G         : out std_logic;

        -- flash
        FRAMMOSI          : out std_logic;
        FRAMSCLK          : out std_logic;
        FRAMWPDIS         : in std_logic; -- write protect input, use for sfp MUX
        FRAMWP_N          : out std_logic;
        FRAMMISO          : in std_logic;
        FRAMCS_N          : out std_logic;


        ---------------------------------------------------------------
        -- other IOs - unused in the WR ref design
        ---------------------------------------------------------------
        SYSRST_N          : in  std_logic;
        PP3_SCL           : in std_logic;
        PP2_SDA           : in std_logic;
        ACE_TDO_I         : in std_logic;
        PP2_SCL           : in std_logic;
        Q_MISO            : in std_logic;
        FP_JTAG_ENA_N     : out std_logic;
        XBAR_DATA         : inout std_logic_vector(7 downto 0 );
        PP1_SDA_I         : in std_logic;
        PP12_SDA          : in std_logic;
        PP12_SCL          : in std_logic;
        GAP_N             : in std_logic;
        XBAR_SET_N        : out std_logic;
        PP11_SDA          : in std_logic;
        PP11_SCL          : in std_logic;
        PP10_SDA          : in std_logic;
        PP10_SCL          : in std_logic;
        HWVERSION         : in std_logic_vector(3 downto 0 );
        GA_N              : in std_logic_vector(4 downto 0 );
        XBAR_RSTRX_N      : out std_logic_vector(1 downto 0 );
        PP9_SDA           : in std_logic;
        XBAR_CS_N         : out std_logic;
        PP9_SCL           : in std_logic;
        XBAR_PERROR       : in std_logic_vector(1 downto 0 );
        PP8_SDA           : in std_logic;
        SFP1_LOS          : in std_logic;
        SFP1_PRSNT_N      : in std_logic;
        PP8_SCL           : in std_logic;
        PP7_SDA           : in std_logic;
        SFP0_LOS          : in std_logic;
        PP7_SCL           : in std_logic;
        PP6_SDA           : in std_logic;
        SFP1_SCL          : inout std_logic;
        XBAR_RST_N        : out std_logic;
        XBAR_LOS          : in std_logic;
        PP6_SCL           : in std_logic;
        XBAR_ADDR         : out std_logic_vector(9 downto 0 );
        SFP1_SDA          : inout std_logic;
        PP5_SDA           : in std_logic;
        SFP0_SCL          : inout std_logic;
        XBAR_DS_N         : out std_logic;
        XBAR_RD           : out std_logic;
        PP5_SCL           : in std_logic;
        SW_SE1            : in std_logic;
        SW_SE2            : in std_logic;
        SW_SE3            : in std_logic;
        SW_SE4            : in std_logic;
        SW_SE5            : in std_logic;
        SW_SE6            : in std_logic;
        SW_SE7            : in std_logic;
        SW_SE8            : in std_logic;
        SFP0_SDA          : inout std_logic;
        PP4_SDA           : in std_logic;
        XBAR_DIS_N        : out std_logic;
        PP4_SCL           : in std_logic;
        PP3_SDA           : in std_logic;
        DQ                : inout std_logic;
        SFP0_PRSNT_N      : in std_logic

        -- pins commeted out in constraints to prevent warnings/errors
--         FPGA2VXS0_N       : out std_logic;
--         FPGA2VXS0_P       : out std_logic;
--         VXS2FPGA0_N       : in std_logic;
--         VXS2FPGA0_P       : in std_logic;
--         FPGA_MHZ100_N     : in std_logic;
--         FPGA_MHZ100_P     : in std_logic;
--         VXS2FPGA1_N       : in std_logic;
--         VXS2FPGA1_P       : in std_logic;
--         FPGA2VXS1_N       : out std_logic;
--         FPGA2VXS1_P       : out std_logic;
--         GTP122_CLK_N      : in std_logic;
--         GTP122_CLK_P      : in std_logic;
--         RF_CLK_FPGA_N     : in std_logic;
--         RF_CLK_FPGA_P     : in std_logic;
--         TAG_FPGA_N        : in std_logic;
--         TAG_FPGA_P        : in std_logic;
--         FPGA_MHZ200_N     : in std_logic;
--         FPGA_MHZ200_P     : in std_logic;
--         FPGA_MHZ50_N      : in std_logic;
--         FPGA_MHZ50_P      : in std_logic;
--         SW_RX_N           : in std_logic;
--         SW_RX_P           : in std_logic;
--         SW_TX_N           : out std_logic;
--         SW_TX_P           : out std_logic;
--         LED_SPARE_R       : out std_logic;
--         LED_SPARE_G       : out std_logic;
--         TESTPORT          : out std_logic_vector(15 downto 4 );
--         PP1_SDA_O_N       : out std_logic;
--         XBAR_SER          : out std_logic;
--         PP1_SCL_I         : in std_logic;
--         ACE_TDI_O         : out std_logic;
--         Q_CS_N            : out std_logic;
--         ACE_TMS_O         : out std_logic;
--         KEEPER            : out std_logic;
--         POWERDOWN         : out std_logic;
--         XBAR_ENRX_N       : out std_logic_vector(1 downto 0 );
--         ACE_TCK_O         : out std_logic;
--         SFP0_TX_DIS       : out std_logic;
--         Q_SCLK            : out std_logic;
--         SFP1_TX_DIS       : out std_logic;
--         PP1_SCL_O_N       : out std_logic;
--         Q_MOSI            : out std_logic;
--         XBAR_ENTX_N       : out std_logic_vector(1 downto 0 );
--         SEL_VXS           : out std_logic
        );
end entity vxs_btrain_ref_top;

architecture top of vxs_btrain_ref_top is

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- clock and reset
  signal clk_sys_62m5   : std_logic;
  signal rst_sys_62m5_n : std_logic;
  signal rst_ref_125m_n : std_logic;
  signal clk_ref_125m   : std_logic;
  signal clk_ext_10m    : std_logic;

  -- I2C EEPROM
  signal eeprom_sda_in  : std_logic;
  signal eeprom_sda_out : std_logic;
  signal eeprom_scl_in  : std_logic;
  signal eeprom_scl_out : std_logic;

  -- SFP I2C -> ch0 & ch1
  signal sfp_sda_in,  sfp1_sda_in  : std_logic;
  signal sfp_sda_out, sfp1_sda_out : std_logic;
  signal sfp_scl_in,  sfp1_scl_in  : std_logic;
  signal sfp_scl_out, sfp1_scl_out : std_logic;

  -- OneWire
  signal onewire_data : std_logic;
  signal onewire_oe   : std_logic;

  -- LEDs and GPIO
  signal wrc_abscal_txts_out : std_logic;
  signal wrc_abscal_rxts_out : std_logic;
  signal wrc_pps_out : std_logic;
  signal wrc_pps_led : std_logic;
  signal rx_valid_ext: std_logic;

  -- DIO Mezzanine
  signal dio_in  : std_logic_vector(4 downto 0);
  signal dio_out : std_logic_vector(4 downto 0);

  -- ChipScope for histogram readout/debugging

--   component chipscope_virtex5_icon
--   port (
--     CONTROL0: inout std_logic_vector(35 downto 0));
--   end component;
-- 
--   component chipscope_virtex5_ila
--   port (
--     CONTROL: inout std_logic_vector(35 downto 0);
--     CLK: in std_logic;
--     TRIG0: in std_logic_vector(31 downto 0);
--     TRIG1: in std_logic_vector(31 downto 0);
--     TRIG2: in std_logic_vector(31 downto 0);
--     TRIG3: in std_logic_vector(31 downto 0));
--   end component;

  signal CONTROL0, CONTROL1, CONTROL2, CONTROL3 : std_logic_vector(35 downto 0);
  signal TRIG0, TRIG1, TRIG2, TRIG3 : std_logic_vector(31 downto 0);

  signal led_link : std_logic;
  signal led_ack  : std_logic;

  signal SFP3_TX_DIS_out, SFP3_LOS_in : std_logic;

  -- WR Streamers <---> BTrain
  signal tx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal tx_valid    : std_logic;
  signal tx_dreq     : std_logic;
  signal tx_last_p1  : std_logic;
  signal tx_flush_p1 : std_logic;
  signal tx_cfg      : t_tx_streamer_cfg;
  -- rx
  signal rx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal rx_valid    : std_logic;
  signal rx_first_p1 : std_logic;
  signal rx_dreq     : std_logic;
  signal rx_last_p1  : std_logic;
  signal rx_cfg      : t_rx_streamer_cfg;

  -- BTrain <---> BTrain application (e.g. on BTrainFMC)
  signal tx_ready            : std_logic;
  signal tx_TransmitFrame_p1 : std_logic;
  signal tx_FrameHeader      : t_FrameHeader;
  signal tx_BFramePayloads   : t_BFramePayload;
  signal tx_IFramePayloads   : t_IFramePayload;
  signal tx_CFramePayloads   : t_CFramePayload;
  signal rx_FrameHeader      : t_FrameHeader;
  signal rx_BFramePayloads   : t_BFramePayload;
  signal rx_IFramePayloads   : t_IFramePayload;
  signal rx_CFramePayloads   : t_CFramePayload;
  signal rx_Frame_valid_pX   : std_logic;
  signal rx_Frame_typeID     : std_logic_vector(c_type_ID_size-1 downto 0);

  signal sfp_mux_sel: std_logic;
begin  -- architecture top

  -----------------------------------------------------------------------------
  -- The WR PTP core board package
  -----------------------------------------------------------------------------

  cmp_xwrc_board_vxs : xwrc_board_vxs
    generic map (
      g_simulation                => g_simulation,
      g_with_external_clock_input => FALSE,
      g_dpram_initf               => g_dpram_initf,
      g_streamers_op_mode         => TX_AND_RX,
      g_gtp_enable_ch0            => 1,
      g_gtp_enable_ch1            => 1,
      g_gtp_mux_enable            => TRUE,
      g_tx_streamer_params        => c_tx_streamer_params_btrain,
      g_rx_streamer_params        => c_rx_streamer_params_btrain,
      g_fabric_iface              => STREAMERS)
    port map (
      areset_n_i          => std_logic(not POR), -- Power
      areset_edge_n_i     => '1', -- not use
      clk_20m_vcxo_i      => VCXO_MHZ20,
      clk_125m_pllref_p_i => VCXO_MHZ125_P,
      clk_125m_pllref_n_i => VCXO_MHZ125_N,
      clk_125m_gtp_n_i    => GTP118_CLK_N,
      clk_125m_gtp_p_i    => GTP118_CLK_P,

      clk_sys_62m5_o      => clk_sys_62m5,
      clk_ref_125m_o      => clk_ref_125m,
      rst_sys_62m5_n_o    => rst_sys_62m5_n,
      rst_ref_125m_n_o    => rst_ref_125m_n,

      plldac_sclk_o       => DAC_PLL_SCLK,
      plldac_din_o        => DAC_PLL_DIN,
      pll25dac_cs_n_o     => DAC_PLL125_SYNC_N,
      pll20dac_cs_n_o     => DAC_PLL20_SYNC_N,

      -- ch0 : backup
      sfp_txp_o           => FPGA2VXS2_P,
      sfp_txn_o           => FPGA2VXS2_N,
      sfp_rxp_i           => VXS2FPGA2_P,
      sfp_rxn_i           => VXS2FPGA2_N,
      sfp_det_i           => SFP2_PRSNT_N,
      sfp_sda_i           => sfp_sda_in,
      sfp_sda_o           => sfp_sda_out,
      sfp_scl_i           => sfp_scl_in,
      sfp_scl_o           => sfp_scl_out,
      sfp_rate_select_o   => open, -- connect later
      sfp_tx_fault_i      => open,
      sfp_tx_disable_o    => SFP2_TX_DIS,
      sfp_los_i           => SFP2_LOS,

      sfp_mux_sel_i       => sfp_mux_sel, -- default is '1'

      -- ch1 : dedicated to WR
      sfp1_txp_o           => FPGA2SFP_P,
      sfp1_txn_o           => FPGA2SFP_N,
      sfp1_rxp_i           => SFP2FPGA_P,
      sfp1_rxn_i           => SFP2FPGA_N,
      sfp1_det_i           => SFP3_PRSNT_N,
      sfp1_sda_i           => sfp1_sda_in,
      sfp1_sda_o           => sfp1_sda_out,
      sfp1_scl_i           => sfp1_scl_in,
      sfp1_scl_o           => sfp1_scl_out,
      sfp1_rate_select_o   => open, -- connect later
      sfp1_tx_fault_i      => open,
      sfp1_tx_disable_o    => SFP3_TX_DIS_out,
      sfp1_los_i           => SFP3_LOS_in,

      eeprom_sda_i        => '1',
      eeprom_sda_o        => open,
      eeprom_scl_i        => '1',
      eeprom_scl_o        => open,

      onewire_i           => onewire_data,
      onewire_oen_o       => onewire_oe,
      -- Uart
      uart_rxd_i          => UART_RXD,
      uart_txd_o          => UART_TXD,

      -- SPI Flash
      flash_sclk_o        => FRAMSCLK,
      flash_ncs_o         => FRAMCS_N,
      flash_mosi_o        => FRAMMOSI,
      flash_miso_i        => FRAMMISO,

      pps_p_o             => wrc_pps_out,
      pps_led_o           => wrc_pps_led,
      led_link_o          => led_link,
      led_act_o           => led_ack,
      wrs_tx_data_i       => tx_data,
      wrs_tx_valid_i      => tx_valid,
      wrs_tx_dreq_o       => tx_dreq,
      wrs_tx_last_i       => tx_last_p1,
      wrs_tx_flush_i      => tx_flush_p1,
      wrs_tx_cfg_i        => tx_cfg,
      wrs_rx_first_o      => rx_first_p1,
      wrs_rx_last_o       => rx_last_p1,
      wrs_rx_data_o       => rx_data,
      wrs_rx_valid_o      => rx_valid,
      wrs_rx_dreq_i       => rx_dreq,
      wrs_rx_cfg_i        => rx_cfg
      );

  LED_CLK_G               <= led_link;
  LED_CLK_R               <= led_ack;

  -- Configuration of streamers:
  -- 1) For synthesis : In Btrain deployments, by default, streamers use VID=0
  --    (i.e. priority tagging). 
  -- 2) For simulation: Using VLANs (VID=0) is problematic (requires special
  --    configuration). It is easier to disable VLANs for simulation.
  gen_streamers_cfg_sim: if (g_simulation>0) generate -- no VLAN
    rx_cfg <= c_rx_streamer_cfg_default;
    tx_cfg <= c_tx_streamer_cfg_default;
  end generate gen_streamers_cfg_sim;
  gen_streamers_cfg_syn: if (g_simulation=0) generate -- with VLAN(VID=0)
    rx_cfg <= c_rx_streamer_cfg_btrain;
    tx_cfg <= c_tx_streamer_cfg_btrain;
  end generate gen_streamers_cfg_syn;

  -- Tristates for SFP EEPROM
  -- --------------------------------------------------------------------------
  -- NOTE: when integrating int VXS design, use multiplexer controlled by 
  -- signal sfp_mux_sel in order to connect it back to a single WR-dedictated
  -- port of SFPCtrl block;
  -- -------------------------------------------------------------------------- integrating 
  -- ch0 - backup
  SFP2_SCL                <= '0' when sfp_scl_out = '0' else 'Z';
  SFP2_SDA                <= '0' when sfp_sda_out = '0' else 'Z';
  sfp_scl_in              <= SFP2_SCL;
  sfp_sda_in              <= SFP2_SDA;
  -- ch1 - dedicated to wr:
  SFP3_SCL                <= '0' when sfp1_scl_out = '0' else 'Z';
  SFP3_SDA                <= '0' when sfp1_sda_out = '0' else 'Z';
  sfp1_scl_in             <= SFP3_SCL;
  sfp1_sda_in             <= SFP3_SDA;

  SFP3_LOS_in             <= SFP3_LOS;
  SFP3_TX_DIS             <= SFP3_TX_DIS_out;

  -- tri-state onewire access
  DQ_WR    <= '0' when (onewire_oe = '1') else 'Z';
  onewire_data <= DQ_WR;

  U_sync_with_clk : gc_sync_ffs
    port map (
      clk_i          => clk_sys_62m5,
      rst_n_i        => rst_sys_62m5_n,
      data_i         => FRAMWPDIS,
      synced_o       => sfp_mux_sel);

  -------------------------------------------------------------------------------------------
  -- BTrain frames transceiver
  -------------------------------------------------------------------------------------------
  cmp_btrain : BTrainFrameTransceiver
    generic map(
      g_rx_BframeType       => c_ID_ALL,  -- accept all types of BTrain frame - only debugging
      g_use_wb_config       => TRUE,
      g_slave_mode          => CLASSIC,
      g_slave_granularity   => BYTE)
    port map(
      clk_i                 => clk_sys_62m5,
      rst_n_i               => rst_sys_62m5_n,

      tx_data_o             => tx_data,
      tx_valid_o            => tx_valid,
      tx_dreq_i             => tx_dreq,
      tx_last_p1_o          => tx_last_p1,
      tx_flush_p1_o         => tx_flush_p1,
      -- rx
      rx_data_i             => rx_data,
      rx_valid_i            => rx_valid,
      rx_first_p1_i         => rx_first_p1,
      rx_dreq_o             => rx_dreq,
      rx_last_p1_i          => rx_last_p1,

      rx_FrameHeader_o      => rx_FrameHeader,
      rx_BFramePayloads_o   => rx_BFramePayloads,
      rx_IFramePayloads_o   => rx_IFramePayloads,
      rx_CframePayloads_o   => rx_CFramePayloads,
      rx_Frame_valid_pX_o   => rx_Frame_valid_pX,
      rx_Frame_typeID_o     => rx_Frame_typeID,

      ready_o               => tx_ready,
      tx_TransmitFrame_p1_i => tx_TransmitFrame_p1,
      tx_FrameHeader_i      => tx_FrameHeader,
      tx_BFramePayloads_i   => tx_BFramePayloads,
      tx_IFramePayloads_i   => tx_IFramePayloads,
      tx_CFramePayloads_i   => tx_CFramePayloads
      );

  tx_TransmitFrame_p1 <= '0';
  tx_FrameHeader      <= c_FrameHeader_dummy;
  tx_BFramePayloads   <= c_BFramePayload_dummy;
  tx_IFramePayloads   <= c_IFramePayload_zero;
  tx_CFramePayloads   <= c_CFramePayload_zero;

  -------------------------------- Register RX pulses for DIO -----------------------------
  -- Register the tx/rx valid signal at the output
  -- 1) rx_valid is registered to avoid gliches as it is combinatorial
  -- 2) tx_valid is registered to match the additional latency of a flip-flop
  p_register_txrx_valid: process (clk_sys_62m5)
  begin
    if rising_edge (clk_sys_62m5) then
      if rst_sys_62m5_n = '0' then
        rx_valid_ext <= '0';
      else
        rx_valid_ext <= rx_valid;
      end if;
    end if;
  end process;  -------------------------------------------------------------------------------------------
  -- other stuff
  -----------------------------------------------------------------------------------------
  -- enable JTAG
  FP_JTAG_ENA_N <= '0';

  -- make sure that switch-bar (M21141G-24) is disabled
  XBAR_SET_N   <= '1';
  XBAR_RST_N   <= '0';
  XBAR_CS_N    <= '1';
  XBAR_ADDR    <= (others => '0');
  XBAR_DS_N    <= '1';
  XBAR_RD      <= '1';
  XBAR_RSTRX_N <= "00";
  XBAR_DIS_N   <= '0';

  -- outputs
  FPGA_IO_E1_N <= '0'; -- enable output buffer of IO1 (active low)
  FPGA_IO_E2_N <= '0'; -- enable output buffer of IO2 (active low)
  FPGA_IO_Z1   <= '0'; -- disable terminatin 50 ohm termination (active high)
  FPGA_IO_Z2   <= '0'; -- disable terminatin 50 ohm termination (active high)
  FPGA_IO_OUT1 <= wrc_pps_out;
  FPGA_IO_OUT2 <= rx_valid_ext;

  -- FLASH
  FRAMWP_N <= '1'; -- not write protected

-- 
--   CS_ICON : chipscope_virtex5_icon
--     port map (
--       CONTROL0 => CONTROL0);
--   CS_ILA : chipscope_virtex5_ila
--     port map (
--       CONTROL => CONTROL0,
--       CLK     => clk_sys_62m5,
--       TRIG0   => TRIG0,
--       TRIG1   => TRIG1,
--       TRIG2   => TRIG2,
--       TRIG3   => TRIG3);
-- 
--   trig0(0)           <= onewire_data;
--   trig0(1)           <= led_link;
--   trig0(2)           <= led_ack;
--   trig0(3)           <= SFP3_PRSNT_N;
--   trig0(4)           <= SFP3_LOS;
--   trig0(5)           <= SFP3_TX_DIS_out;
--   trig0(6)           <= wrc_pps_out;
--   trig0(7)           <= wrc_pps_led;
-- 
--   ---- debug streamers
--   trig0(10)           <= rx_valid;
--   trig0(11)           <= rx_first_p1;
--   trig0(12)           <= rx_dreq;
--   trig0(13)           <= rx_last_p1;
-- 
--   trig1(31 downto 0) <= rx_data(31 downto 0);
-- 
--   ---- debug BTrain transceiver
--   trig0(20)          <= rx_Frame_valid_pX;
-- 
--   trig0(31 downto 24)<= rx_Frame_typeID;
--   trig2(31 downto 0) <= rx_BFramePayloads.B;
--   trig3(31 downto 0) <= rx_BFramePayloads.Bdot;

end architecture top;
