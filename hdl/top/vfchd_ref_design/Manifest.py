fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl/BTrainFrameTransceiver",
        "../../ip_cores/wr-cores", 
        "../../ip_cores/wr-cores/board/vfchd",
        "../../ip_cores/general-cores",
        "../../ip_cores/gn4124-core",
        "../../ip_cores/vme64x-core",        
        "../../ip_cores/etherbone-core",
    ],
}

files = [
    "../../rtl/BTrainFrame_pkg.vhd",
    "vfchd_btrain_ref_top.vhd",
    "vfchd_i2cmux/vfchd_i2cmux_pkg.vhd",
    "vfchd_i2cmux/I2cMuxAndExpReqArbiter.v",
    "vfchd_i2cmux/I2cMuxAndExpMaster.v",
    "vfchd_i2cmux/SfpIdReader.v",
]
