-------------------------------------------------------------------------------
-- Title      : BTrain reference design for VFC-HD
-- Project    : BTrain over White Rabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : vfchd_btrain_ref_top.vhd
-- Author(s)  : Dimitrios Lampridis  <dimitrios.lampridis@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Created    : 2017-04-07
-- Last update: 2020-04-09 by Maciej
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for the BTrain reference design on the VFC-HD.
--
-- This is the BTrain reference top HDL that instantiates the BTrain
-- transceiver, the WR streamers and the WR PTP Core together with its
-- peripherals, to be run on a VFC-HD card.
--
-- VFC-HD:  http://www.ohwr.org/projects/vfc-hd/
--
-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.vme64x_pkg.all;
use work.wr_board_pkg.all;
use work.wr_vfchd_pkg.all;
use work.vfchd_i2cmux_pkg.all;
use work.streamers_pkg.all;

use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;

entity vfchd_btrain_ref_top is
  generic
    (
      -- setting g_simulation to TRUE will speed up some initialization processes
      g_simulation  : integer := 0;
      -- setting g_dpram_initf to file path will result in syntesis/simulation using the
      -- content of this file to run LM32 microprocessor
      -- setting g_dpram_init to empty string (i.e."") will result in synthesis/simulation
      -- with empty RAM for the LM32 (it will not work until code is loaded)
      -- NOTE: the path is correct when used from the synthesis folder (this is where
      --       ISE calls the function to find the file, the path is not correct for where
      --       this file is stored, i.e. in the top/ folder)
      g_dpram_initf : string  := "../../ip_cores/wr-cores/bin/wrpc/wrc_phy8.mif"
      );
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------

    -- Clock inputs from the board
    clk_board_125m_i : in std_logic;
    clk_board_20m_i  : in std_logic;

    -- Reset input (active low, can be async)
    areset_n_i : in std_logic;

    ---------------------------------------------------------------------------
    -- VME interface
    ---------------------------------------------------------------------------

    vme_write_n_i   : in    std_logic;
    vme_lword_n_b   : inout std_logic;
    vme_iackout_n_o : out   std_logic;
    vme_iackin_n_i  : in    std_logic;
    vme_iack_n_i    : in    std_logic;
    vme_dtack_oe_o  : out   std_logic;
    vme_ds_n_i      : in    std_logic_vector(1 downto 0);
    vme_data_oe_n_o : out   std_logic;
    vme_data_dir_o  : out   std_logic;
    vme_as_n_i      : in    std_logic;
    vme_addr_oe_n_o : out   std_logic;
    vme_addr_dir_o  : out   std_logic;
    vme_irq_o       : out   std_logic_vector(7 downto 1);
    vme_data_b      : inout std_logic_vector(31 downto 0);
    vme_am_i        : in    std_logic_vector(5 downto 0);
    vme_addr_b      : inout std_logic_vector(31 downto 1);

    ---------------------------------------------------------------------------
    -- SPI interfaces to DACs
    ---------------------------------------------------------------------------

    dac_ref_sync_n_o  : out std_logic;
    dac_dmtd_sync_n_o : out std_logic;
    dac_din_o         : out std_logic;
    dac_sclk_o        : out std_logic;

    ---------------------------------------------------------------------------
    -- SPI interfaces to VFC Vadj and VADC
    ---------------------------------------------------------------------------

    --vfc_vadj_cs_n_o : out std_logic;
    --vfc_vadj_din_o  : out std_logic;
    --vfc_vadj_sck_o  : out std_logic;

    --vfc_vadc_cs_n_o : out std_logic;
    --vfc_vadc_din_o  : out std_logic;
    --vfc_vadc_dout_i : in  std_logic;
    --vfc_vadc_sck_o  : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/O for transceiver
    ---------------------------------------------------------------------------

    sfp_tx_o : out std_logic;
    sfp_rx_i : in  std_logic;

    ---------------------------------------------------------------------------
    -- VFC IO/I2C Mux
    ---------------------------------------------------------------------------

    i2c_mux_sda_b : inout std_logic;
    i2c_mux_scl_b : inout std_logic;

    io_exp_irq_bsteth_n_i : in std_logic;
    io_exp_irq_los_n_i    : in std_logic;

    ---------------------------------------------------------------------------
    -- I2C EEPROM
    ---------------------------------------------------------------------------

    eeprom_sda_b : inout std_logic;
    eeprom_scl_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- Onewire interface
    ---------------------------------------------------------------------------

    onewire_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- VFC GPIOs
    ---------------------------------------------------------------------------

    vfchd_gpio1_o : out std_logic;                -- tx_valid signal
    vfchd_gpio2_o : out std_logic;                -- rx_valid signal
    vfchd_gpio3_o : out std_logic;                -- WR PPS output
    vfchd_gpio4_o : out std_logic);               -- WR TM valid

end entity vfchd_btrain_ref_top;

architecture top of vfchd_btrain_ref_top is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- VME identification for the VFC-HD board (NOTE: it is not really used...)
  -- Board ID (see: https://boardid.web.cern.ch/boardid/script/singleDev.php?id=423)
  constant c_VFCHD_ID        : std_logic_vector(31 downto 0) := x"00000423";
  -- Revision ID (v3.1 is the newest)
  constant c_VFCHD_REV_ID    : std_logic_vector(31 downto 0) := x"00000003";
  -- Program ID (ASCI "B" for BTrain)
  constant c_VFCHD_PROGRAM_ID: std_logic_vector( 7 downto 0) := x"42"; 

  -- Number of masters on the primary wishbone crossbar
  constant c_NUM_WB1_MASTERS : integer := 1;

  -- Number of slaves on the primary wishbone crossbar
  constant c_NUM_WB1_SLAVES : integer := 4;

  -- Number of masters on the secondary wishbone crossbar
  constant c_NUM_WB2_MASTERS : integer := 2;

  -- Number of slaves on the secondary wishbone crossbar
  constant c_NUM_WB2_SLAVES : integer := 1;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_VME : integer := 0;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_I2CCFG : integer := 0;
  constant c_WB_SLAVE_SECOND : integer := 1;
  constant c_WB_SLAVE_WRC    : integer := 2;
  constant c_WB_SLAVE_BTRAIN : integer := 3;

  -- Secondary Wishbone master(s) offsets
  constant c_WB_MASTER_PRIM  : integer := 0;
  constant c_WB_MASTER_SFPID : integer := 1;

  -- Secondary Wishbone slave(s) offsets
  constant c_WB_SLAVE_I2CMUX : integer := 0;

  -- sdb header address on primary crossbar
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- SDB record for IO Exp configuration port
  constant c_xwb_i2ccfg_sdb : t_sdb_device := (
    abi_class     => x"0000",                     -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"1",                        -- 8-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000003",
      product     => (
        vendor_id => x"000000000000CE42",         -- CERN
        device_id => x"00008889",
        version   => x"00000001",
        date      => x"20170126",
        name      => "BE-BI I2C Mux Cfg  ")));

  constant c_xwb_btrain_sdb : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                        -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"00000000000000FF",
      product     => (
        vendor_id => x"000000000000CE42",         -- CERN
        device_id => x"00000604",
        version   => x"00000002",
        date      => x"20170418",
        name      => "BTrainFrameTxRx    ")));

  constant c_mux_bridge_sdb : t_sdb_bridge :=
    f_xwb_bridge_manual_sdb(x"00003fff", x"00000000");

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  -- Note: sdb_addr is the sdb records address relative to the bridge base address
  constant c_wrc_bridge_sdb : t_sdb_bridge :=
    f_xwb_bridge_manual_sdb(x"0003ffff", x"00030000");

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT : t_sdb_record_array(c_NUM_WB1_SLAVES - 1 downto 0) := (
    c_WB_SLAVE_I2CCFG => f_sdb_embed_device(c_xwb_i2ccfg_sdb, x"00001000"),
    c_WB_SLAVE_BTRAIN => f_sdb_embed_device(c_xwb_btrain_sdb, x"00001200"),
    c_WB_SLAVE_SECOND => f_sdb_embed_bridge(c_mux_bridge_sdb, x"00004000"),
    c_WB_SLAVE_WRC    => f_sdb_embed_bridge(c_wrc_bridge_sdb, x"00040000"));

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- Wishbone buse(s) from masters attached to primary crossbar
  signal cnx1_master_out : t_wishbone_master_out_array(c_NUM_WB1_MASTERS-1 downto 0);
  signal cnx1_master_in  : t_wishbone_master_in_array(c_NUM_WB1_MASTERS-1 downto 0);

  -- Wishbone buse(s) to slaves attached to primary crossbar
  signal cnx1_slave_out : t_wishbone_slave_out_array(c_NUM_WB1_SLAVES-1 downto 0);
  signal cnx1_slave_in  : t_wishbone_slave_in_array(c_NUM_WB1_SLAVES-1 downto 0);

  -- Wishbone buse(s) from masters attached to secondary crossbar
  signal cnx2_master_out : t_wishbone_master_out_array(c_NUM_WB2_MASTERS-1 downto 0);
  signal cnx2_master_in  : t_wishbone_master_in_array(c_NUM_WB2_MASTERS-1 downto 0);

  -- Wishbone buse(s) to slaves attached to secondary crossbar
  signal cnx2_slave_out : t_wishbone_slave_out_array(c_NUM_WB2_SLAVES-1 downto 0);
  signal cnx2_slave_in  : t_wishbone_slave_in_array(c_NUM_WB2_SLAVES-1 downto 0);

  -- clock and reset
  signal clk_sys_62m5   : std_logic;
  signal rst_sys_62m5   : std_logic;
  signal rst_sys_62m5_n : std_logic;
  signal rst_ref_125m_n : std_logic;
  signal clk_ref_125m   : std_logic;

  -- I2C EEPROM
  signal eeprom_sda_in  : std_logic;
  signal eeprom_sda_out : std_logic;
  signal eeprom_scl_in  : std_logic;
  signal eeprom_scl_out : std_logic;

  -- VME
  signal vme_data_b_out    : std_logic_vector(31 downto 0);
  signal vme_addr_b_out    : std_logic_vector(31 downto 1);
  signal vme_lword_n_b_out : std_logic;
  signal Vme_data_dir_int  : std_logic;
  signal vme_addr_dir_int  : std_logic;
  signal vme_dtack_n       : std_logic;
  signal vme_ga            : std_logic_vector(5 downto 0);
  signal vme_irq_n         : std_logic_vector(7 downto 1);

  -- SFP
  signal sfp_present    : std_logic;
  signal sfp_det_valid  : std_logic;
  signal sfp_data       : std_logic_vector (127 downto 0);
  signal sfp_wb_adr     : t_wishbone_address;
  signal sfp_tx_fault   : std_logic;
  signal sfp_los        : std_logic;
  signal sfp_tx_disable : std_logic;

  -- OneWire
  signal onewire_data : std_logic;
  signal onewire_oe   : std_logic;

  -- IO/I2C Mux
  signal io_exp_init_done : std_logic;
  signal io_exp_wr_req    : std_logic;
  signal io_exp_wr_on     : std_logic;
  signal io_exp_rd_req    : std_logic;
  signal io_exp_rd_on     : std_logic;
  signal io_exp_addr      : std_logic_vector(2 downto 0);
  signal io_exp_reg_addr  : std_logic_vector(1 downto 0);
  signal io_exp_data      : std_logic_vector(7 downto 0);
  signal i2c_slv_wr_req   : std_logic;
  signal i2c_slv_wr_on    : std_logic;
  signal i2c_slv_rd_req   : std_logic;
  signal i2c_slv_rd_on    : std_logic;
  signal i2c_slv_addr     : std_logic_vector(6 downto 0);
  signal i2c_slv_reg_addr : std_logic_vector(7 downto 0);
  signal i2c_slv_byte     : std_logic_vector(7 downto 0);
  signal i2c_mux_addr     : std_logic;
  signal i2c_mux_channel  : std_logic_vector(1 downto 0);
  signal i2c_mst_busy     : std_logic;
  signal i2c_mst_dav      : std_logic;
  signal i2c_mst_ack_err  : std_logic;
  signal i2c_mst_data     : std_logic_vector(7 downto 0);
  signal i2c_wb_adr       : std_logic_vector(11 downto 0);
  signal i2c_wb_dat_in    : std_logic_vector(7 downto 0);
  signal i2c_wb_dat_out   : std_logic_vector(7 downto 0);

  -- LEDs
  signal pps_led     : std_logic;
  signal pps_led_d   : std_logic;
  signal rxv_led_d   : std_logic;
  signal vfchd_led   : std_logic_vector(7 downto 0);
  signal wr_led_link : std_logic;
  signal wr_led_act  : std_logic;
  signal tx_valid_ext: std_logic;
  signal rx_valid_ext: std_logic;

  -- WR Streamers <---> BTrain
  signal tx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal tx_valid    : std_logic;
  signal tx_dreq     : std_logic;
  signal tx_last_p1  : std_logic;
  signal tx_flush_p1 : std_logic;
  signal tx_cfg      : t_tx_streamer_cfg;
  -- rx
  signal rx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal rx_valid    : std_logic;
  signal rx_first_p1 : std_logic;
  signal rx_dreq     : std_logic;
  signal rx_last_p1  : std_logic;
  signal rx_cfg      : t_rx_streamer_cfg;

  -- BTrain <---> BTrain application (e.g. on BTrainFMC)
  signal tx_ready            : std_logic;
  signal tx_TransmitFrame_p1 : std_logic;
  signal tx_FrameHeader    : t_FrameHeader;
  signal tx_BFramePayloads : t_BFramePayload;
  signal tx_IFramePayloads : t_IFramePayload;
  signal tx_CFramePayloads : t_CFramePayload;
  signal rx_FrameHeader      : t_FrameHeader;
  signal rx_BFramePayloads   : t_BFramePayload;
  signal rx_IFramePayloads   : t_IFramePayload;
  signal rx_CFramePayloads   : t_CFramePayload;
  signal rx_Frame_valid_pX   : std_logic;
  signal rx_Frame_typeID     : std_logic_vector(c_type_ID_size-1 downto 0);

  -- SIM <---> BTrain application
  signal sim_tx_FrameHeader_version_id   : std_logic_vector(1 downto 0);
  signal sim_tx_FrameHeader_d_low_marker : std_logic;
  signal sim_tx_FrameHeader_f_low_marker : std_logic;
  signal sim_tx_FrameHeader_zero_cycle   : std_logic;
  signal sim_tx_FrameHeader_C0           : std_logic;
  signal sim_tx_FrameHeader_error        : std_logic;
  signal sim_tx_FrameHeader_sim_eff      : std_logic;
  signal sim_tx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_tx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_tx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_tx_TransmitFrame_p1         : std_logic;
  signal sim_tx_CFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_V          : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_MSrcId     : std_logic_vector(15 downto 0);
  signal sim_tx_CFramePayload_CSrcId     : std_logic_vector(15 downto 0);
  signal sim_tx_CFramePayload_UserData   : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_Reserved_1 : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_Reserved_2 : std_logic_vector(31 downto 0);
  signal sim_tx_ready                    : std_logic;
  signal sim_rx_FrameHeader_version_id   : std_logic_vector(1 downto 0);
  signal sim_rx_FrameHeader_d_low_marker : std_logic;
  signal sim_rx_FrameHeader_f_low_marker : std_logic;
  signal sim_rx_FrameHeader_zero_cycle   : std_logic;
  signal sim_rx_FrameHeader_C0           : std_logic;
  signal sim_rx_FrameHeader_error        : std_logic;
  signal sim_rx_FrameHeader_sim_eff      : std_logic;
  signal sim_rx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_rx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_rx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_V          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_MSrcId     : std_logic_vector(15 downto 0);
  signal sim_rx_CFramePayload_CSrcId     : std_logic_vector(15 downto 0);
  signal sim_rx_CFramePayload_UserData   : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_Reserved_1 : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_Reserved_2 : std_logic_vector(31 downto 0);
  signal sim_rx_Frame_valid_pX           : std_logic;
  signal sim_rx_Frame_typeID             : std_logic_vector(c_type_ID_size-1 downto 0);

begin  -- architecture top

  rst_sys_62m5 <= not rst_sys_62m5_n;

  -----------------------------------------------------------------------------
  -- Primary wishbone Crossbar
  -----------------------------------------------------------------------------

  cmp_primary_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB1_MASTERS,
      g_num_slaves  => c_NUM_WB1_SLAVES,
      g_registered  => TRUE,
      g_wraparound  => TRUE,
      g_layout      => c_WB_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx1_master_out,
      slave_o   => cnx1_master_in,
      master_i  => cnx1_slave_out,
      master_o  => cnx1_slave_in);

  -----------------------------------------------------------------------------
  -- VME64x Core (WB Master #1)
  -----------------------------------------------------------------------------

  cmp_vme_core : xvme64x_core
    generic map (
      g_CLOCK_PERIOD    => 16,
      g_DECODE_AM       => True,
      g_USER_CSR_EXT    => False,
      g_WB_GRANULARITY  => BYTE,
      g_MANUFACTURER_ID => c_CERN_ID,
      g_BOARD_ID        => c_VFCHD_ID,         -- board id
      g_REVISION_ID     => c_VFCHD_REV_ID,     -- board revision id (V3)
      g_PROGRAM_ID      => c_VFCHD_PROGRAM_ID) -- board program id (BTrain)
    port map (
      clk_i           => clk_sys_62m5,
      rst_n_i         => rst_sys_62m5_n,
      vme_i.as_n      => vme_as_n_i,
      vme_i.rst_n     => io_exp_init_done, 
      vme_i.write_n   => vme_write_n_i,
      vme_i.am        => vme_am_i,
      vme_i.ds_n      => vme_ds_n_i,
      vme_i.ga        => vme_ga,
      vme_i.lword_n   => vme_lword_n_b,
      vme_i.addr      => vme_addr_b,
      vme_i.data      => vme_data_b,
      vme_i.iack_n    => vme_iack_n_i,
      vme_i.iackin_n  => vme_iackin_n_i,
      vme_o.berr_n    => open,
      vme_o.dtack_n   => vme_dtack_n,
      vme_o.retry_n   => open,
      vme_o.retry_oe  => open,
      vme_o.lword_n   => vme_lword_n_b_out,
      vme_o.data      => vme_data_b_out,
      vme_o.addr      => vme_addr_b_out,
      vme_o.irq_n     => vme_irq_n,
      vme_o.iackout_n => vme_iackout_n_o,
      vme_o.dtack_oe  => open,
      vme_o.data_dir  => vme_data_dir_int,
      vme_o.data_oe_n => vme_data_oe_n_o,
      vme_o.addr_dir  => vme_addr_dir_int,
      vme_o.addr_oe_n => vme_addr_oe_n_o,
      wb_o            => cnx1_master_out(c_WB_MASTER_VME),
      wb_i            => cnx1_master_in(c_WB_MASTER_VME));

  -- Handle DTACK according to VFC-HD hardware
  vme_dtack_oe_o <= not vme_dtack_n;
  vme_irq_o      <= not vme_irq_n;

  -- VME tri-state buffers
  vme_data_b    <= vme_data_b_out    when vme_data_dir_int = '1' else (others => 'Z');
  vme_addr_b    <= vme_addr_b_out    when vme_addr_dir_int = '1' else (others => 'Z');
  vme_lword_n_b <= vme_lword_n_b_out when vme_addr_dir_int = '1' else 'Z';

  vme_addr_dir_o <= vme_addr_dir_int;
  vme_data_dir_o <= vme_data_dir_int;

  -----------------------------------------------------------------------------
  -- The WR PTP core board package (WB Slave)
  -----------------------------------------------------------------------------

  cmp_xwrc_board_vfchd : xwrc_board_vfchd
    generic map (
      g_simulation                => g_simulation,
      g_with_external_clock_input => FALSE,
      g_dpram_initf               => g_dpram_initf,
      g_streamers_op_mode         => TX_AND_RX,
      g_tx_streamer_params        => c_tx_streamer_params_btrain,
      g_rx_streamer_params        => c_rx_streamer_params_btrain,
      g_fabric_iface              => STREAMERS)
    port map (
      clk_board_125m_i  => clk_board_125m_i,
      clk_board_20m_i   => clk_board_20m_i,
      areset_n_i        => areset_n_i,
      clk_sys_62m5_o    => clk_sys_62m5,
      clk_ref_125m_o    => clk_ref_125m,
      rst_sys_62m5_n_o  => rst_sys_62m5_n,
      rst_ref_125m_n_o  => rst_ref_125m_n,
      dac_ref_sync_n_o  => dac_ref_sync_n_o,
      dac_dmtd_sync_n_o => dac_dmtd_sync_n_o,
      dac_din_o         => dac_din_o,
      dac_sclk_o        => dac_sclk_o,
      sfp_tx_o          => sfp_tx_o,
      sfp_rx_i          => sfp_rx_i,
      sfp_det_valid_i   => sfp_det_valid,
      sfp_data_i        => sfp_data,
      sfp_tx_fault_i    => sfp_tx_fault,
      sfp_los_i         => sfp_los,
      sfp_tx_disable_o  => sfp_tx_disable,
      eeprom_sda_i      => eeprom_sda_in,
      eeprom_sda_o      => eeprom_sda_out,
      eeprom_scl_i      => eeprom_scl_in,
      eeprom_scl_o      => eeprom_scl_out,
      onewire_i         => onewire_data,
      onewire_oen_o     => onewire_oe,
      wb_slave_o        => cnx1_slave_out(c_WB_SLAVE_WRC),
      wb_slave_i        => cnx1_slave_in(c_WB_SLAVE_WRC),
      pps_p_o           => vfchd_gpio3_o,
      tm_time_valid_o   => vfchd_gpio4_o,
      pps_led_o         => pps_led,
      led_link_o        => wr_led_link,
      led_act_o         => wr_led_act,
      wrs_tx_data_i     => tx_data,
      wrs_tx_valid_i    => tx_valid,
      wrs_tx_dreq_o     => tx_dreq,
      wrs_tx_last_i     => tx_last_p1,
      wrs_tx_flush_i    => tx_flush_p1,
      wrs_tx_cfg_i      => tx_cfg,
      wrs_rx_first_o    => rx_first_p1,
      wrs_rx_last_o     => rx_last_p1,
      wrs_rx_data_o     => rx_data,
      wrs_rx_valid_o    => rx_valid,
      wrs_rx_dreq_i     => rx_dreq,
      wrs_rx_cfg_i      => rx_cfg
      );

  -- Configuration of streamers:
  -- 1) For synthesis : In Btrain deployments, by default, streamers use VID=0
  --    (i.e. priority tagging). 
  -- 2) For simulation: Using VLANs (VID=0) is problematic (requires special
  --    configuration). It is easier to disable VLANs for simulation.
  gen_streamers_cfg_sim: if (g_simulation>0) generate -- no VLAN
    rx_cfg <= c_rx_streamer_cfg_default;
    tx_cfg <= c_tx_streamer_cfg_default;
  end generate gen_streamers_cfg_sim;
  gen_streamers_cfg_syn: if (g_simulation=0) generate -- with VLAN(VID=0)
    rx_cfg <= c_rx_streamer_cfg_btrain;
    tx_cfg <= c_tx_streamer_cfg_btrain;
  end generate gen_streamers_cfg_syn;

  -- tri-state I2C EEPROM
  eeprom_sda_b  <= '0' when (eeprom_sda_out = '0') else 'Z';
  eeprom_sda_in <= eeprom_sda_b;

  eeprom_scl_b  <= '0' when (eeprom_scl_out = '0') else 'Z';
  eeprom_scl_in <= eeprom_scl_b;

  -- tri-state onewire access
  onewire_b    <= '0' when (onewire_oe = '1') else 'Z';
  onewire_data <= onewire_b;

  -------------------------------------------------------------------------------------------
  -- BTrain frames transceiver
  -------------------------------------------------------------------------------------------
  cmp_wr_btrain : BTrainFrameTransceiver
    generic map(
      g_rx_BframeType     => c_ID_ALL,  -- accept all types of BTrain frame - only debugging
      g_use_wb_config     => TRUE,
      g_slave_mode        => CLASSIC,
      g_slave_granularity => BYTE)
    port map(
      clk_i   => clk_sys_62m5,
      rst_n_i => rst_sys_62m5_n,

      tx_data_o     => tx_data,
      tx_valid_o    => tx_valid,
      tx_dreq_i     => tx_dreq,
      tx_last_p1_o  => tx_last_p1,
      tx_flush_p1_o => tx_flush_p1,
      -- rx
      rx_data_i     => rx_data,
      rx_valid_i    => rx_valid,
      rx_first_p1_i => rx_first_p1,
      rx_dreq_o     => rx_dreq,
      rx_last_p1_i  => rx_last_p1,

      rx_FrameHeader_o    => rx_FrameHeader,
      rx_BFramePayloads_o => rx_BFramePayloads,
      rx_IFramePayloads_o => rx_IFramePayloads,
      rx_CframePayloads_o => rx_CFramePayloads,
      rx_Frame_valid_pX_o => rx_Frame_valid_pX,
      rx_Frame_typeID_o   => rx_Frame_typeID,

      ready_o               => tx_ready,
      tx_TransmitFrame_p1_i => tx_TransmitFrame_p1,
      tx_FrameHeader_i      => tx_FrameHeader,
      tx_BFramePayloads_i   => tx_BFramePayloads,
      tx_IFramePayloads_i   => tx_IFramePayloads,
      tx_CFramePayloads_i   => tx_CFramePayloads,

      wb_slave_i => cnx1_slave_in(c_WB_SLAVE_BTRAIN),
      wb_slave_o => cnx1_slave_out(c_WB_SLAVE_BTRAIN)
      );

  --fake data
  gen_fake_data: if g_simulation = 0 generate
    tx_TransmitFrame_p1 <= '0';
    tx_FrameHeader      <= c_FrameHeader_dummy;
    tx_BFramePayloads   <= c_BFramePayload_dummy;
    tx_IFramePayloads   <= c_IFramePayload_zero;
    tx_CFramePayloads   <= c_CFramePayload_zero;
  end generate gen_fake_data;

  -- Simulation connection points. Since binding to VHDL records in SV is not well-defined,
  -- we break the records into std_logic signals.
  gen_sim_bind : if g_simulation = 1 generate
    tx_FrameHeader.version_id   <= sim_tx_FrameHeader_version_id;
    tx_FrameHeader.d_low_marker <= sim_tx_FrameHeader_d_low_marker;
    tx_FrameHeader.f_low_marker <= sim_tx_FrameHeader_f_low_marker;
    tx_FrameHeader.zero_cycle   <= sim_tx_FrameHeader_zero_cycle;
    tx_FrameHeader.C0           <= sim_tx_FrameHeader_C0;
    tx_FrameHeader.error        <= sim_tx_FrameHeader_error;
    tx_FrameHeader.sim_eff      <= sim_tx_FrameHeader_sim_eff;
    tx_FrameHeader.frame_type   <= sim_tx_FrameHeader_frame_type;
    tx_BFramePayloads.B         <= sim_tx_BFramePayload_B;
    tx_BFramePayloads.Bdot      <= sim_tx_BFramePayload_Bdot;
    tx_BFramePayloads.oldB      <= sim_tx_BFramePayload_oldB;
    tx_BFramePayloads.measB     <= sim_tx_BFramePayload_measB;
    tx_BFramePayloads.simB      <= sim_tx_BFramePayload_simB;
    tx_BFramePayloads.synB      <= sim_tx_BFramePayload_synB;
    tx_IFramePayloads.I         <= sim_tx_IFramePayload_I;
    tx_TransmitFrame_p1         <= sim_tx_TransmitFrame_p1;
 tx_CFramePayloads.I         <= sim_tx_CFramePayload_I;
    tx_CFramePayloads.V         <= sim_tx_CFramePayload_V;
    tx_CFramePayloads.MSrcId    <= sim_tx_CFramePayload_MSrcId;
    tx_CFramePayloads.CSrcId    <= sim_tx_CFramePayload_CSrcId;
    tx_CFramePayloads.UserData  <= sim_tx_CFramePayload_UserData;
    tx_CFramePayloads.Reserved_1<= sim_tx_CFramePayload_Reserved_1;
    tx_CFramePayloads.Reserved_2<= sim_tx_CFramePayload_Reserved_2;
    sim_tx_ready                <= tx_ready;

    sim_rx_FrameHeader_version_id   <= rx_FrameHeader.version_id;
    sim_rx_FrameHeader_d_low_marker <= rx_FrameHeader.d_low_marker;
    sim_rx_FrameHeader_f_low_marker <= rx_FrameHeader.f_low_marker;
    sim_rx_FrameHeader_zero_cycle   <= rx_FrameHeader.zero_cycle;
    sim_rx_FrameHeader_C0           <= rx_FrameHeader.C0;
    sim_rx_FrameHeader_error        <= rx_FrameHeader.error;
    sim_rx_FrameHeader_sim_eff      <= rx_FrameHeader.sim_eff;
    sim_rx_FrameHeader_frame_type   <= rx_FrameHeader.frame_type;
    sim_rx_BFramePayload_B          <= rx_BFramePayloads.B;
    sim_rx_BFramePayload_Bdot       <= rx_BFramePayloads.Bdot;
    sim_rx_BFramePayload_oldB       <= rx_BFramePayloads.oldB;
    sim_rx_BFramePayload_measB      <= rx_BFramePayloads.measB;
    sim_rx_BFramePayload_simB       <= rx_BFramePayloads.simB;
    sim_rx_BFramePayload_synB       <= rx_BFramePayloads.synB;
    sim_rx_IFramePayload_I          <= rx_IFramePayloads.I;
    sim_rx_CFramePayload_I          <= rx_CFramePayloads.I;
    sim_rx_CFramePayload_V          <= rx_CFramePayloads.V;
    sim_rx_CFramePayload_MSrcId     <= rx_CFramePayloads.MSrcId;
    sim_rx_CFramePayload_CSrcId     <= rx_CFramePayloads.CSrcId;
    sim_rx_CFramePayload_UserData   <= rx_CFramePayloads.UserData;
    sim_rx_CFramePayload_Reserved_1 <= rx_CFramePayloads.Reserved_1;
    sim_rx_CFramePayload_Reserved_2 <= rx_CFramePayloads.Reserved_2;
    sim_rx_Frame_valid_pX           <= rx_Frame_valid_pX;
    sim_rx_Frame_typeID             <= rx_Frame_typeID;
  end generate gen_sim_bind;

  -----------------------------------------------------------------------------
  -- VFCHD I2C MUX and Arbiter
  -- Presents two WB Slave ports, one to primary crossbar, one to secondary
  -----------------------------------------------------------------------------

  cmp_secondary_crossbar : xwb_crossbar
    generic map (
      g_num_masters => c_NUM_WB2_MASTERS,
      g_num_slaves  => c_NUM_WB2_SLAVES,
      g_registered  => TRUE,
      g_address     => (0 => (others => '0')),
      g_mask        => (0 => (others => '0')))
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx2_master_out,
      slave_o   => cnx2_master_in,
      master_i  => cnx2_slave_out,
      master_o  => cnx2_slave_in);

  -- link with primary crossbar
  cnx1_slave_out(c_WB_SLAVE_SECOND) <= cnx2_master_in(c_WB_MASTER_PRIM);
  cnx2_master_out(c_WB_MASTER_PRIM) <= cnx1_slave_in(c_WB_SLAVE_SECOND);

  cmp_SfpIdReader : SfpIdReader
    generic map (
      -- g_SfpWbBaseAddress is 0x1a00.
      -- X"1000" for crossbar (x"4000" >> 2 because of adapter later on)
      -- X"0a00" from I2cMuxAndExpReqArbiter
      g_SfpWbBaseAddress => 6656,
      g_WbAddrWidth      => c_wishbone_address_width)
    port map (
      Clk_ik       => clk_sys_62m5,
      SfpPlugged_i => sfp_present,
      SfpIdValid_o => sfp_det_valid,
      SfpPN_b128   => sfp_data,
      WbCyc_o      => cnx2_master_out(c_WB_MASTER_SFPID).cyc,
      WbStb_o      => cnx2_master_out(c_WB_MASTER_SFPID).stb,
      WbAddr_ob    => sfp_wb_adr,
      WbData_ib8   => cnx2_master_in(c_WB_MASTER_SFPID).dat(7 downto 0),
      WbAck_i      => cnx2_master_in(c_WB_MASTER_SFPID).ack);

  -- address adapter needed to properly access each byte in the I2CMux
  cnx2_master_out(c_WB_MASTER_SFPID).adr <= sfp_wb_adr (29 downto 0) & "00";

  -- Drive unused signals
  cnx2_master_out(c_WB_MASTER_SFPID).dat <= (others => '0');
  cnx2_master_out(c_WB_MASTER_SFPID).sel <= (others => '1');
  cnx2_master_out(c_WB_MASTER_SFPID).we  <= '0';

  cmp_I2cExpAndMuxReqArbiter : I2cExpAndMuxReqArbiter
    port map (
      Clk_ik                     => clk_sys_62m5,
      Rst_irq                    => rst_sys_62m5,
      IoExpWrReq_oq              => io_exp_wr_req,
      IoExpWrOn_i                => io_exp_wr_on,
      IoExpRdReq_oq              => io_exp_rd_req,
      IoExpRdOn_i                => io_exp_rd_on,
      IoExpAddr_oqb3             => io_exp_addr,
      IoExpRegAddr_oqb2          => io_exp_reg_addr,
      IoExpData_oqb8             => io_exp_data,
      I2cSlaveWrReq_oq           => i2c_slv_wr_req,
      I2cSlaveWrOn_i             => i2c_slv_wr_on,
      I2cSlaveRdReq_oq           => i2c_slv_rd_req,
      I2cSlaveRdOn_i             => i2c_slv_rd_on,
      I2cMuxAddress_oq           => i2c_mux_addr,
      I2cMuxChannel_oqb2         => i2c_mux_channel,
      I2cSlaveAddr_oqb7          => i2c_slv_addr,
      I2cSlaveRegAddr_oqb8       => i2c_slv_reg_addr,
      I2cSlaveByte_oqb8          => i2c_slv_byte,
      MasterBusy_i               => i2c_mst_busy,
      MasterNewByteRead_ip       => i2c_mst_dav,
      MasterByteOut_ib8          => i2c_mst_data,
      MasterAckError_i           => i2c_mst_ack_err,
      IoExpApp12Int_ian          => '1',
      IoExpApp34Int_ian          => '1',
      IoExpBstEthInt_ian         => io_exp_irq_bsteth_n_i,
      IoExpLosInt_ian            => io_exp_irq_los_n_i,
      IoExpBlmInInt_ian          => '1',
      InitDone_oq                => io_exp_init_done,
      VmeGa_onqb5                => vme_ga(4 downto 0),
      VmeGaP_onq                 => vme_ga(5),
      Led_ib8                    => vfchd_led,
      StatusLed_ob8              => open,
      GpIo1A2B_i                 => '1',
      EnGpIo1Term_i              => '0',
      GpIo2A2B_i                 => '1',
      EnGpIo2Term_i              => '0',
      GpIo34A2B_i                => '1',
      EnGpIo3Term_i              => '0',
      EnGpIo4Term_i              => '0',
      StatusGpIo1A2B_oq          => open,
      StatusEnGpIo1Term_oq       => open,
      StatusGpIo2A2B_oq          => open,
      StatusEnGpIo2Term_oq       => open,
      StatusGpIo34A2B_oq         => open,
      StatusEnGpIo3Term_oq       => open,
      StatusEnGpIo4Term_oq       => open,
      BlmIn_oqb8                 => open,
      AppSfp1Present_oq          => open,
      AppSfp1Id_oq16             => open,
      AppSfp1TxFault_oq          => open,
      AppSfp1Los_oq              => open,
      AppSfp1TxDisable_i         => '0',
      AppSfp1RateSelect_i        => '0',
      StatusAppSfp1TxDisable_oq  => open,
      StatusAppSfp1RateSelect_oq => open,
      AppSfp2Present_oq          => open,
      AppSfp2Id_oq16             => open,
      AppSfp2TxFault_oq          => open,
      AppSfp2Los_oq              => open,
      AppSfp2TxDisable_i         => '0',
      AppSfp2RateSelect_i        => '0',
      StatusAppSfp2TxDisable_oq  => open,
      StatusAppSfp2RateSelect_oq => open,
      AppSfp3Present_oq          => open,
      AppSfp3Id_oq16             => open,
      AppSfp3TxFault_oq          => open,
      AppSfp3Los_oq              => open,
      AppSfp3TxDisable_i         => '0',
      AppSfp3RateSelect_i        => '0',
      StatusAppSfp3TxDisable_oq  => open,
      StatusAppSfp3RateSelect_oq => open,
      AppSfp4Present_oq          => open,
      AppSfp4Id_oq16             => open,
      AppSfp4TxFault_oq          => open,
      AppSfp4Los_oq              => open,
      AppSfp4TxDisable_i         => '0',
      AppSfp4RateSelect_i        => '0',
      StatusAppSfp4TxDisable_oq  => open,
      StatusAppSfp4RateSelect_oq => open,
      BstSfpPresent_oq           => open,
      BstSfpId_oq16              => open,
      BstSfpTxFault_oq           => open,
      BstSfpLos_oq               => open,
      BstSfpTxDisable_i          => '0',
      BstSfpRateSelect_i         => '0',
      StatusBstSfpTxDisable_oq   => open,
      StatusBstSfpRateSelect_oq  => open,
      EthSfpPresent_oq           => sfp_present,
      EthSfpId_oq16              => open,
      EthSfpTxFault_oq           => sfp_tx_fault,
      EthSfpLos_oq               => sfp_los,
      EthSfpTxDisable_i          => sfp_tx_disable,
      EthSfpRateSelect_i         => '1',
      StatusEthSfpTxDisable_oq   => open,         -- TODO
      StatusEthSfpRateSelect_oq  => open,
      CdrLos_oq                  => open,
      CdrLol_oq                  => open,
      I2cWbCyc_i                 => cnx2_slave_in(c_WB_SLAVE_I2CMUX).cyc,
      I2cWbStb_i                 => cnx2_slave_in(c_WB_SLAVE_I2CMUX).stb,
      I2cWbWe_i                  => cnx2_slave_in(c_WB_SLAVE_I2CMUX).we,
      I2cWbAdr_ib12              => i2c_wb_adr,
      I2cWbDat_ib8               => i2c_wb_dat_in,
      I2cWbDat_ob8               => i2c_wb_dat_out,
      I2cWbAck_o                 => cnx2_slave_out(c_WB_SLAVE_I2CMUX).ack,
      WbCyc_i                    => cnx1_slave_in(c_WB_SLAVE_I2CCFG).cyc,
      WbStb_i                    => cnx1_slave_in(c_WB_SLAVE_I2CCFG).stb,
      WbWe_i                     => cnx1_slave_in(c_WB_SLAVE_I2CCFG).we,
      WbDat_ib32                 => cnx1_slave_in(c_WB_SLAVE_I2CCFG).dat,
      WbDat_oqb32                => cnx1_slave_out(c_WB_SLAVE_I2CCFG).dat,
      WbAck_oa                   => cnx1_slave_out(c_WB_SLAVE_I2CCFG).ack);

  -- Adjust WB interface to VFCHD I2CMux expectations and drive unused signals
  -- Adr(13 downto 10) are used to select the I2C peripheral in the mux
  -- Adr(9 downto 2) are the I2C address of that peripheral
  -- Adr(1 downto 0) are dropped (to allow access to the individual bytes
  -- since I2CMux only provides an 8-bit output)
  i2c_wb_adr    <= cnx2_slave_in(c_WB_SLAVE_I2CMUX).adr(13 downto 2);
  i2c_wb_dat_in <= cnx2_slave_in(c_WB_SLAVE_I2CMUX).dat(7 downto 0);

  cnx2_slave_out(c_WB_SLAVE_I2CMUX).dat   <= X"000000" & i2c_wb_dat_out;
  cnx2_slave_out(c_WB_SLAVE_I2CMUX).err   <= '0';
  cnx2_slave_out(c_WB_SLAVE_I2CMUX).rty   <= '0';
  cnx2_slave_out(c_WB_SLAVE_I2CMUX).stall <= not cnx2_slave_out(c_WB_SLAVE_I2CMUX).ack and
                                             (cnx2_slave_in(c_WB_SLAVE_I2CMUX).stb and
                                              cnx2_slave_in(c_WB_SLAVE_I2CMUX).cyc);

  cnx1_slave_out(c_WB_SLAVE_I2CCFG).err   <= '0';
  cnx1_slave_out(c_WB_SLAVE_I2CCFG).rty   <= '0';
  cnx1_slave_out(c_WB_SLAVE_I2CCFG).stall <= not cnx1_slave_out(c_WB_SLAVE_I2CCFG).ack and
                                             (cnx1_slave_in(c_WB_SLAVE_I2CCFG).stb and
                                              cnx1_slave_in(c_WB_SLAVE_I2CCFG).cyc);

  cmp_I2cExpAndMuxMaster : I2cExpAndMuxMaster
    generic map (
      g_SclHalfPeriod => "0010100000")            -- 10'd160
    port map (
      Clk_ik              => clk_sys_62m5,
      Rst_irq             => rst_sys_62m5,
      IoExpWrReq_i        => io_exp_wr_req,
      IoExpWrOn_oq        => io_exp_wr_on,
      IoExpRdReq_i        => io_exp_rd_req,
      IoExpRdOn_oq        => io_exp_rd_on,
      IoExpAddr_ib3       => io_exp_addr,
      IoExpRegAddr_ib2    => io_exp_reg_addr,
      IoExpData_ib8       => io_exp_data,
      I2cSlaveWrReq_i     => i2c_slv_wr_req,
      I2cSlaveWrOn_o      => i2c_slv_wr_on,
      I2cSlaveRdReq_i     => i2c_slv_rd_req,
      I2cSlaveRdOn_o      => i2c_slv_rd_on,
      I2cMuxAddress_i     => i2c_mux_addr,
      I2cMuxChannel_ib2   => i2c_mux_channel,
      I2cSlaveAddr_ib7    => i2c_slv_addr,
      I2cSlaveRegAddr_ib8 => i2c_slv_reg_addr,
      I2cSlaveByte_ib8    => i2c_slv_byte,
      Busy_o              => i2c_mst_busy,
      NewByteRead_op      => i2c_mst_dav,
      ByteOut_ob8         => i2c_mst_data,
      AckError_op         => i2c_mst_ack_err,
      Scl_ioz             => i2c_mux_scl_b,
      Sda_ioz             => i2c_mux_sda_b);

  -------------------------------- Register TX/RX pulses for DIO -----------------------------
  -- Register the tx/rx valid signal at the output
  -- 1) rx_valid is registered to avoid gliches as it is combinatorial
  -- 2) tx_valid is registered to match the additional latency of a flip-flop
  p_register_txrx_valid: process (clk_sys_62m5)
  begin
    if rising_edge (clk_sys_62m5) then
      if rst_sys_62m5_n = '0' then
        tx_valid_ext <= '0';
        rx_valid_ext <= '0';
      else
        tx_valid_ext <= tx_valid;
        rx_valid_ext <= rx_valid;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- VFC GPIO and LEDs
  -----------------------------------------------------------------------------

  vfchd_gpio1_o <= tx_valid_ext;
  vfchd_gpio2_o <= rx_valid_ext;

  -- pps_p signal from the WR core is 8ns- (single clk_ref cycle) wide. This is
  -- too short to drive outputs such as LEDs. Let's extend its length to some
  -- human-noticeable value
  U_Extend_PPS : gc_extend_pulse
    generic map (
      g_width => 10000000)  -- output length: 10000000x8ns = 80 ms.

    port map (
      clk_i      => clk_ref_125m,
      rst_n_i    => rst_ref_125m_n,
      pulse_i    => pps_led,
      extended_o => pps_led_d);

  U_Extend_RX_VALID : gc_extend_pulse
    generic map (
      g_width => 5000000)  -- output length: 5000000x16ns = 80 ms.

    port map (
      clk_i      => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      pulse_i    => rx_valid,
      extended_o => rxv_led_d);

  -- assign LEDs
  vfchd_led <= (0      => wr_led_link,
                3      => pps_led_d,
                4      => wr_led_act,
                7      => rxv_led_d,
                others => '0');

end architecture top;
