-------------------------------------------------------------------------------
-- Title      : BTrain reference design for SPEC
-- Project    : BTrain over White Rabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : spec_fixed_latency_top.vhd
-- Author(s)  : Maciej Lipinski <maciej.lipinski@cern.ch>
--              Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Created    : 2017-04-12
-- Last update: 2017-05-09
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for the BTrain reference design on the SPEC.
--
-- This is the BTrain reference top HDL that instantiates the BTrain
-- transceiver, the WR streamers and the WR PTP Core together with its
-- peripherals, to be run on a SPEC card.
--
-- SPEC:  http://www.ohwr.org/projects/spec/
--
-- This top entity contains the following main components:
-- 1)xwrc_board_spec - The WR PTP core board package (WB Slave) that includes:
--   a) wr_core, wr_gtp_phy_spartan6 and PLLs (These blocks are needed for the White
--      Rabbit to work):
--      * reception/transmission of Ethernet frames
--      * sub-ns synchronisation
--   b) xwr_transmission (This includes the wr_streamers, its diagnostics and statistics)
--      this module:
--      * receives and transmits streams of data. A pair of reception and
--        transmission stream modules work like FIFO over Ethernet. The data
--        written to the transmission streamer is packed into etherent frames,
--        sent over the netwrok and unpacked at the reception
--      * collects statistics regarding the streamer's transmission, i.e.
--        -> max, min, avg latency of the streamed data,
--        -> count of transmitted, received, lost frames
--        -> timestamp of rest
--        Access to the streamer's statistics is possible from the host and from a
--        remote network manager via SNMP through WRPC.
-- 2) BTrain - BTrain frames transceiver that encodes/decodes BTrain frames
--    * it receives BTrain-specific data from one side and it provides to the
--      streamers a formated payload for wr_streamers to sent on the other side, and
--    * vice versa, it receives BTrain frame and decods it into
--      provides BTrain-specific data
-- 3) gn4124_core    - GN4124, PCIe bridge core
-- 4) xwb_sdb_crossbar - wishbone crossbar to interconnect the components

-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.wr_board_pkg.all;
use work.wr_spec_pkg.all;
use work.gn4124_core_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;
use work.streamers_pkg.all;
use work.BTrainFrame_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity spec_fixed_latency_ref_top is
  generic (
    -- setting g_dpram_initf to file path will result in syntesis/simulation using the
    -- content of this file to run LM32 microprocessor
    -- setting g_dpram_init to empty string (i.e."") will result in synthesis/simulation
    -- with empty RAM for the LM32 (it will not work until code is loaded)
    -- NOTE: the path is correct when used from the synthesis folder (this is where
    --       ISE calls the function to find the file, the path is not correct for where
    --       this file is stored, i.e. in the top/ folder)
    g_dpram_initf : string  := "../../ip_cores/wr-cores/bin/wrpc/wrc_phy8.bram";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation : integer := 0
  );
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------

    -- Local oscillators
    clk_20m_vcxo_i : in std_logic;                -- 20MHz VCXO clock

    clk_125m_pllref_p_i : in std_logic;           -- 125 MHz PLL reference
    clk_125m_pllref_n_i : in std_logic;

    clk_125m_gtp_n_i : in std_logic;              -- 125 MHz GTP reference
    clk_125m_gtp_p_i : in std_logic;

    ---------------------------------------------------------------------------
    -- GN4124 PCIe bridge signals
    ---------------------------------------------------------------------------
    -- From GN4124 Local bus
    gn_rst_n_i : in std_logic;                           -- Reset from GN4124 (RSTOUT18_N)

    -- PCIe to Local [Inbound Data]                      -- RX:
    gn_p2l_clk_n_i  : in  std_logic;                     -- Receiver Source Synchronous Clock-
    gn_p2l_clk_p_i  : in  std_logic;                     -- Receiver Source Synchronous Clock+
    gn_p2l_rdy_o    : out std_logic;                     -- Rx Buffer Full Flag
    gn_p2l_dframe_i : in  std_logic;                     -- Receive Frame
    gn_p2l_valid_i  : in  std_logic;                     -- Receive Data Valid
    gn_p2l_data_i   : in  std_logic_vector(15 downto 0); -- Parallel receive data
    -- Inbound Buffer Request/Status
    gn_p_wr_req_i   : in  std_logic_vector(1 downto 0);  -- PCIe Write Request
    gn_p_wr_rdy_o   : out std_logic_vector(1 downto 0);  -- PCIe Write Ready
    gn_rx_error_o   : out std_logic;                     -- Receive Error
    -- Local to Parallel [Outbound Data]                 -- TX:
    gn_l2p_clkn_o   : out std_logic;                     -- Transmitter Source Synchronous Clock-
    gn_l2p_clkp_o   : out std_logic;                     -- Transmitter Source Synchronous Clock+
    gn_l2p_dframe_o : out std_logic;                     -- Transmit Data Frame
    gn_l2p_valid_o  : out std_logic;                     -- Transmit Data Valid
    gn_l2p_edb_o    : out std_logic;                     -- Packet termination and discard
    gn_l2p_data_o   : out std_logic_vector(15 downto 0); -- Parallel transmit data
    -- Outbound Buffer Status
    gn_l2p_rdy_i    : in std_logic;                      -- Tx Buffer Full Flag
    gn_l_wr_rdy_i   : in std_logic_vector(1 downto 0);   -- Local-to-PCIe Write
    gn_p_rd_d_rdy_i : in std_logic_vector(1 downto 0);   -- PCIe-to-Local Read Response Data Ready
    gn_tx_error_i   : in std_logic;                      -- Transmit Error
    gn_vc_rdy_i     : in std_logic_vector(1 downto 0);   -- Channel ready
    -- General Purpose Interface
    gn_gpio_b       : inout std_logic_vector(1 downto 0);-- gn_gpio_b[0] -> GN4124 GPIO8
                                                         -- gn_gpio_b[1] -> GN4124 GPIO9
    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------

    plldac_sclk_o     : out std_logic;
    plldac_din_o      : out std_logic;
    pll25dac_cs_n_o : out std_logic; --cs1
    pll20dac_cs_n_o : out std_logic; --cs2

    ---------------------------------------------------------------------------
    -- SFP I/O for transceiver
    ---------------------------------------------------------------------------

    sfp_txp_o         : out   std_logic;
    sfp_txn_o         : out   std_logic;
    sfp_rxp_i         : in    std_logic;
    sfp_rxn_i         : in    std_logic;
    sfp_mod_def0_i    : in    std_logic;          -- sfp detect
    sfp_mod_def1_b    : inout std_logic;          -- scl
    sfp_mod_def2_b    : inout std_logic;          -- sda
    sfp_rate_select_o : out   std_logic;
    sfp_tx_fault_i    : in    std_logic;
    sfp_tx_disable_o  : out   std_logic;
    sfp_los_i         : in    std_logic;

    ---------------------------------------------------------------------------
    -- Onewire interface
    ---------------------------------------------------------------------------

    onewire_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------

    uart_rxd_i : in  std_logic;
    uart_txd_o : out std_logic;

    ---------------------------------------------------------------------------
    -- Flash memory SPI interface
    ---------------------------------------------------------------------------

    flash_sclk_o : out std_logic;
    flash_ncs_o  : out std_logic;
    flash_mosi_o : out std_logic;
    flash_miso_i : in  std_logic;

    ---------------------------------------------------------------------------
    -- Miscellanous SPEC pins
    ---------------------------------------------------------------------------
    -- Red LED next to the SFP: blinking indicates that packets are being
    -- transferred.
    led_act_o   : out std_logic;
    -- Green LED next to the SFP: indicates if the link is up.
    led_link_o : out std_logic;

    button1_n_i            : in    std_logic := '1';
    button2_n_i            : in    std_logic := '1';

    ---------------------------------------------------------------------------
    -- Digital I/O FMC Pins
    -- used in this design to output WR-aligned 1-PPS (in Slave mode)
    ---------------------------------------------------------------------------

    -- Differential inputs, dio_p_i(N) inputs the current state of I/O (N+1) on
    -- the mezzanine front panel.
    dio_n_i : in std_logic_vector(4 downto 0);
    dio_p_i : in std_logic_vector(4 downto 0);

    -- Differential outputs. When the I/O (N+1) is configured as output (i.e. when
    -- dio_oe_n_o(N) = 0), the value of dio_p_o(N) determines the logic state
    -- of I/O (N+1) on the front panel of the mezzanine
    dio_n_o : out std_logic_vector(4 downto 0);
    dio_p_o : out std_logic_vector(4 downto 0);

    -- Output enable. When dio_oe_n_o(N) is 0, connector (N+1) on the front
    -- panel is configured as an output.
    dio_oe_n_o    : out std_logic_vector(4 downto 0);

    -- Termination enable. When dio_term_en_o(N) is 1, connector (N+1) on the front
    -- panel is 50-ohm terminated
    dio_term_en_o : out std_logic_vector(4 downto 0);

    -- Two LEDs on the mezzanine panel. Only Top one is currently used - to
    -- blink 1-PPS.
    dio_led_top_o : out std_logic;
    dio_led_bot_o : out std_logic;

    -- I2C interface for accessing FMC EEPROM. Deprecated, was used in
    -- pre-v3.0 releases to store WRPC configuration. Now we use Flash for this.
    dio_scl_b : inout std_logic;
    dio_sda_b : inout std_logic

  );
end entity spec_fixed_latency_ref_top;

architecture top of spec_fixed_latency_ref_top is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- Number of masters on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 1;

  -- Number of slaves on the primary wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 2;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_PCIE    : integer := 0;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_WRC : integer := 0;
  constant c_WB_SLAVE_BTRAIN : integer := 1;

  -- sdb header address on primary crossbar
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  constant c_xwb_btrain_sdb : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                        -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"00000000000000FF",
      product     => (
        vendor_id => x"000000000000CE42",         -- CERN
        device_id => x"00000604",
        version   => x"00000002",
        date      => x"20170418",
        name      => "BTrainFrameTxRx    ")));

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  -- Note: sdb_addr is the sdb records address relative to the bridge base address
  constant c_wrc_bridge_sdb : t_sdb_bridge :=
    f_xwb_bridge_manual_sdb(x"0003ffff", x"00030000");

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT : t_sdb_record_array(c_NUM_WB_SLAVES - 1 downto 0) := (
    c_WB_SLAVE_BTRAIN => f_sdb_embed_device(c_xwb_btrain_sdb, x"00001200"),
    c_WB_SLAVE_WRC    => f_sdb_embed_bridge(c_wrc_bridge_sdb, x"00040000"));

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- Wishbone buse(s) from masters attached to crossbar
  signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  -- Wishbone buse(s) to slaves attached to crossbar
  signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- Gennum signals
  signal gn_wbadr : std_logic_vector(31 downto 0);

  -- clock and reset
  signal areset_n       : std_logic;
  signal clk_sys_62m5   : std_logic;
  signal rst_sys_62m5_n : std_logic;
  signal rst_ref_125m_n : std_logic;
  signal clk_ref_125m   : std_logic;

  -- I2C EEPROM
  signal eeprom_sda_in  : std_logic;
  signal eeprom_sda_out : std_logic;
  signal eeprom_scl_in  : std_logic;
  signal eeprom_scl_out : std_logic;

  -- SFP
  signal sfp_sda_in  : std_logic;
  signal sfp_sda_out : std_logic;
  signal sfp_scl_in  : std_logic;
  signal sfp_scl_out : std_logic;

  -- OneWire
  signal onewire_data : std_logic;
  signal onewire_oe   : std_logic;

  -- LEDs and GPIO
  signal wrc_pps_out : std_logic;
  signal wrc_pps_led : std_logic;
  signal wrc_pps_in  : std_logic;
  signal tx_valid_ext: std_logic;
  signal rx_valid_ext: std_logic;

  -- DIO Mezzanine
  signal dio_in  : std_logic_vector(4 downto 0);
  signal dio_out : std_logic_vector(4 downto 0);

  -- WR Streamers <---> BTrain
  signal tx_data     : std_logic_vector(c_rx_streamer_params_btrain.data_width-1 downto 0);
  signal tx_valid    : std_logic;
  signal tx_dreq     : std_logic;
  signal tx_last_p1  : std_logic;
  signal tx_flush_p1 : std_logic;
  signal tx_cfg      : t_tx_streamer_cfg;
  -- rx
  signal rx_data     : std_logic_vector(c_tx_streamer_params_btrain.data_width-1 downto 0);
  signal rx_valid    : std_logic;
  signal rx_first_p1 : std_logic;
  signal rx_dreq     : std_logic;
  signal rx_last_p1  : std_logic;
  signal rx_cfg      : t_rx_streamer_cfg;

  -- BTrain <---> BTrain application (e.g. on BTrainFMC)
  signal tx_ready            : std_logic;
  signal tx_TransmitFrame_p1 : std_logic;
  signal tx_FrameHeader    : t_FrameHeader;
  signal tx_BFramePayloads : t_BFramePayload;
  signal tx_IFramePayloads : t_IFramePayload;
  signal tx_CFramePayloads : t_CFramePayload;
  signal rx_FrameHeader      : t_FrameHeader;
  signal rx_BFramePayloads   : t_BFramePayload;
  signal rx_IFramePayloads   : t_IFramePayload;
  signal rx_PFramePayloads   : t_PFramePayload;
  signal rx_Frame_valid_pX   : std_logic;
  signal rx_Frame_typeID     : std_logic_vector(c_type_ID_size-1 downto 0);
  signal rx_cfg_pol_inv      : std_logic;

  -- B Up/Down converter
  signal bup_out   : std_logic;
  signal bdown_out : std_logic;

  -- SIM <---> BTrain application
  signal sim_tx_FrameHeader_bit_14to15   : std_logic_vector(1 downto 0);
  signal sim_tx_FrameHeader_d_low_marker : std_logic;
  signal sim_tx_FrameHeader_f_low_marker : std_logic;
  signal sim_tx_FrameHeader_zero_cycle   : std_logic;
  signal sim_tx_FrameHeader_C0           : std_logic;
  signal sim_tx_FrameHeader_error        : std_logic;
  signal sim_tx_FrameHeader_sim_eff      : std_logic;
  signal sim_tx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_tx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_tx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_tx_TransmitFrame_p1         : std_logic;
  signal sim_tx_PFramePayload_Istatus    : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWLB8L     : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWFNI      : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWFNP      : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWDNI      : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWDNP      : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWFW       : std_logic_vector(31 downto 0);
  signal sim_tx_PFramePayload_IWDW       : std_logic_vector(31 downto 0);
  signal sim_tx_ready                    : std_logic;
  signal sim_rx_FrameHeader_bit_14to15   : std_logic_vector(1 downto 0);
  signal sim_rx_FrameHeader_d_low_marker : std_logic;
  signal sim_rx_FrameHeader_f_low_marker : std_logic;
  signal sim_rx_FrameHeader_zero_cycle   : std_logic;
  signal sim_rx_FrameHeader_C0           : std_logic;
  signal sim_rx_FrameHeader_error        : std_logic;
  signal sim_rx_FrameHeader_sim_eff      : std_logic;
  signal sim_rx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_rx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_rx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_Istatus    : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWLB8L     : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWFNI      : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWFNP      : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWDNI      : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWDNP      : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWFW       : std_logic_vector(31 downto 0);
  signal sim_rx_PFramePayload_IWDW       : std_logic_vector(31 downto 0);
  signal sim_rx_Frame_valid_pX           : std_logic;
  signal sim_rx_Frame_typeID             : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_rx_bup                      : std_logic;
  signal sim_rx_bdown                    : std_logic;

  -- for dummy tx process
  signal cnt                             : unsigned(31 downto 0);
  signal B_dummy                         : unsigned(31 downto 0);
  constant c_cnt_period                  : unsigned(31 downto 0) := to_unsigned(125000, 32); --1ms


  constant c_tx_streamer_params_ref_clk: t_tx_streamer_params :=(
      data_width            => c_BTrain_streamer_data_width,
      buffer_size           => 4,
      threshold             => 2,
      max_words_per_frame   => 4,
      timeout               => 1024,
      use_ref_clk_for_data  => 1,
      escape_code_disable   => FALSE);

  constant c_rx_streamer_params_ref_clk: t_rx_streamer_params :=(
      data_width            => c_BTrain_streamer_data_width,
      buffer_size           => 4,
      escape_code_disable   => FALSE,
      use_ref_clk_for_data  => 1,
      expected_words_number => 0);


begin  -- architecture top

  ------------------------------------------------------------------------------
  -- System reset
  ------------------------------------------------------------------------------

  -- logic AND of all async reset sources (active low)
  areset_n <= button1_n_i and gn_rst_n_i;

  -----------------------------------------------------------------------------
  -- Primary wishbone Crossbar
  -----------------------------------------------------------------------------

  cmp_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB_MASTERS,
      g_num_slaves  => c_NUM_WB_SLAVES,
      g_registered  => TRUE,
      g_wraparound  => TRUE,
      g_layout      => c_WB_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx_master_out,
      slave_o   => cnx_master_in,
      master_i  => cnx_slave_out,
      master_o  => cnx_slave_in);

  -----------------------------------------------------------------------------
  -- GN4124, PCIe bridge core
  -----------------------------------------------------------------------------
  cmp_gn4124_core : gn4124_core
    port map (
      ---------------------------------------------------------
      -- Control and status
      rst_n_a_i => gn_rst_n_i,
      status_o  => open,

      ---------------------------------------------------------
      -- P2L Direction
      --
      -- Source Sync DDR related signals
      p2l_clk_p_i  => gn_p2l_clk_p_i,
      p2l_clk_n_i  => gn_p2l_clk_n_i,
      p2l_data_i   => gn_p2l_data_i,
      p2l_dframe_i => gn_p2l_dframe_i,
      p2l_valid_i  => gn_p2l_valid_i,
      -- P2L Control
      p2l_rdy_o    => gn_p2l_rdy_o,
      p_wr_req_i   => gn_p_wr_req_i,
      p_wr_rdy_o   => gn_p_wr_rdy_o,
      rx_error_o   => gn_rx_error_o,
      vc_rdy_i     => gn_vc_rdy_i,

      ---------------------------------------------------------
      -- L2P Direction
      --
      -- Source Sync DDR related signals
      l2p_clk_p_o  => gn_l2p_clkp_o,
      l2p_clk_n_o  => gn_l2p_clkn_o,
      l2p_data_o   => gn_l2p_data_o,
      l2p_dframe_o => gn_l2p_dframe_o,
      l2p_valid_o  => gn_l2p_valid_o,
      -- L2P Control
      l2p_edb_o    => gn_l2p_edb_o,
      l2p_rdy_i    => gn_l2p_rdy_i,
      l_wr_rdy_i   => gn_l_wr_rdy_i,
      p_rd_d_rdy_i => gn_p_rd_d_rdy_i,
      tx_error_i   => gn_tx_error_i,

      ---------------------------------------------------------
      -- Interrupt interface
      dma_irq_o => open,
      irq_p_i   => '0',
      irq_p_o   => gn_gpio_b(0),

      ---------------------------------------------------------
      -- DMA registers wishbone interface (slave classic)
      dma_reg_clk_i => clk_sys_62m5,
      dma_reg_adr_i => (others=>'0'),
      dma_reg_dat_i => (others=>'0'),
      dma_reg_sel_i => (others=>'0'),
      dma_reg_stb_i => '0',
      dma_reg_we_i  => '0',
      dma_reg_cyc_i => '0',

      ---------------------------------------------------------
      -- CSR wishbone interface (master pipelined)
      csr_clk_i   => clk_sys_62m5,
      csr_rst_n_i => rst_sys_62m5_n,
      csr_adr_o   => gn_wbadr,
      csr_dat_o   => cnx_master_out(c_WB_MASTER_PCIE).dat,
      csr_sel_o   => cnx_master_out(c_WB_MASTER_PCIE).sel,
      csr_stb_o   => cnx_master_out(c_WB_MASTER_PCIE).stb,
      csr_we_o    => cnx_master_out(c_WB_MASTER_PCIE).we,
      csr_cyc_o   => cnx_master_out(c_WB_MASTER_PCIE).cyc,
      csr_dat_i   => cnx_master_in(c_WB_MASTER_PCIE).dat,
      csr_ack_i   => cnx_master_in(c_WB_MASTER_PCIE).ack,
      csr_stall_i => cnx_master_in(c_WB_MASTER_PCIE).stall,
      csr_err_i   => cnx_master_in(c_WB_MASTER_PCIE).err,
      csr_rty_i   => cnx_master_in(c_WB_MASTER_PCIE).rty,

      ---------------------------------------------------------
      -- L2P DMA Interface (Pipelined Wishbone master)
      dma_clk_i   => clk_sys_62m5,
      dma_dat_i   => (others=>'0'),
      dma_ack_i   => '1',
      dma_stall_i => '0',
      dma_err_i   => '0',
      dma_rty_i   => '0');

  -- "translating" word addressing of Gennum module into byte addressing
  cnx_master_out(c_WB_MASTER_PCIE).adr(1 downto 0)   <= (others => '0');
  cnx_master_out(c_WB_MASTER_PCIE).adr(18 downto 2)  <= gn_wbadr(16 downto 0);
  cnx_master_out(c_WB_MASTER_PCIE).adr(31 downto 19) <= (others => '0');


  -----------------------------------------------------------------------------
  -- The WR PTP core board package (WB Slave)
  -----------------------------------------------------------------------------


  cmp_xwrc_board_spec : xwrc_board_spec
    generic map (
      g_simulation                => g_simulation,
      g_with_external_clock_input => FALSE,
      g_dpram_initf               => g_dpram_initf,
      g_streamers_op_mode         => TX_AND_RX,
      g_tx_streamer_params        => c_tx_streamer_params_ref_clk,
      g_rx_streamer_params        => c_rx_streamer_params_ref_clk,
      g_fabric_iface              => STREAMERS)
    port map (
      areset_n_i          => areset_n,
      clk_20m_vcxo_i      => clk_20m_vcxo_i,
      clk_125m_pllref_p_i => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i => clk_125m_pllref_n_i,
      clk_125m_gtp_n_i    => clk_125m_gtp_n_i,
      clk_125m_gtp_p_i    => clk_125m_gtp_p_i,
      clk_sys_62m5_o      => clk_sys_62m5,
      clk_ref_125m_o      => clk_ref_125m,
      rst_sys_62m5_n_o    => rst_sys_62m5_n,
      rst_ref_125m_n_o    => rst_ref_125m_n,

      plldac_sclk_o       => plldac_sclk_o,
      plldac_din_o        => plldac_din_o,
      pll25dac_cs_n_o     => pll25dac_cs_n_o,
      pll20dac_cs_n_o     => pll20dac_cs_n_o,

      sfp_txp_o           => sfp_txp_o,
      sfp_txn_o           => sfp_txn_o,
      sfp_rxp_i           => sfp_rxp_i,
      sfp_rxn_i           => sfp_rxn_i,
      sfp_det_i           => sfp_mod_def0_i,
      sfp_sda_i           => sfp_sda_in,
      sfp_sda_o           => sfp_sda_out,
      sfp_scl_i           => sfp_scl_in,
      sfp_scl_o           => sfp_scl_out,
      sfp_rate_select_o   => sfp_rate_select_o,
      sfp_tx_fault_i      => sfp_tx_fault_i,
      sfp_tx_disable_o    => sfp_tx_disable_o,
      sfp_los_i           => sfp_los_i,

      eeprom_sda_i        => eeprom_sda_in,
      eeprom_sda_o        => eeprom_sda_out,
      eeprom_scl_i        => eeprom_scl_in,
      eeprom_scl_o        => eeprom_scl_out,

      onewire_i           => onewire_data,
      onewire_oen_o       => onewire_oe,
      -- Uart
      uart_rxd_i          => uart_rxd_i,
      uart_txd_o          => uart_txd_o,
      -- SPI Flash
      flash_sclk_o        => flash_sclk_o,
      flash_ncs_o         => flash_ncs_o,
      flash_mosi_o        => flash_mosi_o,
      flash_miso_i        => flash_miso_i,

      wb_slave_o          => cnx_slave_out(c_WB_SLAVE_WRC),
      wb_slave_i          => cnx_slave_in(c_WB_SLAVE_WRC),

      pps_p_o             => wrc_pps_out,
      pps_led_o           => wrc_pps_led,
      led_link_o          => led_link_o,
      led_act_o           => led_act_o,

      -- BTRAIN
      wrs_tx_data_i       => tx_data,
      wrs_tx_valid_i      => tx_valid,
      wrs_tx_dreq_o       => tx_dreq,
      wrs_tx_last_i       => tx_last_p1,
      wrs_tx_flush_i      => tx_flush_p1,
      wrs_tx_cfg_i        => tx_cfg,
      wrs_rx_first_o      => rx_first_p1,
      wrs_rx_last_o       => rx_last_p1,
      wrs_rx_data_o       => rx_data,
      wrs_rx_valid_o      => rx_valid,
      wrs_rx_dreq_i       => rx_dreq,
      wrs_rx_cfg_i        => rx_cfg
      );

  -- Configuration of streamers:
  -- 1) For synthesis : In Btrain deployments, by default, streamers use VID=0
  --    (i.e. priority tagging). 
  -- 2) For simulation: Using VLANs (VID=0) is problematic (requires special
  --    configuration). It is easier to disable VLANs for simulation.
  gen_streamers_cfg_sim: if (g_simulation>0) generate -- no VLAN
    rx_cfg <= c_rx_streamer_cfg_default;
    tx_cfg <= c_tx_streamer_cfg_default;
  end generate gen_streamers_cfg_sim;
  gen_streamers_cfg_syn: if (g_simulation=0) generate -- with VLAN(VID=0)
    rx_cfg <= c_rx_streamer_cfg_btrain;
    tx_cfg <= c_tx_streamer_cfg_btrain;
  end generate gen_streamers_cfg_syn;

  -------------------------------------------------------------------------------------------
  -- BTrain frames transceiver
  -------------------------------------------------------------------------------------------
  cmp_btrain : BTrainFrameTransceiver
    generic map(
      g_rx_BframeType     => c_ID_ALL,  -- accept all types of BTrain frame - only debugging
      g_use_wb_config     => TRUE,
      g_slave_mode        => CLASSIC,
      g_slave_granularity => BYTE,
      g_clk_rate          => 125000000)
    port map(
      clk_i   => clk_ref_125m,
      rst_n_i => rst_ref_125m_n,

      tx_data_o     => tx_data,
      tx_valid_o    => tx_valid,
      tx_dreq_i     => tx_dreq,
      tx_last_p1_o  => tx_last_p1,
      tx_flush_p1_o => tx_flush_p1,
      -- rx
      rx_data_i     => rx_data,
      rx_valid_i    => rx_valid,
      rx_first_p1_i => rx_first_p1,
      rx_dreq_o     => rx_dreq,
      rx_last_p1_i  => rx_last_p1,

      rx_FrameHeader_o    => rx_FrameHeader,
      rx_BFramePayloads_o => rx_BFramePayloads,
      rx_IFramePayloads_o => rx_IFramePayloads,
      rx_PframePayloads_o => rx_PFramePayloads,
      rx_Frame_valid_pX_o => rx_Frame_valid_pX,
      rx_Frame_typeID_o   => rx_Frame_typeID,

      ready_o               => tx_ready,
      tx_TransmitFrame_p1_i => tx_TransmitFrame_p1,
      tx_FrameHeader_i      => tx_FrameHeader,
      tx_BFramePayloads_i   => tx_BFramePayloads,
      tx_IFramePayloads_i   => tx_IFramePayloads,
      tx_CFramePayloads_i   => tx_CFramePayloads,

      -- NOTE/Warning: The module works internally with clk_ref_125m
      -- while the WB registers work with clk_sys_62m5. This results
      -- in clock doman crossing. It is not good, yet it works in this
      -- case. Please, avoid this in final designs. It can cause you
      -- timing errors.
      wb_slave_i => cnx_slave_in(c_WB_SLAVE_BTRAIN),
      wb_slave_o => cnx_slave_out(c_WB_SLAVE_BTRAIN)
      );

  --fake data
  gen_fake_data: if g_simulation = 0 generate
    tx_TransmitFrame_p1 <= '0';
    tx_FrameHeader      <= c_FrameHeader_dummy;
    tx_BFramePayloads   <= c_BFramePayload_dummy;
    tx_IFramePayloads   <= c_IFramePayload_zero;
    tx_CFramePayloads   <= c_CFramePayload_zero;
  end generate gen_fake_data;
  ------------------------------------------------------------------------------
  --^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  -- Beyond this point, in general, application-specific code is placed instead
  -- of the provided example code that is also specific to the DIO FMC.
  --
  -- The example code below uses a module available in hdl/rtl/bupdown_conveter.
  -- This module translates the B value in WR-BTrain farmes into legacy Bup/Bdown
  -- pulses. The DIO FMC used in the reference design outputs on its 5 IOs:
  -- 0: PPS signal that is a Pulse Per Second synchronized with WR accuarcy
  -- 1: tx_valid that is a signal indicating transmission of BTrain frame
  -- 2: rx_valid that is a signal indicating reception of BTrain frame
  -- 3: Bup   pulse that is a legacy signal conveted from the received B value
  -- 4: Bdown pulse that is a legacy signal conveted from the received B value
  --
  --^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ------------------------------------------------------------------------------
  --- B UpDown convereter
  ------------------------------------------------------------------------------
  cmp_bupdown_converter: entity work.bupdown_converter
  generic map(
    g_PULSE_WIDTH_NS        => 1000,
    g_MIN_PULSE_GAP_NS      => 1000,
    g_CLOCK_PERIOD_NS       => 8)
  port map(
    clk_i                   => clk_ref_125m,
    rst_n_i                 => rst_ref_125m_n,

    rx_frame_header_i       => rx_FrameHeader,
    rx_bFrame_payloads_i    => rx_BFramePayloads,
    rx_frame_valid_p_i      => rx_Frame_valid_pX,
    rx_cfg_pol_inv_i        => rx_cfg_pol_inv,

    bup_o                   => bup_out,
    bdown_o                 => bdown_out
  );

  ------------------------------------------------------------------------------
  --- some connections
  ------------------------------------------------------------------------------
  -- Tristates for SFP EEPROM
  sfp_mod_def1_b <= '0' when sfp_scl_out = '0' else 'Z';
  sfp_mod_def2_b <= '0' when sfp_sda_out = '0' else 'Z';
  sfp_scl_in     <= sfp_mod_def1_b;
  sfp_sda_in     <= sfp_mod_def2_b;

  -- tri-state onewire access
  onewire_b    <= '0' when (onewire_oe = '1') else 'Z';
  onewire_data <= onewire_b;

  ------------------------------------------------------------------------------
  -- Digital I/O FMC Mezzanine connections
  ------------------------------------------------------------------------------
  gen_dio_iobufs: for I in 0 to 4 generate
    U_ibuf: IBUFDS
      generic map (
        DIFF_TERM => true)
      port map (
        O  => dio_in(i),
        I  => dio_p_i(i),
        IB => dio_n_i(i));

    U_obuf : OBUFDS
      port map (
        I  => dio_out(i),
        O  => dio_p_o(i),
        OB => dio_n_o(i));
  end generate;

  -- EEPROM I2C tri-states
  dio_sda_b <= '0' when (eeprom_sda_out = '0') else 'Z';
  eeprom_sda_in <= dio_sda_b;
  dio_scl_b <= '0' when (eeprom_scl_out = '0') else 'Z';
  eeprom_scl_in <= dio_scl_b;

  -------------------------------- Register TX/RX pulses for DIO -----------------------------
  -- Register the tx/rx valid signal at the output
  -- 1) rx_valid is registered to avoid gliches as it is combinatorial
  -- 2) tx_valid is registered to match the additional latency of a flip-flop
  p_register_txrx_valid: process (clk_ref_125m)
  begin
    if rising_edge (clk_ref_125m) then
      if rst_ref_125m_n = '0' then
        tx_valid_ext <= '0';
        rx_valid_ext <= '0';
      else
        tx_valid_ext <= tx_valid;
        rx_valid_ext <= rx_valid;
      end if;
    end if;
  end process;

  -------------------------------- DIO config and assignment --------------------------------
  -- DIO[0]  - out PPS output
  -- DIO[1]  - out tx_valid_ext signal (extended/registered tx_valid)
  -- DIO[2]  - out rx_valid_ext signal (extended/registered rx_valid)
  -- DIO[3]  - out tx_valid     signal (directly from streamers)
  -- DIO[4]  - out rx_valid     signal (directly from streamers)
  --
  -- LED TOP - PPS outpout
  -- LED BOT - rx_valid signal (extended to 80ms)
  -------------------------------------------------------------------------------------------
  -- all DIO connectors are outputs
  dio_oe_n_o(4 downto 0) <= (others => '0');

  -- no termination, since all outputs
  dio_term_en_o(4 downto 0) <= (others => '0');

  dio_out(0) <= wrc_pps_out;
  dio_out(1) <= tx_valid_ext;
  dio_out(2) <= rx_valid_ext;
  dio_out(3) <= tx_valid;
  dio_out(4) <= rx_valid;

  -- LEDs
  U_Extend_PPS : gc_extend_pulse
  generic map (
    g_width => 10000000)
  port map (
    clk_i      => clk_ref_125m,
    rst_n_i    => rst_ref_125m_n,
    pulse_i    => wrc_pps_led,
    extended_o => dio_led_top_o);

  U_Extend_RX_VALID : gc_extend_pulse
    generic map (
      g_width => 5000000)     -- output length: 5000000x16ns = 80 ms.

    port map (
      clk_i      => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      pulse_i    => rx_valid,
      extended_o => dio_led_bot_o);

end architecture top;
