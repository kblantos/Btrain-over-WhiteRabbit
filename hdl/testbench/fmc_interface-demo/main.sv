//-----------------------------------------------------------------------------
// Title      : Btrain over White Rabbit
// Project    : Btrain
//-----------------------------------------------------------------------------
// File       : main.sv
// Author     : Maciej Lipinski
// Company    : CERN
// Created    : 2016-05-30
// Platform   : FPGA-generics
// Standard   : VHDL
//-----------------------------------------------------------------------------
// Description:
// 
// Simple simulation of two interconnected SPEC boards with Btrain FMC:
// - both boards send and receive Bframes
// - SPEC A receives up/down/C0_restet pulses
// 
// The simulation uses gennum model to allow writing/readin SPEC's control
// registers.
// 
// Please, note that the initializatin of LM32 and the software takes some time,
// allow the simulation to run for ~5 minutes (at least 105us of simulation time)
//-----------------------------------------------------------------------------
//
// Copyright (c) 2016 CERN/BE-CO-HT
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-------------------------------------------------------------------------------

`timescale 1ns/1ps

`include "gn4124_bfm.svh"
`include "wr_streamers_wb.svh"
`include "BTrain_wb.svh"

const uint64_t BASE_WRPC        = 'h00C0000;
const uint64_t BASE_AUX_IN_WRPC = 'h0020700;
const uint64_t BASE_WRSTREAMERS = BASE_WRPC + BASE_AUX_IN_WRPC;
const uint64_t BASE_BTRAIN      = 'h00001200;

module main;
  reg clk_125m_pllref = 0;
  reg clk_20m_vcxo = 0;
  reg clk_40m_fmc_carrier = 1;
  reg clk_ext = 0;
  wire [9:0] DIOs_A;
  wire [9:0] DIOs_B;
  reg b_up_in = 0;
  reg b_dn_in = 0;
  reg err_b_to_a = 0;
  reg err_a_to_b = 0;
  reg start_b_up_down = 0;
  
  always #50ns clk_ext <= ~clk_ext;
  always #4ns clk_125m_pllref <= ~clk_125m_pllref;
  always #20ns clk_20m_vcxo <= ~clk_20m_vcxo;
  
  always #12.5ns clk_40m_fmc_carrier <= ~clk_40m_fmc_carrier;
   
  IGN4124PCIMaster I_GennumA ();
  IGN4124PCIMaster I_GennumB ();
  /// ///////////////////////////////////////////////////////////////////////////////////////
  /// Instantiation of two interconnected SPECs 
  /// ///////////////////////////////////////////////////////////////////////////////////////


  spec_btrain_ref_top
     #(
       .g_simulation (1),
       .g_dpram_initf("../../ip_cores/wr-cores/bin/wrpc/wrc_phy8_sim.bram")
       )
   DUT_SPEC_A
     (
      .clk_125m_pllref_p_i(clk_125m_pllref),
      .clk_125m_pllref_n_i(~clk_125m_pllref),
      .clk_125m_gtp_p_i(clk_125m_pllref),
      .clk_125m_gtp_n_i(~clk_125m_pllref),
      .clk_20m_vcxo_i(clk_20m_vcxo),

      .sfp_txp_o(a_to_b_p_o),
      .sfp_txn_o(a_to_b_n),
      .sfp_rxp_i(b_to_a_p_i),
      .sfp_rxn_i(b_to_a_n),

      `GENNUM_WIRE_SPEC_BTRAIN_REF(I_GennumA)
      );

  
  spec_fmc_interface_test // to be replaced by cute_ref
    #(
      .g_simulation (1),
      .g_dpram_initf("../../ip_cores/wr-cores/bin/wrpc/wrc_phy8_sim.bram")
    )
    DUT_SPEC_B 
    (
      .clk_125m_pllref_p_i(clk_125m_pllref),
      .clk_125m_pllref_n_i(~clk_125m_pllref),
      .clk_125m_gtp_p_i(clk_125m_pllref),
      .clk_125m_gtp_n_i(~clk_125m_pllref),
      .clk_20m_vcxo_i(clk_20m_vcxo),
      .clk_40m_fmc_carrier_i(clk_40m_fmc_carrier),

      .sfp_txp_o(b_to_a_p_o),
      .sfp_txn_o(b_to_a_n),

      .sfp_rxp_i(a_to_b_p_i),
      .sfp_rxn_i(a_to_b_n)

//[CUTE]       `GENNUM_WIRE_SPEC_BTRAIN_REF(I_GennumB)
    );

   // enable breaking the  connection to test counting lost frames
   assign a_to_b_p_i = (err_a_to_b) ? 1'bz : a_to_b_p_o;
   assign b_to_a_p_i = (err_b_to_a) ? 1'bz : b_to_a_p_o;

   // observe the link LEDs on both sides, and tell us when the link is ready.
   wire link_up_a = DUT_SPEC_A.cmp_xwrc_board_spec.led_link_o;
   wire link_up_b = DUT_SPEC_B.cmp_xwrc_board_spec.led_link_o;

  /// ///////////////////////////////////////////////////////////////////////////////////////
  /// perform example configuration of both specs
  /// ///////////////////////////////////////////////////////////////////////////////////////
  initial begin
    uint64_t rval_A, rval_B, cycLen, cycNum;
    static uint64_t tx_dbg_mode = 0;
    int cycInc;
    uint64_t ts_TAI_A,ts_TAI_B, ts_cyc_A, ts_cyc_B;
    CBusAccessor accA, accB;
    accA = I_GennumA.get_accessor();
//[CUTE]     accB = I_GennumB.get_accessor();
//[CUTE]     @(posedge (I_GennumA.ready & I_GennumB.ready));
//[CUTE]     $display("Gennum ready A=%d and B=%d",I_GennumA.ready, I_GennumB.ready);
    @(posedge (I_GennumA.ready));
    $display("Gennum ready A=%d",I_GennumA.ready);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// read dummy value from WR STREAMERS, should be 0xcafebabe
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_DUMMY, rval_A, 4);
//[CUTE]   accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_DUMMY, rval_B, 4);
    rval_A =  (`WR_STREAMERS_DUMMY_DUMMY & rval_A)>> `WR_STREAMERS_DUMMY_DUMMY_OFFSET;
//[CUTE]    rval_B =  (`WR_STREAMERS_DUMMY_DUMMY & rval_B)>> `WR_STREAMERS_DUMMY_DUMMY_OFFSET;
    $display("Read dummy value from streamers @ A : 0x%x (should be 0xDEADBEEF)",rval_A);
//[CUTE]    $display("Read dummy value from streamers @ B : 0x%x (should be 0xDEADBEEF)",rval_B);

    /// /////////////////////////////////////////////////////////////////////////////////////
    /// read dummy value from WRBtrainStuff, should be 0xCAFE
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
//[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
    rval_A =  (`BTRAIN_SCR_DUMMY & rval_A)>> `BTRAIN_SCR_DUMMY_OFFSET;
//[CUTE]    rval_B =  (`BTRAIN_SCR_DUMMY & rval_B)>> `BTRAIN_SCR_DUMMY_OFFSET;
    $display("DUMMY nubmer from spec A: 0x%x",rval_A);
//[CUTE]    $display("DUMMY nubmer from spec B: 0x%x",rval_B);

    /// /////////////////////////////////////////////////////////////////////////////////////
    /// wait until both SPECs see the Ethernet link. Otherwise the packet we're going 
    /// to send might end up in void...
    /// /////////////////////////////////////////////////////////////////////////////////////
     
    $display("");$display("");
    $display("====================================================");
    $display("===============wait for link up=====================");
    $display("====================================================");
    $display("= be very patient, it might take some time, i.e.   =");
    $display("= few minutes of your time and 200us on simulation =");
    $display("====================================================");
    $display("");$display("");
    #200us //wait until link goes down
    $display("End 200us wait, now just wait for links up (yet another 100us)");
    wait(link_up_a == 1'b1 && link_up_b == 1'b1);
    $display("both up now");

    #40us

    /// /////////////////////////////////////////////////////////////////////////////////////
    /// fix latency to be 2.4us = 2400ns = 300 cycles of 125MHz clock 
    /// /////////////////////////////////////////////////////////////////////////////////////

//[CUTE]    accB.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_CFG5, 300, 4);
//[CUTE]    accB.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_CFG,`WR_STREAMERS_CFG_OR_RX_FIX_LAT, 4);

    #4us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// programm the b-value simulatin generator just in case it's needed
    /// /////////////////////////////////////////////////////////////////////////////////////
    ///     B=20      *
    ///              * *
    ///             *   *
    ///            *     *
    ///           *       *
    ///          *         *
    ///         *           *
    ///        *             *
    ///       *               *   *
    ///      *                 * *
    /// B=0 *                   *
    ///     |cycNum=0 |cycNum=1 |cycNum=
    /// /////////////////////////////////////////////////////////////////////////////////////
    // read the max number of subcycles
    accA.read(BASE_BTRAIN + `ADDR_BTRAIN_SIMB_CTRL, rval_A, 4);
    $display("Spec A - max number of sub-cycles in SimGen: %d",rval_A>>`BTRAIN_SIMB_CTRL_CMAXLEN_OFFSET);
    // sybcycle 0:
    cycLen = 20;
    cycInc = 1;
    cycNum = 0;
    rval_B = (cycNum <<`BTRAIN_SIMB_CTRL_SCID_OFFSET);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SIMB_CTRL,    rval_B, 4);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_BSIM_SCYC_LEN, cycLen, 4);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_BSIM_SCYC_INC, cycInc, 4);
    // sybcycle 1:
    cycLen = 20;
    cycInc = -1;
    cycNum = 1;
    rval_B = (cycNum <<`BTRAIN_SIMB_CTRL_SCID_OFFSET);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SIMB_CTRL,    rval_B, 4);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_BSIM_SCYC_LEN, cycLen, 4);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_BSIM_SCYC_INC, cycInc, 4);
    // set number of subcycles
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SIMB_CTRL, 'h2, 4); // two subcycles

    #4us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// send single btrain frame
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, `BTRAIN_SCR_TX_SINGLE, 4);

    #1us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// set period of RX valid signal
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, 1, 4);
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, 5, 4);
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// configure period of transmission 
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_TX_PERIOD, 175, 4);
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_TX_PERIOD, 60, 4);
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// set debugging mode
    /// 0: debugging disabled
    /// 1: send coutner
    /// 2: send simulated waveform (need to program the waveform)
    /// 3: send constant values (dummy frame)
    /// /////////////////////////////////////////////////////////////////////////////////////
    tx_dbg_mode = 1;
    rval_A = (tx_dbg_mode << `BTRAIN_SCR_TX_DBG_OFFSET);
//[CUTE]    rval_B = (tx_dbg_mode << `BTRAIN_SCR_TX_DBG_OFFSET);
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// enable wishbone configuration of WRbtrain - the transmission is started
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
//[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
    rval_A &= ~`BTRAIN_SCR_TX_DBG_FTYPE;
    rval_A |= `BTRAIN_SCR_TX_OR_CONFIG | `BTRAIN_SCR_RX_OR_CONFIG;
//[CUTE]    rval_B |= `BTRAIN_SCR_TX_OR_CONFIG | `BTRAIN_SCR_RX_OR_CONFIG;
    accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);

    #100us
     accA.write(BASE_BTRAIN + `ADDR_BTRAIN_TX_PERIOD, 250, 4);
//     /// /////////////////////////////////////////////////////////////////////////////////////
//     /// start sending I in one direction and PFW in the other
//     /// /////////////////////////////////////////////////////////////////////////////////////
//     accA.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
// //[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//     rval_A &= ~`BTRAIN_SCR_TX_DBG_FTYPE;
//     rval_A |= (2 << `BTRAIN_SCR_TX_DBG_FTYPE_OFFSET); // enforce I frame
// //[CUTE]    rval_B |= (3 << `BTRAIN_SCR_TX_DBG_FTYPE_OFFSET); // enforce PFW frame
//     accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
// //[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//     $display("Enforce sending I   frames on spec A");
// //[CUTE]    $display("Enforce sending PFW frames on spec B and set debug mode to %d",tx_dbg_mode);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// set period of RX valid signal for SPEC B:
    /// - delay by 20 cycle
    /// - valid for 20 cycles
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    rval_B = (20 <<`BTRAIN_RX_OUT_DATA_TIME_DELAY_OFFSET) | 20;
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, rval_B, 4);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// disable RX valid signal  for SPEC B:
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, 0, 4);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// set period of Rx valid:
    /// - delay by 10 cycle
    /// - valid continuously
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    rval_B = (10 <<`BTRAIN_RX_OUT_DATA_TIME_DELAY_OFFSET) | 'hFFFF;
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, rval_B, 4);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// set period of RX valid signal for SPEC B:
    /// - delay by 0 cycle
    /// - valid for 1 cycles
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    rval_B = (0 <<`BTRAIN_RX_OUT_DATA_TIME_DELAY_OFFSET) | 1;
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_RX_OUT_DATA_TIME, rval_B, 4);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// invert polarity
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//[CUTE]    rval_B |= `BTRAIN_SCR_RX_VALID_POL_INV;
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR , rval_B, 4);

    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// invert polarity
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//[CUTE]    rval_B &= ~`BTRAIN_SCR_RX_VALID_POL_INV;
//[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);

    #5us
//     /// /////////////////////////////////////////////////////////////////////////////////////
//     /// start sending I in one direction and PFW in the other
//     /// /////////////////////////////////////////////////////////////////////////////////////
//     accA.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
// //[CUTE]    accB.read(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//     rval_A &= ~`BTRAIN_SCR_TX_DBG_FTYPE;
//     rval_A |= (4 << `BTRAIN_SCR_TX_DBG_FTYPE_OFFSET); // enforce C frame
// //[CUTE]    rval_B |= (3 << `BTRAIN_SCR_TX_DBG_FTYPE_OFFSET); // enforce PFW frame
//     accA.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_A, 4);
// //[CUTE]    accB.write(BASE_BTRAIN + `ADDR_BTRAIN_SCR, rval_B, 4);
//     $display("Enforce sending C   frames on spec A");
// //[CUTE]    $display("Enforce sending PFW frames on spec B and set debug mode to %d",tx_dbg_mode);

    /// /////////////////////////////////////////////////////////////////////////////////////
    /// Read WR Streamers' statistics
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_TX_STAT2, rval_A, 4);
    $display("Number of transmitted frames at specA: %d",rval_A);
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_STAT4, rval_A, 4);
    $display("Number of received at specA: %d",rval_A);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_TX_STAT2, rval_B, 4);
//[CUTE]    $display("Number of transmitted frames at specB: %d",rval_B);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_STAT4, rval_B, 4);
//[CUTE]    $display("Number of received at specB: %d",rval_B);
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// reset statistics
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, `WR_STREAMERS_SSCR1_RST_STATS | `WR_STREAMERS_SSCR1_SNAPSHOT_STATS, 4);
//[CUTE]    accB.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, `WR_STREAMERS_SSCR1_RST_STATS, 4);
    accA.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, 0, 4);
//[CUTE]    accB.write(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, 0, 4);

    #2us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// read reset timestamps
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, rval_A, 4);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR1, rval_B, 4);
    ts_cyc_A = (`WR_STREAMERS_SSCR1_RST_TS_CYC & rval_A)>> `WR_STREAMERS_SSCR1_RST_TS_CYC_OFFSET;
//[CUTE]    ts_cyc_B = (`WR_STREAMERS_SSCR1_RST_TS_CYC & rval_B)>> `WR_STREAMERS_SSCR1_RST_TS_CYC_OFFSET;
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR2, rval_A, 4);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_SSCR2, rval_B, 4);
    ts_TAI_A = (`WR_STREAMERS_SSCR1_RST_TS_CYC & rval_A)>> `WR_STREAMERS_SSCR2_RST_TS_TAI_LSB_OFFSET;
//[CUTE]    ts_TAI_B = (`WR_STREAMERS_SSCR1_RST_TS_CYC & rval_B)>> `WR_STREAMERS_SSCR2_RST_TS_TAI_LSB_OFFSET;
    $display("Timestamp of Statistics reset for spec A: TAI=%d, CYC=%d",ts_TAI_A,ts_cyc_A);
//[CUTE]    $display("Timestamp of Statistics reset for spec A: TAI=%d, CYC=%d",ts_TAI_B,ts_cyc_B);

    /// /////////////////////////////////////////////////////////////////////////////////////
    /// Read WR Streamers' statistics
    /// /////////////////////////////////////////////////////////////////////////////////////
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_TX_STAT2, rval_A, 4);
    $display("Number of transmitted frames at specA: %d",rval_A);
    accA.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_STAT4, rval_A, 4);
    $display("Number of received at specA: %d",rval_A);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_TX_STAT2, rval_B, 4);
    //[CUTE]$display("Number of transmitted frames at specB: %d",rval_B);
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_STAT4, rval_B, 4);
//[CUTE]    $display("Number of received at specB: %d",rval_B);
/*
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// break the link for a while to test frame loss counter
    /// /////////////////////////////////////////////////////////////////////////////////////
    err_a_to_b = 1;
    $display("Breaking link between spec A and spec B for some time to test loss cnt");
    err_b_to_a = 1;
    $display("Breaking link between spec B and spec A for some time to test loss cnt");
    #5us
    err_a_to_b = 0;
    $display("Fixing link between spec A and spec B");
    err_b_to_a = 0;
    $display("Fixing link between spec B and spec A");*/

//[CUTE]    #5us
    /// /////////////////////////////////////////////////////////////////////////////////////
    /// Read WR Streamers' statistics
    /// /////////////////////////////////////////////////////////////////////////////////////
//[CUTE]    accB.read(BASE_WRSTREAMERS + `ADDR_WR_STREAMERS_RX_STAT6, rval_B, 4);
//[CUTE]      $display("Number of lost frames at specB: %d",rval_B);

  end

  task automatic wait_cycles;
    input [31:0] ncycles;
    begin : wait_body
      integer i;
      for(i=0;i<ncycles;i=i+1) @(posedge clk_125m_pllref);
    end
  endtask // wait_cycles   
endmodule // main



