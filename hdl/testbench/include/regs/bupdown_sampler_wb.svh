`define WBGEN2_BUPDOWN_SAMPLER_VERSION 32'h00000001
`define ADDR_BUPDOWN_SAMPLER_VER       17'h0
`define BUPDOWN_SAMPLER_VER_ID_OFFSET 0
`define BUPDOWN_SAMPLER_VER_ID 32'hffffffff
`define ADDR_BUPDOWN_SAMPLER_SCR       17'h4
`define BUPDOWN_SAMPLER_SCR_ACQ_START_OFFSET 0
`define BUPDOWN_SAMPLER_SCR_ACQ_START 32'h00000001
`define BUPDOWN_SAMPLER_SCR_ACQ_STOP_OFFSET 1
`define BUPDOWN_SAMPLER_SCR_ACQ_STOP 32'h00000002
`define BUPDOWN_SAMPLER_SCR_C0_RESET_OFFSET 2
`define BUPDOWN_SAMPLER_SCR_C0_RESET 32'h00000004
`define BUPDOWN_SAMPLER_SCR_ACQ_MODE_OFFSET 4
`define BUPDOWN_SAMPLER_SCR_ACQ_MODE 32'h00000030
`define BUPDOWN_SAMPLER_SCR_C0_MODE_OFFSET 6
`define BUPDOWN_SAMPLER_SCR_C0_MODE 32'h000000c0
`define BUPDOWN_SAMPLER_SCR_ACQ_STATUS_OFFSET 8
`define BUPDOWN_SAMPLER_SCR_ACQ_STATUS 32'h00000f00
`define BUPDOWN_SAMPLER_SCR_IN_LOAD_DISABLED_OFFSET 12
`define BUPDOWN_SAMPLER_SCR_IN_LOAD_DISABLED 32'h00001000
`define BUPDOWN_SAMPLER_SCR_DUMMY_OFFSET 16
`define BUPDOWN_SAMPLER_SCR_DUMMY 32'hffff0000
`define ADDR_BUPDOWN_SAMPLER_MSR       17'h8
`define BUPDOWN_SAMPLER_MSR_ADD_LAST_OFFSET 0
`define BUPDOWN_SAMPLER_MSR_ADD_LAST 32'hffffffff
`define ADDR_BUPDOWN_SAMPLER_MCR       17'hc
`define BUPDOWN_SAMPLER_MCR_S_PERIOD_OFFSET 0
`define BUPDOWN_SAMPLER_MCR_S_PERIOD 32'h0000ffff
`define BASE_BUPDOWN_SAMPLER_MEM       17'h10000
`define SIZE_BUPDOWN_SAMPLER_MEM       32'h4000
