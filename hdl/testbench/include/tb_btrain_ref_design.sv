//-----------------------------------------------------------------------------
// Title      : BTrain reference design testbench
// Project    : BTrain over White Rabbit
// URL        : https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit
//-----------------------------------------------------------------------------
// File       : tb_btrain_ref_design.sv
// Author(s)  : Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
// Company    : CERN (BE-CO-HT)
// Created    : 2017-04-13
//-----------------------------------------------------------------------------
// Description:
//
// SystemVerilog file with all definitions, classes, interfaces, etc.
// necessary for the BTrain reference design testbench.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2017 CERN
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//

//-----------------------------------------------------------------------------


`include "common_macros.svh"       // macros common to all reference designs
`include "wb_btrain_ref_design.sv" // for CWBBTRainMgt class

// Couple of typedefs to simplify declaration of virtual interfaces.
// The actual interfaces are declared at the end of this file.
typedef virtual IBTrain.TB_Tx vIBTrainTx;
typedef virtual IBTrain.TB_Rx vIBTrainRx;
typedef virtual ILink vILink;

// As defined in WRBTrain_pkg.vhd
const bit[7:0] c_ID_BkFrame  = 'h42;
const bit[7:0] c_ID_ImFrame  = 'h49;
const bit[7:0] c_ID_CdFrame  = 'h43;

// enumeration set via Config class and used by many classes
typedef enum  {random, programmed} t_gen_mode;

// structure to hold content of a WR BTrain frame (any type)
typedef struct {
  enum {B,I, C} f_type;
  struct {
    bit version_id;
    bit d_low_marker;
    bit f_low_marker;
    bit zero_cycle;
    bit C0;
    bit error;
    bit sim_eff;
    bit [7:0] frame_type;
  } header;

  struct {
    bit[31:0] B;
    bit[31:0] Bdot;
    bit[31:0] oldB;
    bit[31:0] measB;
    bit[31:0] simB;
    bit[31:0] synB;
  } payload_B;

  struct {
    bit[31:0] I;
  } payload_I;

  struct {
    bit[31:0] I;
    bit[31:0] V;
    bit[31:0] ID;
    bit[15:0] MSrcId;
    bit[15:0] CSrcId;
    bit[31:0] UserData;
    bit[31:0] Reserved_1;
    bit[31:0] Reserved_2;
  } payload_C;
} t_Bframe;

/// ///////////////////////////////////////////////////////////////////////////
/// Config Class
///
/// This class holds all the configuration values for the BTrain reference
/// design testbench. Most of these are randomized.
///
/// Configuration values include:
///
/// 1. _tx_period_e: Enumerated variable representing the frame tx rate.
///    "Full" corresponds to the maximum allowed (500kHz / 2us),
///    "Half" corresponds to half of the maximum (250kHz / 4us), and
///    "Random" picks any rate between Half and Full.
///     This is a random "cyclic" variable, which means that the randomizer
///     will iterate through all three values in successive calls.
/// 2. tx_period: This is the actual tx period (in 16ns clock cycles), set
///    by the post_randomize() function, according to _tx_period_e.
/// 3. n_frames: Number of frames to send, constrained between 100 and 250.
/// 4. mbox_size: Size of Transaction mailbox between Generator and Driver
///    class. This can be between 0 (infinite) and 15.
/// 5. name: A string used to identify the instance when displaying info.
/// 6. seed: The seed for the randomizer.
/// 7. verbose: When set to 1, it enables verbose messages on the console.
/// 8. rx_timeout_ns: Timeout when waiting for a transmitted frame to appear
///    on the receiver. Default is 15000ns.
/// 9. rx_timeout_step_ns: Time increments when checking for rx_timeout_ns.
///    Default is 100ns.
///
/// This class is instantiated by the Environment class and is then passed
/// to most of the other classes to give them access to the configuration
/// values.
/// ///////////////////////////////////////////////////////////////////////////
class Config;

   randc enum {Full, Half, Random} _tx_period_e;
   integer     tx_period;
   rand bit [31:0] nFrames;
   rand bit [3:0] mbox_size;
   string      name;
   int	       seed;
   bit	       verbose;
   int	       rx_timeout_ns, rx_timeout_step_ns;
   t_gen_mode  gen_mode;
   t_Bframe    BtrainProg[];

   constraint c_nFrames_reasonable
     {nFrames > 100; nFrames < 250; }

   function new(input string name,
		input int seed,
		input bit verbose,
		input int rx_timeout_ns = 15000,
		input int rx_timeout_step_ns = 100);

      this.name = name;
      this.seed = seed;
      this.verbose = verbose;
      this.rx_timeout_ns = rx_timeout_ns;
      this.rx_timeout_step_ns = rx_timeout_step_ns;
      this.gen_mode = random; //default mode

      srandom(seed);
   endfunction // new

   function void post_randomize();
      case(_tx_period_e)
	Full:   tx_period = 250;
	Half:   tx_period = 125;
	Random: tx_period = $urandom_range(126, 249);
      endcase // case (_tx_period_e)
   endfunction // post_randomize

   // this function allows pre-programmed (non-random) sequence of BTrain
   // frames to be transmitted
   function void set_config(integer tx_period, input t_Bframe BtrainProg[]);
      this.gen_mode  = programmed;
      this.tx_period = tx_period;
      this.nFrames   = $size(BtrainProg);
      this.BtrainProg= BtrainProg;

   endfunction // set_config

   function void display();
      $write("[%t] %s/Cfg: tx_period = %0d, nFrames = %0d, mbox_size = %0d, verbose = %b",
	     $time, name, tx_period, nFrames, mbox_size, verbose);
      $display;
   endfunction // display

endclass // Config

/// ///////////////////////////////////////////////////////////////////////////
/// BTrainTr Class
///
/// This class holds all the information relevant to a BTrain Transaction, in
/// other words a BTrain frame transmission or reception.
///
/// This class is instantiated by the BTrain Generator class as a blueprint
/// which is then copied to generate the actual transactions.
/// ///////////////////////////////////////////////////////////////////////////
class BTrainTr;
   Config cfg;
   int	       id;
   rand enum {BFrame, IFrame, CFrame, Unknown} _ftype_e;
   rand bit ver_id, d_low_m, f_low_m, zero_cyc, C0, error, sim_eff;
   rand bit [7:0] ftype;
   rand bit [31:0] B, Bdot, oldB, measB, simB, synB, Im;
   rand bit [31:0] I, V, UserData, Reserved_1, Reserved_2;
   rand bit [15:0] MSrcId, CSrcId;

   // Distribution between various types of frames
   constraint ftype_dist
     {_ftype_e dist {BFrame := 60, IFrame := 20, CFrame := 15, Unknown := 5}; }

   // Make the link between _ftype_e and ftype
   // if _ftype_e is Unknown, then a random non valid number will be generated
   constraint ftype_set
     {(_ftype_e == BFrame) -> (ftype == c_ID_BkFrame);
      (_ftype_e == IFrame) -> (ftype == c_ID_ImFrame);
      (_ftype_e == CFrame) -> (ftype == c_ID_CdFrame);
      (_ftype_e == Unknown) -> !(ftype inside {c_ID_BkFrame, c_ID_ImFrame, c_ID_CdFrame}); }

   function new(const ref Config cfg,
		input int id = 0);
      this.cfg = cfg;
      this.id = id;
      srandom(cfg.seed);
   endfunction // new

   /*
    * Returns 1 if the type of the frame is known/valid
    */
   function bit type_is_valid();
      return ((ftype == c_ID_BkFrame) ||
	      (ftype == c_ID_ImFrame) ||
	      (ftype == c_ID_CdFrame));
   endfunction // is_valid

   /*
    * Returns 1 if this frame is the same as the "to" argument
    */
   function bit compare(const ref BTrainTr to);
      // check header
      if ((this.ftype    != to.ftype)    ||
          (this.ver_id   != to.ver_id)   ||
          (this.d_low_m  != to.d_low_m)  ||
          (this.f_low_m  != to.f_low_m)  ||
          (this.zero_cyc != to.zero_cyc) ||
          (this.C0       != to.C0)       ||
          (this.error    != to.error)    ||
          (this.sim_eff  != to.sim_eff))
	return 0;

      // check payload
      case(ftype)
	c_ID_BkFrame: begin
	   return ((this.B     == to.B)     &&
		   (this.Bdot  == to.Bdot)  &&
		   (this.oldB  == to.oldB)  &&
		   (this.measB == to.measB) &&
		   (this.simB  == to.simB)  &&
		   (this.synB  == to.synB));
	end
	c_ID_ImFrame: begin
	   return (this.Im == to.Im);
	end
	c_ID_CdFrame: begin
	   return ((this.I          == to.I)          &&
		   (this.V          == to.V)          &&
		   (this.MSrcId     == to.MSrcId)     &&
		   (this.CSrcId     == to.CSrcId)     &&
		   (this.UserData   == to.UserData)   &&
		   (this.Reserved_1 == to.Reserved_1) &&
		   (this.Reserved_2 == to.Reserved_2));
	end
	default: begin
	   // return true, we only care about ftype
	   // being the same in this case
	   return 1;
	end
      endcase // case (this.ftype)

   endfunction // compare

   /*
    * Make copy of this frame into the "to" argument
    */
   function BTrainTr copy(input BTrainTr to = null);
      BTrainTr tr;
      if (to == null)
	tr = new(cfg);
      else
	tr = to;
      tr.id      = this.id;
      // header
      tr.ftype   = this.ftype;
      tr.ver_id  = this.ver_id;
      tr.d_low_m = this.d_low_m;
      tr.f_low_m = this.f_low_m;
      tr.zero_cyc= this.zero_cyc;
      tr.C0      = this.C0;
      tr.error   = this.error;
      tr.sim_eff = this.sim_eff;
      // B frame
      tr.B       = this.B;
      tr.Bdot    = this.Bdot;
      tr.oldB    = this.oldB;
      tr.measB   = this.measB;
      tr.simB    = this.simB;
      tr.synB    = this.synB;
      // I frame
      tr.Im      = this.Im;
      //C frame
      tr.I          = this.I;
      tr.V          = this.V;
      tr.MSrcId     = this.MSrcId;
      tr.CSrcId     = this.CSrcId;
      tr.UserData   = this.UserData;
      tr.Reserved_1 = this.Reserved_1;
      tr.Reserved_2 = this.Reserved_2;
      return tr;
   endfunction // copy

   /*
    * Pretty-print frame information
    */
   function void display(input string prefix = "");
      if (!cfg.verbose)	return;
      $write("%sBTrainTr %0d: type=", prefix, id);
      case(ftype)
	c_ID_BkFrame: begin
	   $write("BFrame, ");
	   $write("B=%0d, Bdot=%0d, oldB=%0d, ", B, Bdot, oldB);
	   $write("measB=%0d, simB=%0d, synB=%0d", measB, simB, synB);
	end
	c_ID_ImFrame: begin
	   $write("IFrame, I=%0d", Im);
	end
	c_ID_CdFrame: begin
	   $write("CFrame, ");
	   $write("I=%0d, V=%0d, ", I, V);
	   $write("MSrcId=%0d, CSrcId=%0d, ", MSrcId, CSrcId);
	   $write("UserData=%0d, ", UserData);
	   $write("Reserved_1=%0d, Reserved_2=%0d, ", Reserved_1, Reserved_2);
	end
	default: begin
	   $write("UNKNOWN (0x%0x)", ftype);
	end
      endcase // case (ftype)
      $display;
   endfunction // display

endclass // BTrainTr

/// ///////////////////////////////////////////////////////////////////////////
/// BTrain Generator Class
///
/// This class instantiates a BTrainTr blueprint instance and then uses it
/// to generate N random frames (N taken from the Config class) and submit
/// them to the Driver class via the gen2drv mailbox.
///
/// This class is instantiated by the Environment class
/// ///////////////////////////////////////////////////////////////////////////
class BTrain_generator;
   Config cfg;
   BTrainTr blueprint;
   mailbox     #(BTrainTr) gen2drv;
   int	       fCount;

   function new (const ref Config cfg,
		 input mailbox gen2drv,
		 input int     fCount = 0);
      this.cfg     = cfg;
      this.gen2drv = gen2drv;
      this.fCount  = 0;
      srandom(cfg.seed);
      // instantiate once the BTrainTr blueprint here
      blueprint    = new(cfg);
   endfunction // new

   // load pre-programmed frame content
   task load(t_Bframe f);
     // header
     this.blueprint.ftype     = f.header.frame_type;
     this.blueprint.ver_id    = f.header.version_id;
     this.blueprint.d_low_m   = f.header.d_low_marker;
     this.blueprint.f_low_m   = f.header.f_low_marker;
     this.blueprint.zero_cyc  = f.header.zero_cycle;
     this.blueprint.C0        = f.header.C0;
     this.blueprint.error     = f.header.error;
     this.blueprint.sim_eff   = f.header.sim_eff;

     //payload
     case(f.f_type)
       B: begin
         this.blueprint.B     = f.payload_B.B;
         this.blueprint.Bdot  = f.payload_B.Bdot;
         this.blueprint.oldB  = f.payload_B.oldB;
         this.blueprint.measB = f.payload_B.measB;
         this.blueprint.simB  = f.payload_B.simB;
         this.blueprint.synB  = f.payload_B.synB;
       end
       I: begin
         this.blueprint.Im    = f.payload_I.I;
       end
       C: begin
         this.blueprint.I          = f.payload_C.I;
         this.blueprint.V          = f.payload_C.V;
         this.blueprint.MSrcId     = f.payload_C.MSrcId;
         this.blueprint.CSrcId     = f.payload_C.CSrcId;
         this.blueprint.UserData   = f.payload_C.UserData;
         this.blueprint.Reserved_1 = f.payload_C.Reserved_1;
         this.blueprint.Reserved_2 = f.payload_C.Reserved_2;
       end
     endcase //case(this.blueprint.f_type)
   endtask //load()

   task run();
      BTrainTr tr;
      repeat (cfg.nFrames) begin
	 if(cfg.gen_mode == random) begin
	    // randomize the blueprint
	    `SV_RAND_CHECK(blueprint.randomize());
	 end else if(cfg.gen_mode == programmed) begin
	    // load frame content
	    load(cfg.BtrainProg[blueprint.id]);
	 end
	 // increase the id to differentiate between the copies
	 blueprint.id++;
	 // make a copy, casting is not really necessary, but it's
	 // good for possible future extensions to the BTrainTr class
	 $cast(tr, blueprint.copy());
	 tr.display($sformatf("[%t] %s/Gen: ", $time, cfg.name));
	 // submit the generated transaction to the Driver
	 gen2drv.put(tr);
      end
   endtask // run

endclass // BTrain_generator

/// ///////////////////////////////////////////////////////////////////////////
/// Driver Class
///
/// This class receives a BTrainTr transaction from the Generator via the
/// gen2drv mailbox and drives it on the BTrain Tx interface, while respecting
/// the tx_period configuration value (from the Config class).
/// When the frame has been sent, the Driver also submits the transaction to
/// the Scoreboard class for book-keeping, via the drv2scr mailbox.
///
/// This class is instantiated by the Environment class
/// ///////////////////////////////////////////////////////////////////////////
class Driver;
   Config cfg;
   vIBTrainTx Tx;
   vILink lnk;
   mailbox   #(BTrainTr) gen2drv, drv2scr;

   function new (const ref Config cfg,
		 input	       vIBTrainTx Tx,
		 input         vILink lnk,
		 input mailbox gen2drv,
		 input mailbox drv2scr);
      this.cfg     = cfg;
      this.Tx      = Tx;
      this.lnk     = lnk;
      this.gen2drv = gen2drv;
      this.drv2scr = drv2scr;
      srandom(cfg.seed);
   endfunction // new

   // Init Tx signals
   task init_to_zero();
      Tx.cbtx.FrameHeader_version_id   <= 0;
      Tx.cbtx.FrameHeader_d_low_marker <= 0;
      Tx.cbtx.FrameHeader_f_low_marker <= 0;
      Tx.cbtx.FrameHeader_zero_cycle   <= 0;
      Tx.cbtx.FrameHeader_C0           <= 0;
      Tx.cbtx.FrameHeader_error        <= 0;
      Tx.cbtx.FrameHeader_sim_eff      <= 0;
      Tx.cbtx.tx_TransmitFrame_p1      <= 0;
      Tx.cbtx.FrameHeader_frame_type   <= 0;
      Tx.cbtx.BFramePayload_B          <= 0;
      Tx.cbtx.BFramePayload_Bdot       <= 0;
      Tx.cbtx.BFramePayload_oldB       <= 0;
      Tx.cbtx.BFramePayload_measB      <= 0;
      Tx.cbtx.BFramePayload_simB       <= 0;
      Tx.cbtx.BFramePayload_synB       <= 0;
      Tx.cbtx.IFramePayload_I          <= 0;
      Tx.cbtx.CFramePayload_I          <= 0;
      Tx.cbtx.CFramePayload_V          <= 0;
      Tx.cbtx.CFramePayload_MSrcId     <= 0;
      Tx.cbtx.CFramePayload_CSrcId     <= 0;
      Tx.cbtx.CFramePayload_UserData   <= 0;
      Tx.cbtx.CFramePayload_Reserved_1 <= 0;
      Tx.cbtx.CFramePayload_Reserved_2 <= 0;
   endtask // init_to_zero

   task wait_ready();
      if (Tx.cbtx.tx_ready == 1)
	return;
      wait(Tx.cbtx.tx_ready == 1);
   endtask // wait_ready

   task run();
      BTrainTr tr;

      init_to_zero();

      // Wait for reset to finish
      wait(Tx.rst_n == 1);

      $display("[%t] %s/Drv: Wait for TX ready (requires 350us after reset)",
	       $time, cfg.name);
      // Wait for first TX ready before sending
      wait_ready();
      $display("[%t] %s/Drv: TX ready", $time, cfg.name);

      // Random wait, 0 to 10us
      #($urandom_range(0, 10000)*1ns);

      $display("[%t] %s/Drv: Start TX", $time, cfg.name);

      // Check for transactions and send them every tx_period
      // clock cycles (subtracting any cycles used for sending)
      forever begin
	 gen2drv.get(tr);
	 send(tr);
	 tr.display($sformatf("[%t] %s/Drv: ", $time, cfg.name));
	 drv2scr.put(tr);
	 if (cfg.tx_period > 1)
	   repeat (cfg.tx_period - 2) @(Tx.cbtx);
      end

   endtask // run

   task send(input BTrainTr tr);
      wait_ready();
      @(Tx.cbtx);
      Tx.cbtx.tx_TransmitFrame_p1      <= 1;
      Tx.cbtx.FrameHeader_version_id   <= tr.ver_id;
      Tx.cbtx.FrameHeader_d_low_marker <= tr.d_low_m;
      Tx.cbtx.FrameHeader_f_low_marker <= tr.f_low_m;
      Tx.cbtx.FrameHeader_zero_cycle   <= tr.zero_cyc;
      Tx.cbtx.FrameHeader_C0           <= tr.C0;
      Tx.cbtx.FrameHeader_error        <= tr.error;
      Tx.cbtx.FrameHeader_sim_eff      <= tr.sim_eff;
      Tx.cbtx.FrameHeader_frame_type <= tr.ftype;
      case(tr.ftype)
	c_ID_BkFrame: begin
	   Tx.cbtx.BFramePayload_B     <= tr.B;
	   Tx.cbtx.BFramePayload_Bdot  <= tr.Bdot;
	   Tx.cbtx.BFramePayload_oldB  <= tr.oldB;
	   Tx.cbtx.BFramePayload_measB <= tr.measB;
	   Tx.cbtx.BFramePayload_simB  <= tr.simB;
	   Tx.cbtx.BFramePayload_synB  <= tr.synB;
	end
	c_ID_ImFrame: begin
	   Tx.cbtx.IFramePayload_I <= tr.Im;
	end
	c_ID_CdFrame: begin
	   Tx.cbtx.CFramePayload_I          <= tr.I;
	   Tx.cbtx.CFramePayload_V          <= tr.V;
	   Tx.cbtx.CFramePayload_MSrcId     <= tr.MSrcId;
	   Tx.cbtx.CFramePayload_CSrcId     <= tr.CSrcId;
	   Tx.cbtx.CFramePayload_UserData   <= tr.UserData;
	   Tx.cbtx.CFramePayload_Reserved_1 <= tr.Reserved_1;
	   Tx.cbtx.CFramePayload_Reserved_2 <= tr.Reserved_2;
	end
      endcase // case (tr.ftype)
      @(Tx.cbtx);
      Tx.cbtx.tx_TransmitFrame_p1 <= 0;
   endtask // send

endclass // Driver

/// ///////////////////////////////////////////////////////////////////////////
/// Monitor Class
///
/// This class monitors the BTrain Rx interface for incoming frames and
/// assembles a BTrainTr transaction, which it then submits to the Scoreboard
/// class for book-keeping via the mon2scr mailbox.
///
/// This class is instantiated by the Environment class
/// ///////////////////////////////////////////////////////////////////////////
class Monitor;
   Config cfg;
   vIBTrainRx Rx;
   mailbox   #(BTrainTr) mon2scr;

   function new (const ref Config cfg,
		 input	       vIBTrainRx Rx,
		 input mailbox mon2scr);
      this.cfg     = cfg;
      this.Rx      = Rx;
      this.mon2scr = mon2scr;
      srandom(cfg.seed);
   endfunction // new

   task run();
      BTrainTr tr;

      // Wait for reset to finish
      wait(Rx.rst_n == 1);

      // Check for received frames and push the transactions
      // to the scoreboard
      forever begin
	 receive(tr);
	 tr.display($sformatf("[%t] %s/Mon: ", $time, cfg.name));
	 mon2scr.put(tr);
      end

   endtask // run

   task receive(ref BTrainTr tr);
      @(posedge Rx.cbrx.rx_Frame_valid_pX);
      tr = new(cfg);
      //header
      tr.ver_id  = Rx.cbrx.FrameHeader_version_id;
      tr.d_low_m = Rx.cbrx.FrameHeader_d_low_marker;
      tr.f_low_m = Rx.cbrx.FrameHeader_f_low_marker;
      tr.zero_cyc= Rx.cbrx.FrameHeader_zero_cycle;
      tr.C0      = Rx.cbrx.FrameHeader_C0;
      tr.error   = Rx.cbrx.FrameHeader_error;
      tr.sim_eff = Rx.cbrx.FrameHeader_sim_eff;
      tr.ftype   = Rx.cbrx.FrameHeader_frame_type;
      //payloads
      tr.B       = Rx.cbrx.BFramePayload_B;
      tr.Bdot    = Rx.cbrx.BFramePayload_Bdot;
      tr.oldB    = Rx.cbrx.BFramePayload_oldB;
      tr.measB   = Rx.cbrx.BFramePayload_measB;
      tr.simB    = Rx.cbrx.BFramePayload_simB;
      tr.synB    = Rx.cbrx.BFramePayload_synB;
      tr.Im      = Rx.cbrx.IFramePayload_I;
      tr.I          = Rx.cbrx.CFramePayload_I;
      tr.V          = Rx.cbrx.CFramePayload_V;
      tr.MSrcId     = Rx.cbrx.CFramePayload_MSrcId;
      tr.CSrcId     = Rx.cbrx.CFramePayload_CSrcId;
      tr.UserData   = Rx.cbrx.CFramePayload_UserData;
      tr.Reserved_1 = Rx.cbrx.CFramePayload_Reserved_1;
      tr.Reserved_2 = Rx.cbrx.CFramePayload_Reserved_2;

   endtask // receive

endclass // Monitor

/// ///////////////////////////////////////////////////////////////////////////
/// Scoreboard Class
///
/// This class keeps track of transmitted frames and tries to match them to
/// received ones. It monitors the drv2scr mailbox for transmitted frames and
/// then waits (with a timeout configurable via the Config class) for the frame
/// to also appear on the mon2scr mailbox.
///
/// This class is instantiated by the Environment class
/// ///////////////////////////////////////////////////////////////////////////
class Scoreboard;
   Config cfg;
   mailbox   #(BTrainTr) drv2scr, mon2scr;
   int	     b_count, i_count, c_count, u_count;
   int	     rx_timeout_ns;
   bit       rx_rcvd;
   function new (const ref Config cfg,
		 input mailbox drv2scr,
		 input mailbox mon2scr);
      this.cfg     = cfg;
      this.drv2scr = drv2scr;
      this.mon2scr = mon2scr;
      this.b_count = 0; // B
      this.i_count = 0; // I
      this.c_count = 0; // C
      this.u_count = 0; // Unknown
      srandom(cfg.seed);
   endfunction // new

   /*
    * Returns number of matched (seen by both Tx and Rx) frames
    */
   function int matched();
      return (b_count + i_count + c_count);
   endfunction // matched

   /*
    * Returns number of total frames
    */
   function int total();
      return (matched() + u_count);
   endfunction // total

   task run();

      BTrainTr tr_sent, tr_rcvd;

      // Check for sent transactions and compare them against
      // received ones
      do begin
	 // block waiting for transmitted frame
	 drv2scr.get(tr_sent);
	 // if type is unknown, discard it
	 if (!tr_sent.type_is_valid) begin
	    u_count++;
	    continue;
	 end
	 // Set a timeout, to avoid waiting forever
	 rx_timeout_ns = 0;
	 rx_rcvd = 0;
	 while (rx_timeout_ns < cfg.rx_timeout_ns)
	   begin
	      rx_rcvd = mon2scr.try_get(tr_rcvd);
	      if (rx_rcvd > 0)
		break;
	      #(cfg.rx_timeout_step_ns * 1ns);
	      rx_timeout_ns += cfg.rx_timeout_step_ns;
	   end
	 // Complain if the timeout was reached and nothing was received
	 assert (rx_rcvd > 0) else begin
	    tr_sent.display($sformatf("[%t] %s/Scr: ", $time, cfg.name));
	    $fatal(1, "[%t] %s/Scr: unexpected lost frame detected", $time, cfg.name);
	 end
	 // If something was received, compare it against the transmitted one
	 assert(tr_sent.compare(tr_rcvd)) else begin
	    tr_sent.display($sformatf("[%t] %s/Scr/Sent: ", $time, cfg.name));
	    tr_rcvd.display($sformatf("[%t] %s/Scr/Rcvd: ", $time, cfg.name));
	    $fatal(1, "[%t] %s/Scr: frame mismatch", $time, cfg.name);
	 end
	 // Frame type book-keeping
	 case (tr_sent.ftype)
	   c_ID_BkFrame:  b_count++;
	   c_ID_ImFrame:  i_count++;
	   c_ID_CdFrame:  c_count++;
	 endcase // case (tr_sent.ftype)
      end while (total() < cfg.nFrames); // do begin
      // When done, print a short summary
      $write("[%t] %s/Scr: %0d total frames: ", $time, cfg.name, total());
      $write("%0d matched ( %0d B, %0d I, %0d C), %0d unknown (skipped)",
	     matched(), b_count, i_count, c_count, u_count);
      $display;
   endtask // run

endclass // Scoreboard

/// ///////////////////////////////////////////////////////////////////////////
/// BTrain Interface
/// ///////////////////////////////////////////////////////////////////////////
interface IBTrain
  (input	rst_n,
   input	clk,
   inout [1:0]	FrameHeader_version_id,
   inout	FrameHeader_d_low_marker,
   inout	FrameHeader_f_low_marker,
   inout	FrameHeader_zero_cycle,
   inout	FrameHeader_C0,
   inout	FrameHeader_error,
   inout	FrameHeader_sim_eff,
   inout [7:0]	FrameHeader_frame_type,
   inout [31:0] BFramePayload_B,
   inout [31:0] BFramePayload_Bdot,
   inout [31:0] BFramePayload_oldB,
   inout [31:0] BFramePayload_measB,
   inout [31:0] BFramePayload_simB,
   inout [31:0] BFramePayload_synB,
   inout [31:0] IFramePayload_I,
   inout [31:0] CFramePayload_I,
   inout [31:0] CFramePayload_V,
   inout [15:0] CFramePayload_MSrcId,
   inout [15:0] CFramePayload_CSrcId,
   inout [31:0] CFramePayload_UserData,
   inout [31:0] CFramePayload_Reserved_1,
   inout [31:0] CFramePayload_Reserved_2,

   input	rx_Frame_valid_pX,
   input [7:0]	rx_Frame_typeID,
   input	tx_ready,
   output	tx_TransmitFrame_p1,
   input        bup,
   input        bdown);

   clocking cbtx @(posedge clk);
      input     tx_ready;
      output	FrameHeader_version_id,
		FrameHeader_d_low_marker,
		FrameHeader_f_low_marker,
		FrameHeader_zero_cycle,
		FrameHeader_C0,
		FrameHeader_error,
		FrameHeader_sim_eff,
		tx_TransmitFrame_p1,
		FrameHeader_frame_type,
		BFramePayload_B,
		BFramePayload_Bdot,
		BFramePayload_oldB,
		BFramePayload_measB,
		BFramePayload_simB,
		BFramePayload_synB,
		IFramePayload_I,
		CFramePayload_I,
		CFramePayload_V,
		CFramePayload_MSrcId,
		CFramePayload_CSrcId,
		CFramePayload_UserData,
		CFramePayload_Reserved_1,
		CFramePayload_Reserved_2;
   endclocking // cbtx

   clocking cbrx @(posedge clk);
      input	FrameHeader_version_id,
		FrameHeader_d_low_marker,
		FrameHeader_f_low_marker,
		FrameHeader_zero_cycle,
		FrameHeader_C0,
		FrameHeader_error,
		FrameHeader_sim_eff,
		rx_Frame_valid_pX,
		FrameHeader_frame_type,
		rx_Frame_typeID,
		BFramePayload_B,
		BFramePayload_Bdot,
		BFramePayload_oldB,
		BFramePayload_measB,
		BFramePayload_simB,
		BFramePayload_synB,
		IFramePayload_I,
		CFramePayload_I,
		CFramePayload_V,
		CFramePayload_MSrcId,
		CFramePayload_CSrcId,
		CFramePayload_UserData,
		CFramePayload_Reserved_1,
		CFramePayload_Reserved_2,
		bup,
		bdown;
   endclocking // cbrx

   modport TB_Tx(clocking cbtx, input rst_n);

   modport TB_Rx(clocking cbrx, input rst_n);

endinterface // IBTrain

/// ///////////////////////////////////////////////////////////////////////////
/// Physical link Interface
/// ///////////////////////////////////////////////////////////////////////////
interface ILink ();

   logic	p, n;
   logic	break_link;

   task bring_down();
      break_link = 1;
   endtask // bring_down

   task bring_up();
      break_link = 0;
   endtask // bring_up

endinterface // ILink

