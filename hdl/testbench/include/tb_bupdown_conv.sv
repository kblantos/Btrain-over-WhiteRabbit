//-----------------------------------------------------------------------------
// CERN
// BTrain-over-WhiteRabbit
// https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
//-----------------------------------------------------------------------------
//
// unit name:     tb_bupdown_conv.sv
//
// description:
//
// Classes specific to bupdown_converter
//
//-----------------------------------------------------------------------------
// Copyright (c) 2018 CERN BE/CO/HT
//-----------------------------------------------------------------------------
// GNU LESSER GENERAL PUBLIC LICENSE
//-----------------------------------------------------------------------------
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------

// scaling factor, i.e. what is the correspondence of one bup/bdown pulse
// to a basic unit (resolution) of Bvalue conveyed in the BTrain frame
const int      c_BupDownScaling = 1000;

/// ///////////////////////////////////////////////////////////////////////////
/// Counter Class
///
/// This class monitors the bup/bdown pulses and the C0 bit in the header
/// of BTrain frame. Based on the observation, submitted maintaines a counter of
/// the Bvalue. The value of this counter is pushed to the Scoreboard each
/// time a pulse is received.
///
/// This class is instantiated by the Environment class
/// ///////////////////////////////////////////////////////////////////////////
class Counter;
  Config cfg;
  vIBTrainRx Rx;
  mailbox #(int) cnt2scp;

  function new (const ref Config cfg,
                input            vIBTrainRx Rx,
                input mailbox    cnt2scp);
    this.cfg     = cfg;
    this.Rx      = Rx;
    this.cnt2scp = cnt2scp;
    srandom(cfg.seed);
  endfunction // new

  task run();
    int Bcnt;

    // Wait for reset to finish
    wait(Rx.rst_n == 1);
    Bcnt = 0;

    // Check for received frames and push the transactions
    // to the scoreboard
    forever begin
      countBpulses(Bcnt);
      cnt2scp.put(Bcnt);
    end
  endtask // run

  task countBpulses(ref int Bcnt);

    @(posedge Rx.cbrx.bup || Rx.cbrx.bdown || Rx.cbrx.FrameHeader_C0 );
      if(cfg.verbose == 1) begin
        if(Rx.cbrx.FrameHeader_C0 == 1) begin
          $display("[%t] %s/Cnt: reset cnt,   Bcnt = 0 (C0)", $time, cfg.name);
        end else if(Rx.cbrx.bup == 1 && Rx.cbrx.bdown == 0) begin
          $display("[%t] %s/Cnt: Bup pulse,   Bcnt = %0d + %0d",
                   $time, cfg.name, Bcnt, c_BupDownScaling);
        end else if(Rx.cbrx.bup == 0 && Rx.cbrx.bdown == 1) begin
          $display("[%t] %s/Cnt: Bdown pulse, Bcnt = %0d - %0d",
                   $time, cfg.name, Bcnt, c_BupDownScaling);
        end
      end // if(cfg.verbose == 1)
      if(Rx.cbrx.FrameHeader_C0 == 1)                 Bcnt = 0;
      else if(Rx.cbrx.bup == 1 && Rx.cbrx.bdown == 0) Bcnt=Bcnt+c_BupDownScaling;
      else if(Rx.cbrx.bup == 0 && Rx.cbrx.bdown == 1) Bcnt=Bcnt-c_BupDownScaling;

  endtask // countBpulses
endclass // Counter

/// ///////////////////////////////////////////////////////////////////////////
/// Scoreboard_UpDown Class
///
/// This class keeps track of
/// 1) the Bvalue counted from bup/bdown pulses
/// 2) the Bvalue received in the BTrain messages
/// and verifies that these values are the same
/// ///////////////////////////////////////////////////////////////////////////
class ScoreboardForPulses;
  Config cfg;
  mailbox   #(BTrainTr) mon2scr;
  mailbox   #(int)  cnt2scr;
  int       WR_B, cnt_B;
  int       rx_timeout_ns;
  int       f_cnt;
  bit       rx_rcvd;
  function new (const ref Config cfg,
                input mailbox    mon2scr,
                input mailbox    cnt2scr);
    this.cfg     = cfg;
    this.mon2scr = mon2scr;
    this.cnt2scr = cnt2scr;
    this.WR_B    = 0;
    this.cnt_B   = 0;
    this.f_cnt   = 0;
  endfunction // new

  task receiveBTrainFrames();

    BTrainTr tr_rcvd;

    // Check for sent transactions and compare them against
    // received ones
    do begin
       // wait to receive something
       mon2scr.get(tr_rcvd);
       f_cnt++;
       if(tr_rcvd.ftype == c_ID_BkFrame) begin
         if(cfg.verbose == 1) begin
           $write("[%t] %s/Scr: When Bframe received",$time, cfg.name, tr_rcvd.B);
           $write(" the value were: WR_B =%0d vs. cnt_B=%0d", WR_B, cnt_B);
           $display;
         end
  
         assert (WR_B>(cnt_B-c_BupDownScaling) && WR_B<(cnt_B+c_BupDownScaling)) else
         $warning(1, "[%t] %s/Scr: mismatch before reception of new BTrain ",
                     "frame WR_B=%0d and cnt_B=%0d ", $time, cfg.name, WR_B,
                     cnt_B);
         WR_B  = tr_rcvd.B;
       end;

    end while (f_cnt < cfg.nFrames); // do begin
      $display("[%t] %s/Scr: received all frames",$time, cfg.name, WR_B);
  endtask // receiveBupBdownCnt

  task receiveBupBdownCnt();
    int cnt_rcvd;
    int rx_rcvd = 0;
    // Check for sent transactions and compare them against
    // received ones
    do begin
      // wait to receive something
      cnt2scr.get(cnt_rcvd);
      $display("[%t] %s/Scr: received Bcnt = %0d",$time, cfg.name,cnt_rcvd);
      cnt_B = cnt_rcvd;
      do begin
        #2us //if this is a burst, we should have already received next pulse
        rx_rcvd = 0;
        rx_rcvd = cnt2scr.try_get(cnt_rcvd);
        if(rx_rcvd > 0) begin
          $display("[%t] %s/Scr: received Bcnt = %0d",$time, cfg.name,cnt_rcvd);
          cnt_B = cnt_rcvd;
        end else begin
          $display("[%t] %s/Scr: did not receive",$time, cfg.name);
        end
      end while(rx_rcvd > 0);
      if(cfg.verbose == 1) begin
        $write("[%t] %s/Scr: After train of pulses or C0",$time, cfg.name);
        $write(" the values are: WR_B =%0d vs. cnt_B=%0d", WR_B, cnt_B);
        $display;
      end

      assert (WR_B>(cnt_B-c_BupDownScaling) && WR_B<(cnt_B+c_BupDownScaling)) else
        $warning(1, "[%t] %s/Scr: mismatch after train of pulses WR_B=%0d ",
                    "and cnt_B=%0d ", $time, cfg.name, WR_B, cnt_B);

    end while (f_cnt < cfg.nFrames); // do begin
    $display("[%t] %s/Scr: received all frames",$time, cfg.name, WR_B );
  endtask // receiveBupBdownCnt

  task run();
    fork
      receiveBTrainFrames();
      receiveBupBdownCnt();
    join
  endtask // run();
endclass // Scoreboard_UpDown

