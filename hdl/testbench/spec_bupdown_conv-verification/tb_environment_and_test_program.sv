//-----------------------------------------------------------------------------
// CERN
// BTrain-over-WhiteRabbit
// https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
//-----------------------------------------------------------------------------
//
// unit name:     tb_environment_and_test_program.sv
//
// description:
//
// SystemVerilog file with environment and test program.
//
//-----------------------------------------------------------------------------
// Copyright (c) 2018 CERN BE/CO/HT
//-----------------------------------------------------------------------------
// GNU LESSER GENERAL PUBLIC LICENSE
//-----------------------------------------------------------------------------
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
`include "common_macros.svh"       // macros common to all reference designs
`include "tb_btrain_ref_design.sv" // classes common to btrain_ref_design
`include "wb_bupdown_sampler.sv"   // bupdown_sampler
`include "tb_bupdown_conv.sv"      // classes specific to bupdown_conveter
`include "tb_frame_program.svh"    // list of frames to be sent (a program)

/*
 * The BUpDownConv "management" class
 *
 * Extends the class CWBBTRainMgt (that includes one instance of CWBStreamer
 * and CWBBTrain) to include also CWBBUpDownSampler.
 * Peripheral base offsets corresond to the BTrain BUpDown Conv design.
 * Compared with the base class, this class
 * * overrides methods new(), init() and run()
 * * adds      mehtod bupdown_sample()
 * * uses      method wrap_up() from the base class
 */
class CWBBUpDownConvMgt extends CWBBTRainMgt;

   // base offsets/pointers for WR peripherals
   const uint32_t c_BASE_SAMPLER     = 'h00020000;

   CWBBUpDownSampler sampler;

   protected string name;

   function new(input string name, const ref CBusAccessor bus);
      super.new(name, bus);
      this.sampler  = new(name, bus, c_BASE_SAMPLER);
   endfunction // new

   /*
    * Refresh status for both peripherals and make sure they are present
    */
   task init();
      wrstream.refresh_status();
      wrbtrain.refresh_status();
      sampler.refresh_status();
      assert ((wrstream.present == 1) && (wrbtrain.present == 1) &&
              (sampler.present == 1))
        $display("[%t] %s: init OK", $time, name);
      else
        $fatal(1, "[%t] %s: init FAILED", $time, name);
   endtask // refresh_status

   /*
    * Initialize class and reset streamer stats
    */
   task run();
      init();
      wrstream.stats_acq_start();
   endtask // run

   task bupdown_sample();
     sampler.cmode(1 /*1: C0 from Btrain frame*/);
     sampler.acq_mode(2 /*2:start acq when C0*/);
     sampler.speriod(3); /*take only every 3rd sample*/
     sampler.acq_stat_read();
     sampler.acq_start();
     sampler.acq_stat_read();
     sampler.acq_stat_read();
   endtask

endclass // CWBBUpDownConvMgt

/// ///////////////////////////////////////////////////////////////////////////
/// Environment Class
///
/// This is the top-level class which represents the complete testbench
/// environment between a BTrain transmitter and a BTrain receiver.
///
/// This class is instantiated by the testbench program itself.
/// ///////////////////////////////////////////////////////////////////////////
class Environment;

  Config cfg;
  BTrain_generator gen;
  mailbox #(BTrainTr) gen2drv, drv2scr, mon2scr, mon2scp;
  mailbox #(int) cnt2scp;
  Driver drv;
  Monitor mon1, mon2;
  Counter cnt;
  Scoreboard scr;
  ScoreboardForPulses scp;
  vIBTrainTx Tx;
  vIBTrainRx Rx;
  vILink lnk;
  CBusAccessor WB;

  function new (input        vIBTrainTx Tx,
                input        vIBTrainRx Rx,
                input        vILink lnk,
                input string name,
                input int    seed,
                input bit    verbose);
    this.Tx = Tx;
    this.Rx = Rx;
    this.lnk = lnk;
    srandom(seed);
    cfg = new(name, seed, verbose);

  endfunction // new

  function void gen_cfg(integer tx_period, input t_Bframe BtrainProg[]);
    cfg.set_config(tx_period /*[ns]*/, BtrainProg);
    cfg.display();
  endfunction // gen_cfg

  function void build();
    gen2drv = new(cfg.mbox_size);
    drv2scr = new();
    mon2scr = new();
    mon2scp = new();
    cnt2scp = new();
    gen     = new(cfg, gen2drv);
    drv     = new(cfg, Tx, lnk, gen2drv, drv2scr);
    mon1    = new(cfg, Rx, mon2scr);
    mon2    = new(cfg, Rx, mon2scp);
    cnt     = new(cfg, Rx, cnt2scp);
    scr     = new(cfg, drv2scr, mon2scr);
    scp     = new(cfg, mon2scp, cnt2scp);
  endfunction // build

  task run();

    fork
      gen.run();
      drv.run();
      mon1.run();
      mon2.run();
      cnt.run();
      scp.run();
    join_none

    scr.run();

  endtask // run

endclass // Environment

/// ///////////////////////////////////////////////////////////////////////////
/// The BTrain reference design testbench program
///
/// This program instantiates two Environments, one for transmission from
/// DUT A to DUT B, and another one in the opposite direction.
///
/// Apart from trasmitting frames, it also verifies that the bupdown_conveter
/// works
/// ///////////////////////////////////////////////////////////////////////////
program automatic test_btrain_bupdown_converter
  (IBTrain.TB_Tx    TxA, TxB,
   IBTrain.TB_Rx    RxA, RxB,
   ILink            LinkA2B, LinkB2A,
   ref CBusAccessor accA, accB,
   input bit        verbose);

  CWBBUpDownConvMgt mgtA, mgtB;

  Environment envA2B, envB2A;

  const string nameA = "A2B", nameB = "B2A";

  vIBTrainTx vTxA, vTxB;
  vIBTrainRx vRxA, vRxB;
  vILink vLA2B, vLB2A;

  int i;

  initial begin

    $timeformat(-6, 3, "us", 10);

    // Assign virtual interfaces to be passed around in the various classes
    vTxA = TxA; vTxB = TxB;
    vRxA = RxA; vRxB = RxB;

    vLA2B = LinkA2B; vLB2A = LinkB2A;

    // Make sure that both physical links are enabled
    fork
      LinkA2B.bring_up();
      LinkB2A.bring_up();
    join

    // Instantiate the management classes
    mgtA = new({nameA, "/Mgt"}, accA);
    mgtB = new({nameB, "/Mgt"}, accB);

    // Instantiate the actual two environments
    envA2B = new (vTxA, vRxB, vLA2B, nameA, $urandom(), verbose);
    envB2A = new (vTxB, vRxA, vLB2A, nameB, $urandom(), verbose);

    // Wait a bit for everything to settle
    #5us;

    // Perform three runs, each using a different tx_period (see the
    // Config class for more details)
    for(i=1; i < 4; i++) begin
      $display("\n[%t] START sim run #%0d\n", $time, i);


      // Run the management classes to configure
      // the peripherals over Wishbone
      fork
        mgtA.run();
        mgtB.run();
      join

      // Configure, build and run the two environments in parallel,
      // wait for both to finish
      fork
        begin
          envA2B.gen_cfg(4000 /*[x16ns]*/, prog);
          envA2B.build();
          envA2B.run();
        end
        begin
          envB2A.gen_cfg(4000 /*[x16ns]*/, prog);
          envB2A.build();
          envB2A.run();
        end
        begin
          mgtA.bupdown_sample();
          mgtB.bupdown_sample();
        end
      join

      // Wrap up and retrieve the statistics from the streamers
      fork
        mgtA.bupdown_sample();
        mgtB.bupdown_sample();
      join
      fork
        mgtA.wrap_up();
        mgtB.wrap_up();
      join

      // Perform basic frame counter checks
      assert((mgtA.wrstream.stats.sent_frames == mgtB.wrstream.stats.rcvd_frames) &&
             (mgtB.wrstream.stats.sent_frames == mgtA.wrstream.stats.rcvd_frames) &&
             (envA2B.scr.matched() == mgtA.wrstream.stats.sent_frames) &&
             (envB2A.scr.matched() == mgtB.wrstream.stats.sent_frames) &&
             (envA2B.scr.total() == envA2B.cfg.nFrames) &&
             (envB2A.scr.total() == envB2A.cfg.nFrames) &&
             (mgtA.wrstream.stats.lost_blocks == 0) &&
             (mgtB.wrstream.stats.lost_blocks == 0) &&
             (mgtA.wrstream.stats.lost_frames == 0) &&
             (mgtB.wrstream.stats.lost_frames == 0))
      else begin
        $fatal(1, "[%t] ERROR matching frame counters", $time);
      end

      $display("\n[%t] END sim run #%0d\n", $time, i);

    end    // for(i=1; i < 4; i++)
  end      //initial begin
endprogram // test_btrain_ref_design
