onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider A->B
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/ready_o
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/tx_data_o
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/tx_valid_o
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/cmp_txCtrl/FrameHeader
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/cmp_txCtrl/Btype_payload
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/cmp_txCtrl/Itype_payload
add wave -noupdate -expand -group {DUT_A Tx} /main/DUT_A/cmp_btrain/cmp_txCtrl/Ctype_payload
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_data_i
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_valid_i
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/wb_out.btrain_simb_ctrl_scid_o
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/bup_out
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/bdown_out
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_FrameHeader_o
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_BFramePayloads_o
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_IFramePayloads_o
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_CFramePayloads_o
add wave -noupdate -expand -group {DUT_B Rx} /main/DUT_B/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group {link A2B} /main/DUT_A/cmp_xwrc_board_spec/cmp_board_common/link_ok_o
add wave -noupdate -group {link A2B} /main/LinkA2B/p
add wave -noupdate -group {link A2B} /main/LinkA2B/n
add wave -noupdate -group {link A2B} /main/LinkA2B/break_link
add wave -noupdate -divider B->A
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/ready_o
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/tx_data_o
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/tx_valid_o
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/cmp_txCtrl/FrameHeader
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/cmp_txCtrl/Btype_payload
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/cmp_txCtrl/Itype_payload
add wave -noupdate -expand -group {DUT_B Tx} /main/DUT_B/cmp_btrain/cmp_txCtrl/Ctype_payload
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_data_i
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_valid_i
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/wb_out
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/bdown_out
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/bup_out
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_FrameHeader_o
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_BFramePayloads_o
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_IFramePayloads_o
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_CFramePayloads_o
add wave -noupdate -expand -group {DUT_A Rx} /main/DUT_A/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group {link B2A} /main/DUT_A/cmp_xwrc_board_spec/cmp_board_common/link_ok_o
add wave -noupdate -group {link B2A} /main/LinkB2A/p
add wave -noupdate -group {link B2A} /main/LinkB2A/n
add wave -noupdate -group {link B2A} /main/LinkB2A/break_link
add wave -noupdate -divider {Host interface(s)}
add wave -noupdate -expand -group {DUT_A WB crossbar} /main/DUT_A/cnx_master_out
add wave -noupdate -expand -group {DUT_A WB crossbar} /main/DUT_A/cnx_master_in
add wave -noupdate -expand -group {DUT_A WB crossbar} /main/DUT_A/cnx_slave_out
add wave -noupdate -expand -group {DUT_A WB crossbar} /main/DUT_A/cnx_slave_in
add wave -noupdate -expand -group {DUT_B WB crossbar} /main/DUT_B/cnx_master_out
add wave -noupdate -expand -group {DUT_B WB crossbar} /main/DUT_B/cnx_master_in
add wave -noupdate -expand -group {DUT_B WB crossbar} /main/DUT_B/cnx_slave_out
add wave -noupdate -expand -group {DUT_B WB crossbar} /main/DUT_B/cnx_slave_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {422996000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 511
configure wave -valuecolwidth 95
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1251580715 ps}
