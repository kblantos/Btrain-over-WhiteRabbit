action   = "simulation"
sim_tool = "modelsim"

target      = "xilinx"
syn_device  = "xc6slx45t"

top_module = "main" # for hdlmake2
sim_top    = "main" # for hdlmake3

include_dirs = [
    "../include/",
    "../include/regs/",
    "../../ip_cores/gn4124-core/hdl/gn4124core/sim/gn4124_bfm/",
    "../../ip_cores/wr-cores/sim/",
    "../../ip_cores/general-cores/modules/wishbone/wb_lm32/src/"
]

modules = {
    "local" :  [
        "../../ip_cores/gn4124-core/hdl/gn4124core/sim/gn4124_bfm",
        "../../top/spec_ref_design/",
    ],
}

files = [
    "main.sv",
]
