//-----------------------------------------------------------------------------
// Title      : Testbench for verification of BTrain SPEC reference design
// Project    : BTrain over White Rabbit
// URL        : https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit
//-----------------------------------------------------------------------------
// File       : main.sv
// Author(s)  : Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
// Company    : CERN (BE-CO-HT)
// Created    : 2017-05-05
//-----------------------------------------------------------------------------
// Description:
//
// Simulation of two interconnected SPEC reference design modules (A and B)
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2017 CERN
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`include "gn4124_bfm.svh"          // for PCI core interface (IGN4124PCIMaster)
`include "spec_ref_design_macros.svh"         // macros and definitions used locally
`include "tb_environment_and_test_program.sv" // environment and test programm

module main;

   initial begin
      $display("\n\n");
      $display("/----------------------------------------\\");
      $display("|                                        |");
      $display("| SPEC BTrain Reference Design Testbench |");
      $display("|                                        |");
      $display("\\----------------------------------------/");
      $display("\n\n");
   end

   /// ////////////////////////////////////////////////////////////////////////
   /// Clocks and reset generation
   /// ////////////////////////////////////////////////////////////////////////

   logic rst_n;

   initial begin
      rst_n = 1;
      #15ns rst_n = 0;
      #15ns rst_n = 1;
   end

   logic clk_125m_pllref = 0;
   logic clk_20m_vcxo    = 0;

   always #4ns  clk_125m_pllref <= ~clk_125m_pllref;
   always #20ns clk_20m_vcxo    <= ~clk_20m_vcxo;

   /// ////////////////////////////////////////////////////////////////////////
   /// Instantiation of two interconnected DUTs
   /// ////////////////////////////////////////////////////////////////////////

   // PCI "host" interfaces
   IGN4124PCIMaster I_HostA ();
   IGN4124PCIMaster I_HostB ();

   // Bus accessor classes for each host interface. The CBusAccessor class
   // is included indirectly by tb_btrain_ref_design.sv
   CBusAccessor accA = I_HostA.get_accessor();
   CBusAccessor accB = I_HostB.get_accessor();

   // Physical link interfaces
   ILink LinkA2B(), LinkB2A();

   // Device Under Test "A"
   spec_btrain_ref_top
     #(
       .g_simulation (1),
       .g_dpram_initf("../../ip_cores/wr-cores/bin/wrpc/wrc_phy8_sim.bram")
       )
   DUT_A
     (
      .button1_n_i(rst_n),
      .sfp_txp_o(LinkA2B.p),
      .sfp_txn_o(LinkA2B.n),
      .sfp_rxp_i(LinkB2A.p),
      .sfp_rxn_i(LinkB2A.n),
      .sfp_los_i(LinkA2B.break_link),
      `SPEC_BTRAIN_REF_CONNECT_CLOCKS,
      `SPEC_BTRAIN_REF_CONNECT_UNUSED,
      `GENNUM_WIRE_SPEC_BTRAIN_REF(I_HostA)
      );

   // Device Under Test "B"
   spec_btrain_ref_top
     #(
       .g_simulation (1),
       .g_dpram_initf("../../ip_cores/wr-cores/bin/wrpc/wrc_phy8_sim.bram")
       )
   DUT_B
     (
      .button1_n_i(rst_n),
      .sfp_txp_o(LinkB2A.p),
      .sfp_txn_o(LinkB2A.n),
      .sfp_rxp_i(LinkA2B.p),
      .sfp_rxn_i(LinkA2B.n),
      .sfp_los_i(LinkB2A.break_link),
      `SPEC_BTRAIN_REF_CONNECT_CLOCKS,
      `SPEC_BTRAIN_REF_CONNECT_UNUSED,
      `GENNUM_WIRE_SPEC_BTRAIN_REF(I_HostB)
      );

   /// ////////////////////////////////////////////////////////////////////////
   /// Declare and attach BTrain TX/RX interfaces to both DUTs and
   /// to the testbench.
   ///
   /// Interfaces are bound to the DUTs, thus they are actually instantiated
   /// in the DUTs. We later pass these hierarchical location of the
   /// interfaces to the TB programs.
   /// ////////////////////////////////////////////////////////////////////////

   bind DUT_A IBTrain I_BTrainTx (`BTRAIN_REF_DUT_ATTACH_TX_IF);
   bind DUT_A IBTrain I_BTrainRx (`BTRAIN_REF_DUT_ATTACH_RX_IF);
   bind DUT_B IBTrain I_BTrainTx (`BTRAIN_REF_DUT_ATTACH_TX_IF);
   bind DUT_B IBTrain I_BTrainRx (`BTRAIN_REF_DUT_ATTACH_RX_IF);

   // Set the last argument to '1' to enable verbose messages
   test_btrain_ref_design TB(DUT_A.I_BTrainTx, DUT_B.I_BTrainTx,
			     DUT_A.I_BTrainRx, DUT_B.I_BTrainRx,
			     LinkA2B, LinkB2A,
			     accA, accB, 1'b0);

   /// ////////////////////////////////////////////////////////////////////////
   /// Silence Xilinx unisim DSP48A1 warnings about invalid OPMODE
   /// ////////////////////////////////////////////////////////////////////////
   initial begin
      force DUT_A.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D1.OPMODE_dly = 0;
      force DUT_A.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D2.OPMODE_dly = 0;
      force DUT_A.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D3.OPMODE_dly = 0;
      force DUT_B.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D1.OPMODE_dly = 0;
      force DUT_B.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D2.OPMODE_dly = 0;
      force DUT_B.cmp_xwrc_board_spec.cmp_board_common.cmp_xwr_core.
	WRPC.LM32_CORE.gen_profile_medium_icache.U_Wrapped_LM32.cpu.
	  multiplier.D3.OPMODE_dly = 0;
   end


endmodule // main
