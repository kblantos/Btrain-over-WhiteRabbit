\normalsize
\subsection{Generics}

The generics that can be used to configure BTrainFrameTransceiver module are listed in
the table below.

\footnotesize
\begin{hdlparamtable} 
  g\_rx\_BframeType & std\_logic\_vector & c\_ID\_ALL & Select the type of
  BTrain frames accepted by the BTrainFrameTransceiver. The receiver can 
  either accept one type only (c\_ID\_BkFrame, c\_ID\_ImFrame,
  c\_ID\_CdFrame) or it can accept all types (c\_ID\_ALL). \\
  \hline
  g\_rx\_valid\_pol\_inv & std\_logic & 0 &  Polarity of the signal 
  \texttt{rx\_Frame\_valid\_pX\_o}. Set this generic to 0 for normal polarity 
  (active high). Set this generic to 1 for inverted polarity (active low)\\
  \hline
  g\_tx\_period\_value & std\_logic\_vector & 0x0 & Transmission period offframe
  BTrain frames. Period transmission is disabled when it is set to zero. Otherwise,
  BTrain frames are transmitted with a period set in this generic. Period is expressed
  in clock cylces (16ns).
  \\
  \hline
  g\_rx\_out\_data\_time\_valid & std\_logic\_vector & 0x1  & Length of the validity 
  of the output data, expressed in clock cycles (16ns). The received BTrain data is 
  registered for the indicated period, the validity of data is indicated by 
  the \texttt{rx\_Frame\_valid\_pX\_o} output signal. There are two special 
  values: 0x0000 and 0xFFFF. Set this generic to 0x0000 to disable the output. 
  Set this generic to 0xFFFF to make the output data continuously valid until next 
  update.
  \\
  \hline
  g\_rx\_out\_data\_time\_delay & std\_logic\_vector & 0x0 & Delay before the received BTrain
  information is presented in the output registers (indicated by
  \texttt{rx\_Frame\_valid\_pX\_o}). It is expressed in clock cycles (16ns).
  \\
  \hline
  g\_use\_wb\_config & boolean & true & If set to TRUE, configuration of
  BTrainFrameTransceiver can be accessed via Wishbone. This allows to override
  the values that are set via generics. Additionally, debugging facilities are 
  provided. Set to FALSE, if do not need/have WB access to this module, it will
  save you some resources.
  \\
  \hline
  g\_slave\_mode & enum & CLASSIC &  external Wishbone Slave interface mode
  [PIPELINED/CLASSIC]
  \\
  \hline
  g\_slave\_granularity & enum & BYTE & granularity of address bus in external
  Wishbone Slave interface [BYTE/WORD]
  \\
  \hline
  g\_clk\_rate & integer & 62500000 & The rate of the clk\_i in Hz. It is
  used to calculate the transmission period properly, if set. Two values
  supported: 62500000 and 125000000.
  \\
\end{hdlparamtable}

\newpage
\subsection{Ports}\normalsize
\label{sec:BTrainFrameTransceiverPorts}
The table below lists all the input/output ports of the BTrainFrameTransceiver module.
\footnotesize
\begin{hdlporttable}
  \hdltablesection{Clocks and resets}\\
  \hline
  clk\_i    & in & 1 & clock input, It should be:
      \begin{itemize*}
    \item identical to the system clock provided by the Board Support Package (\tts{clk\_sys\_62m5\_o}), if 
          the default BTrain Rx configuration of streamers is used for the Board Support Package, i.e. \texttt{c\_rx\_streamer\_params\_btrain} in which \texttt{use\_ref\_clk\_for\_data} is set to zero; or
    \item identical to the system clock provided by the Board Support Package (\tts{clk\_ref\_125m\_o}), if 
          the BTrain Rx configuration of streamers used for the Board Support Package is modified such
          that \texttt{use\_ref\_clk\_for\_data} parameter of \texttt{c\_rx\_streamer\_params\_btrain} is set to one.
    \item the clock provided to the WR Streamers if used directly (i.e. without  the Board Support Package).
    \end{itemize*}
  \\
  \hline
  rst\_n\_i & in & 1 & reset input, active-low, syncrhonous to \tts{clk\_i}, it should be identical
  to the system reset provided by the Board Support Package (\tts{rst\_sys\_62m5\_o}), or to the
  reset provided to the WR Streamers if used directly.\\
  \hline
  \hdltablesection{Data interface with WR Streamers included in Board Support Package}\\
  \hline
  tx\_data\_o      & out & 142 & Data to be sent\\
  \hline
  tx\_valid\_o     & out & 1 & Indicates whether \tts{tx\_data\_o} contains valid data\\
  \hline
  tx\_dreq\_i      & in  & 1 &  When active, the user may send a data word in the
  following clock cycle\\
  \hline
  tx\_last\_p1\_o  & out & 1 & Can be used to indicate the last data word in a
  larger block of samples\\
  \hline
  tx\_flush\_p1\_o & out & 1 &  When asserted, the streamer will immediatly send
  out all the data that is stored in its TX buffer\\
  \hline
  rx\_data\_i      & in  & 142 & Received data\\
  \hline
  rx\_valid\_i     & in  & 1 & Indicates that \tts{rx\_data\_i} contains valid data\\
  \hline
  rx\_first\_p1\_i & in  & 1 & Indicates the first word of the data block on \tts{rx\_data\_i}\\
  \hline
  rx\_dreq\_o      & out & 1 & When asserted, the streamer may output another data word in the
  subsequent clock cycle\\
  \hline
  rx\_last\_p1\_i  & in  & 1 & Indicates the last word of the data block on \tts{rx\_data\_i}\\
  \hline
  \hdltablesection{Interface to receive BTrain-related information}\\
  \hline
  rx\_FrameHeader\_o      & out & rec & \tts{t\_FrameHeader}: WR-BTrain frame header\\
  \hline
  \hdltablerecfield{version\_id} & 2 & indicate version ID of frame payload, currently zero, to be used for backward compatible updates (was reserved before v1.2)\\
  \hline
  \hdltablerecfield{d\_low\_marker} & 1 & Defocusing low marker flag\\
  \hline
  \hdltablerecfield{f\_low\_marker} & 1 & Focusing low marker flag\\
  \hline
  \hdltablerecfield{zero\_cycle} & 1 & Zero cycle pulse\\
  \hline
  \hdltablerecfield{C0} & 1 & C0 pulse\\
  \hline
  \hdltablerecfield{error} & 1 & Error flag\\
  \hline
  \hdltablerecfield{sim\_eff} & 1 & Simulation/Effective bit, '0' if the values are measured, '1' if simulated\\
  \hline
  \hdltablerecfield{frame\_type} & 8 & type: 0x42 for B field frame, 0x49 for Imain frame, 0x4B for C frame\\
  \hline
  rx\_BFramePayloads\_o   & out & rec & \tts{t\_BFramePayload}: B-type payload\\
  \hline
  \hdltablerecfield{B} & 32 & B (dipole) value\\
  \hline
  \hdltablerecfield{Bdot} & 32 & Bdot value\\
  \hline
  \hdltablerecfield{oldB} & 32 & old B value\\
  \hline
  \hdltablerecfield{measB} & 32 & measured B value\\
  \hline
  \hdltablerecfield{simB} & 32 & simulated B value\\
  \hline
  \hdltablerecfield{synB} & 32 & synthetic B value\\
  \hline
  rx\_IFramePayloads\_o   & out & rec & \tts{t\_IFramePayload}: I-type payload\\
  \hline
  \hdltablerecfield{I} & 32 & Imain value\\
  \hline
  rx\_PFramePayloads\_o   & out & rec & \tts{t\_PFramePayload}: Deprecated P-type payload (not supported, maintained for backward-compatibility of the interface)\\
  \hline
  rx\_CFramePayloads\_o   & out & rec & \tts{t\_CFramePayload}: C-type payload\\
  \hline
  \hdltablerecfield{I} & 32 & I (current) value\\
  \hline
  \hdltablerecfield{V} & 32 & V (voltage) value\\
  \hline
  \hdltablerecfield{MSrcId} & 16 & Machine Source, ID 0: Undefined 1: PSB; 2: PS; 3: SPS; 4: LEIR; 5: AD; 6: ELENA \\
  \hline
  \hdltablerecfield{CSrcId} & 16 & Converter Source ID \\
  \hline
  \hdltablerecfield{UserData} & 32 & User-defined data\\
  \hline
  \hdltablerecfield{Reserved\_1} & 32 & Reserved for backward-compatibility Extensions of the format\\
  \hline
  \hdltablerecfield{Reserved\_2} & 32 & Reserved for backward-compatibility Extensions of the format\\
  \hline
  rx\_Frame\_valid\_pX\_o & out & 1 & indicates the reception of a valid frame. Pulse duration, polarity and delay configurable via generics\\
  \hline
  rx\_Frame\_typeID\_o    & out & 8 & Copy of \tts{rx\_FrameHeader\_o.frame\_type} for easier access\\
  \hline
  \hdltablesection{Interface to transmit BTrain-related information}\\
  \hline
  ready\_o & in & 1 & ready to transit (active high)\\
  \hline
  tx\_TransmitFrame\_p1\_i & in & 1 & Active high single-cycle request to transmit a frame to the WR network\\
  \hline
  tx\_FrameHeader\_i & in & rec & \tts{t\_FrameHeader}: see \tts{rx\_FrameHeader\_o}\\
  \hline
  tx\_BFramePayloads\_i & in & rec & \tts{t\_BFramePayload}: see \tts{rx\_BFramePayloads\_o}\\
  \hline
  tx\_IFramePayloads\_i & in & rec & \tts{t\_IFramePayload}: see \tts{rx\_IFramePayloads\_o}\\
  \hline
  rx\_PFramePayloads\_i  & in & rec & \tts{t\_PFramePayload}: see \tts{rx\_PFramePayloads\_o} (not supported, maintained for backward compatibility)\\
  \hline
  tx\_CFramePayloads\_i & in & rec & \tts{t\_CFramePayload}: see \tts{rx\_CFramePayloads\_o}\\
  \hline
\end{hdlporttable}
