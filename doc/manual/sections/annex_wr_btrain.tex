\normalsize
\subsection{General}


The WB interface  to the BTrain Frame Transceiver (\texttt{BTrainFrameTransceiver.vhd})
module allows configuration of transmission and reception parameters as well as simulation 
and debugging of BTrain frames. The module can be found in \\\texttt{hdl/rtl/BTrainFrameTransceiver/}.

\subsection{Control of transmission and reception}
\label{sec:Control of transmission and reception}

The transmission of BTrain frames can be triggered by 3 sources:
\begin{enumerate*}
\item \textbf{Input std\_logic signal} - the \texttt{tx\_TransmitFrame\_p1\_i} that
       can be driven in VHDL by the \textit{user's logic}.
\item \textbf{Single transmission request} - it can be made via WB interface by writing 
      one to the bit \texttt{TX\_SINGLE} of the \texttt{SCR} register. This can accomplished
      using \texttt{wr-btrain} and its \texttt{txsfr} command.
\item \textbf{Periodic transmission signal} - the default period can be set with VHDL
      generic \texttt{g\_tx\_period\_value} (if not set, the default is zero). This default
      configuration can be overridden via WB interface by writing the period to the the 
      TX\_PERIOD register
      and writing  one to the bit \texttt{TX\_OR\_CONFIG} of register \texttt{SCR}. This can
      accomplished using the \texttt{wr-btrain} and its \texttt{txp} command. 
      In all cases, the period is expressed as a number of clock cycles of 62.5MHz clock (i.e. 16ns).
\end{enumerate*}
The reception of BTrain frames can be customized. In particular, the accepted BTrain 
frame type, timing characteristics and polarity of the signals provided to the \textit{user's logic} can be configured. The following configuration
is provided:
\begin{itemize*}
\item \textbf{Type of expected BTrain frame} -- it is possible to specify which type
      of BTrain frame is accepted by the receiver (i.e. B, I or C). This is done
      by providing the BTrain type ID to the \texttt{g\_rx\_BframeType} generic.
      If BTrain type ID is provided, BTrain frames of other type will be ignored.
      BTrain type ID enumeration is specified in \texttt{hdl/rtl/BTrainFrame\_pkg.vhd}.
      By default, accept all is specified.
\item \textbf{Time-length and delay of data valid} -- it is possible to define the delay 
      between the instant the BTrain frame is received and the instant the BTrain-related
      data is presented to the User's logic as well as the number of cycles for which the 
      data is presented to the User's logic. The BTrain-related data is presented to the
      User's logic in one of the output signals organized into records, i.e.:  
      \texttt{rx\_FrameHeader\_o}, \texttt{rx\_BFramePayloads\_o}, 
      \texttt{rx\_IFramePayloads\_o}, \texttt{rx\_CFramePayloads\_o}. The validity
      of data is indicated with the output signal \texttt{rx\_Frame\_valid\_pX\_o}.
      The default values of valid time-length and delay are set with VHDL generics 
      \texttt{g\_rx\_out\_data\_time\_valid} and  \texttt{g\_rx\_out\_data\_time\_delay}.
      These default values can be overridden via WB interface by writing to 
      \texttt{VALID} and \texttt{DELAY} fields of the register \texttt{RX\_OUT\_DATA\_TIME} 
      and writing one to the bit \texttt{RX\_OR\_CONFIG} of the register \texttt{SCR}.
      This can be accomplished using the \texttt{wr-btrain} and its \texttt{dv} command with
      appropriate parameters, i.e. \texttt{-d} and \texttt{-l} to set the
      delay and length respectively. Two special values of the \texttt{VALID} (
      \texttt{-l} parameter) are: 0x0000 -- output data disabled and
      0xFFFF: output data continuously valid until next update (the data might be invalid for the 
      time defined by delay). 
\item \textbf{Polarity of data valid}  -- it can be configured whether the value '1' (HIGH) signal of 
      \texttt{rx\_Frame\_valid\_pX\_o} indicates that the BTrain-related output data is 
      valid or not. The default value of \texttt{rx\_Frame\_valid\_pX\_o} polarity is 
      set with the VHDL generic \texttt{g\_rx\_valid\_pol\_inv} (HIGH=valid, LOW=invalid
      for BTrain). This default value can be overridden via WB interface by writing to 
      the big \texttt{RX\_VALID\_POL\_INV} of the register \texttt{SCR} and 
      writing one to the bit \texttt{RX\_OR\_CONFIG} of the register \texttt{SCR}. 
      This can be accomplished using the \texttt{wr-btrain} and its \texttt{dv} command with
      \texttt{-p} parameter.
\end{itemize*}
\noindent\textbf{IMPORTANT:} There is one "override" register for transmission and one for reception
configuration. If you want to override only one of the reception parameters (e.g. polarity), 
you must remember to configure the other parameters (valid time and delay) to the same
values as default. The type of accepted BTrain frames can be configured only via generic.


\subsection{Transmission of dummy or simulation BTrain frames}
\label{Transmission of dummy or simulation BTrain frames}

The BTrainFrameTransceiver IP core provides facilities to send dummy frames of all types
(i.e. B, I, C) and to simulate their B or I values (the first field after the BTrain header)
with a waveform that resembles real-life.
This can be used for developments/tests and demonstrations.

The debugging mode allows transmission of "fake" frames. The debugging mode is set by 
writing to the field \texttt{TX\_DBG} of the register \texttt{SRC}. The following modes are 
available:
\begin{itemize*}
\item \textbf{mode=0} -- debugging is disabled and the values provided on the inputs 
       of the  BTrainFrameTransceiver are transmitted.
\item \textbf{mode=1} -- fields of BTrain frame are filled with incremented values, 
      depending which type of frame is set to be transmitted (the type can be overridden) 
      \begin{itemize*}
      \item B-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      B     = x"00" & dbg_counter;
      Bdot  = x"00000001";
      oldB  = x"00000002";
      measB = x"00000003";
      simB  = x"DEADBEEF";
      synB  = x"01234567"; 
      \end{lstlisting}
      \item I-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I     = x"00" & dbg_counter;
      \end{lstlisting}
      \item C-frame
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I          = x"00" & dbg_counter;
      V          = x"CAFEBABE";
      MSrcId     = x"1234";
      CSrcId     = x"5678";
      UserData   = x"00000003";
      UserData   = x"00000003";
      Reserved_1 = x"deedbeaf";
      Reserved_2 = x"87654321";
      \end{lstlisting}
      \end{itemize*}
\item \textbf{mode=2} -- the value provided by the simple generator (simValue), which
      is described later, is sent in the fields B, or  I, depending which BTrain 
      frame type is transmitted. In all cases, it is the first field after the header
      of the BTrain frame.
      The fields of the BTrain frames are filled as follows:
      \begin{itemize*}
      \item B-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      B     = simValue; 
      Bdot  = x"00000001";
      oldB  = x"00000002";
      measB = x"00000003";
      simB  = x"DEADBEEF";
      synB  = x"01234567"; 
      \end{lstlisting}
      \item I-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I     = simValue;
      \end{lstlisting}
      \item C-frame
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I          = simValue;
      V          = x"CAFEBABE";
      MSrcId     = x"1234";
      CSrcId     = x"5678";
      UserData   = x"00000003";
      Reserved_1 = x"deedbeaf";
      Reserved_2 = x"87654321";
      \end{lstlisting}
      \end{itemize*}
\item \textbf{mode=3} --  fields of BTrain frame are filled with dummy values, i.e. 
      \begin{itemize*}
      \item B-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      B      = x"deafbeef",
      Bdot   = x"cafecafe",
      oldB   = x"87654321",
      measB  = x"deedbeaf",
      simB   = x"bad0babe",
      synB   = x"00000000"
      \end{lstlisting}
      \item I-frame:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I       = x"deedbeaf"
      \end{lstlisting}
      \item C-frame
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      I          = x"deafbeef",
      V          = x"cafecafe",
      MSrcId     = x"8765",
      CSrcId     = x"4321",
      UserData   = x"deedbeaf",
      Reserved_1 = x"87654321",
      Reserved_2 = x"deedbeaf"
      \end{lstlisting}
      \end{itemize*}
\end{itemize*}
The debugging mode can be set using the \texttt{wr-btrain} and its command \texttt{txdbg}.

The type of BTrain frame that is transmitted depends on the input provided by the User's logic
in the \texttt{FrameHeader\_i} input register. This type can be overridden via WB interface
by writing to the field \texttt{TX\_DBG\_FTYPE} of the register \texttt{SCR}.This can be
accomplished by using the \texttt{wr-btrain} and its command \texttt{ftdbg}.

The BTrainFrameTransceiver provides a simple generator of values that can be programmed
to provide stream of BTrain frames with B or I values (\texttt{simValue}) resembling 
real-live waveform. 
The generator is implemented in the module \texttt{BvalueSimGen.vhd} that is located in the folder 
\texttt{hdl/rtl/BTrainFrameTransceiver}, see the file for detailed description.

When the simulation is enabled, each time a BTrain frame is sent, the simulator increments 
the transmitted B value by a configurable signed value for a configurable number of 
transmissions. The two parameters (increment signed value and the number of transmissions) 
can be set for a configurable number of so-called sub-cycles. This configured number of 
sub-cycles compose a cycle that is repeated indefinitely. An example cycle consisting of
8 sub-cycles is presented in Figure~\ref{fig:simulatedCycle}.
\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{simulatedCycle.jpg}
    \caption{Simulated B value waveform}
    \label{fig:simulatedCycle}
  \end{center}
\end{figure}
The maximum number of sub-cycles is a VHDL generic parameter that can be read via WB interface
(parameter \texttt{CMAXLEN} of register \texttt{SIMB\_CTRL}). Each sub-cycle is configured
by writing
\begin{itemize*}
\item the sub-cycle ID to the field \texttt{SCID} of the register \texttt{SIMB\_CTRL} 
\item the sub-cycle length to the register \texttt{BSIM\_SCYC\_LEN}
\item the sub-cycle increment to the register \texttt{BSIM\_SCYC\_INC}
\end{itemize*}
This can be accomplished using the \texttt{wr-btrain} and its command \texttt{simsc}.

Once the required sub-cycles are configured, the number of sub-cycles that constitute 
the repeated cycle needs to be written to the field \texttt{CLEN} of the register
\texttt{SIMB\_CTRL}. This can be accomplished using the \texttt{wr-btrain} and its 
command \texttt{simc}.

\noindent\textbf{Note:} Each cycle is started with B (or I) value equal to zero, whatever was the 
end value at the end of the previous cycle. 

\noindent\textbf{Note:} Setting the field \texttt{CLEN} of the register \texttt{SIMB\_CTRL}
to 0xFF enables continuous mode. In this mode, the only value that matters is the increment 
 value for sub-cycle 0, i.e. the register \texttt{BSIM\_SCYC\_INC}. This allows a simple 
simulations of ramping the B (or I) value.

The generator is enabled when a value different than 0x0 is written to the field of 
\texttt{CLEN} in the register \texttt{SIMB\_CTRL}.  The transmission of values generated by 
this simple simulator is enabled by setting appropriate debugging mode (i.e. 0x02). 
The configured generator is envoked each time a BTrain frame is transitted. The
transmission can be triggered in one of 3 ways, as explained in 
section~\ref{sec:Control of transmission and reception}. It is recommended
to configure \textit{periodic transmission}.


The waveform in Figure~\ref{fig:simulatedCycle} is programmed by a script
\texttt{generateBtrain.sh} available in \texttt{sw/tools}.


\subsection{Snooping of received and transmitted BTrain values}

For quick debugging, it is possible to read the most recently received and transmitted
value in the first, after BTrain header, 32-bit field of a BTrain frame, i.e. the B, or I value
for the B type, or I and C type frame respectively. This value can be read from the
registers \texttt{DBG\_RX\_B\_OR\_I} and \texttt{DBG\_TX\_B\_OR\_I} for received
and transmitted frames respectively. This can be accomplished by using the 
\texttt{wr-btrain} and its commands \texttt{lrxfr} and \texttt{ltxfr}.

