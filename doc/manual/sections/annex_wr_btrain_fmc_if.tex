\normalsize
\subsection{General}

BTrain-over-WhiteRabbit project includes reference top level design for
the \href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP FMC}
board that is an FPGA Mezzanine Card (FMC) with a standard 
Low Pin Count (LPC) connector\footnote{FMC is defined in 
ANSI/VITA (VMEbus International Trade Association) 57.1 standard.}. This 
FMC module can be plugged onto a carrier board that can also have an FPGA.
With such an arrangement, the application-specific \textit{user's logic}
can be implemented in a separate FPGA than the \textit{WR-BTrain node}, 
as depicted in Figure~\ref{fig:WR-BTrainNode-BTrainOverFMC}.
This is an alternative architecture to the one presented in Figure~\ref{fig:WR-BTrainNode},
section~\ref{sec:WR-BTrainNode}. To support such an architecture, the BTrain-over-WhiteRabbit
project includes the \textit{Btrain FMC Interface} module (\texttt{hdl/rtl/BTrainFmcInterface})
which allows accessing the \textit{BTrain Frame Transceiver} module over the FMC pins 
to receive and transmit BTrain data. Following the \textit{BTrain over FMC interface}
specification in section~\ref{sec:BTrain FMC Interface}, the user can implement the 
carrier-side of the  \textit{BTrain over FMC interface}. For convenience, \textit{BTrain FMC Carrier} module ((\texttt{hdl/rtl/BTrainCarrierInterface}) is also provided 
in the BTrain-over-WhiteRabbit project. It is described in section~\ref{sec:BTrain Carrier Interface}. 
A top level reference design that instantiates
the \textit{BTrain FMC Carrier} module is available in the project
\href{https://gitlab.cern.ch/BTrain-TEAM/regfgc3-wr-btrain}{RegFGC3-WR-BTrain} 
for the 
\href{https://edms.cern.ch/ui/#!master/navigator/item?P:100129936:100208293:subDocs}{RegFGC3 VS Extension - FMC Carrier \& POF} board. This project assumes that the FMC is the 
\href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP FMC} with 
the v1.2 release binary.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{WR-BTrainNode-BTrainOverFMC.jpg}
    \caption{BTrain over FMC ace with WR-BTrain Node}
    \label{fig:WR-BTrainNode-BTrainOverFMC}
  \end{center}
\end{figure}

\newpage

\subsection{BTrain FMC Interface}
\label{sec:BTrain FMC Interface}

The user-defined pins of the LPC-FMC interface that are connected to FPGA
in the \href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP FMC} were
divided into two categories:
\begin{enumerate*}
\item \textbf{WR management interface} - it is meant to be the same across different WR applications, it includes reset input/output, interrupts output, WR Timing I/F outputs, as well as other signals
that were not implemented in this design.
\item \textbf{Data interface} - it is meant to be specific to application of the FMC carrier board. For \textit{Btrain over FMC interface}, it is implemented as two slave Wishbone classic
interfaces, each with 8-bit address and 8-bit bidirectional data working with the clock provided by the carrier
\end{enumerate*}
An overview of the \textit{Btrain over FMC interface} is provided in 
Figure~\ref{fig:BTrainOverFMC}. It is implemented by the \texttt{BTrainFmcInterface.vhd} 
module located in \texttt{hdl/rtl/BTrainFmcInterface}. This module
translates the "standard" BTrain interface (VHDL records) of \textit{Btrain Frame Transceiver}
into Wishbone (WB) registers. There are two sets of WB
registers and two WB slave interfaces, one for transmission and one for reception so that
these two can happen in parallel. The register maps of the two WB interfaces 
are provided in \ref{subsec:wbgen:RxBTFmcIF} and \ref{subsec:wbgen:TxBTFmcIF}. 
The registers have $2^8$ address space, 8-bit data size and the WB data bus is bidirectional, its direction is
based on the \texttt{rx\_wb\_we} signal (when \texttt{rx\_wb\_we} is high,
the data is written by the carrier, otherwise it is provided by the FMC).
\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.99\textwidth]{BTrainOverFMC.jpg}
    \caption{BTrain over FMC interface}
    \label{fig:BTrainOverFMC}
  \end{center}
\end{figure}


When a BTrain frame is received on the CUTE-WR-DP FMC, its content is written 
to appropriate WB registers in the \textit{Btrain FMC interface}.
There is a set of registers for the BTrain header and a set of registers for each
type of the BTrain payload. The appropriate payload register set is filled in based on the frame
type in the header. When the data is received, an interrupt is provided over
the \textit{WR management Interface} to the carrier. This is a shared interrupt,
thus the carrier needs to read the \textit{Status and control} Rx WB register
to verify whether the interrupt indicates data reception. If yes, first the 
header should be read such that the frame type  is known. Based on the frame type,
the carrier knows from which address to read the received frame payload.

When a BTrain frame is to be sent, the carrier should first write the 
header WB registers and the WB register payload set corresponding to the type of frame 
to be sent. Once all the data is written to  the appropriate WB registers,
the  \textit{Status and control} Tx WB register should be written to trigger
transmission. The type of transmitted frame depends on the value of BTrain
frame type in the header register set. 

Each of the WB slave interfaces works with the clock provided by the carrier. 
The Rx and Tx register maps are provided in \ref{subsec:wbgen:RxBTFmcIF} and \ref{subsec:wbgen:TxBTFmcIF}, respectively. 

% \newpage
\subsubsection{FMC pin mapping}
The mapping between the VHDL signals and the
FMC pins is provided in Figure~\ref{fig:BTrainOverFMC-management} and
Figure~\ref{fig:BTrainOverFMC-data}. 
Both figures describe mapping of FMC pins into hardware signals of 
\href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP} 
FMC board v2.2 and VHDL signals of the \texttt{cute\_btrain\_ref\_top.vhd}
provided in \texttt{hdl/top/cute\_ref\_design}. 
Figure~\ref{fig:BTrainOverFMC-management} provides FMC pin mapping for 
FMC-defined and \textit{WR management signals}.
Figure~\ref{fig:BTrainOverFMC-data} provides FMC pin mapping for WR-BTrain Data signals.
Note that a subset of the \textit{WR management} interface was implemented in
this prototype design.

The input/output ports and generics of the \texttt{BTrainFmcInterface} module 
that implements the \textit{BTrain over FMC} interface are described in the tables
in sections \ref{sec:Generics of BTrainFrameTransceiver} and \ref{sec:Ports of BTrainFrameTransceiver}.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{BTrain-over-FMC-IF-management.jpg}
    \caption{FMC pin mapping for FMC-defined and WR management signals.}
    \label{fig:BTrainOverFMC-management}
  \end{center}
\end{figure}
\newpage
\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{BTrain-over-FMC-IF-data.jpg}
    \caption{FMC pin mapping for WR Data signals.}
    \label{fig:BTrainOverFMC-data}
  \end{center}
\end{figure}

\subsubsection{Generics of BTrainFrameTransceiver}
\label{sec:Generics of BTrainFrameTransceiver}
\begin{hdlparamtable} 
  g\_with\_chipscope & boolean & FALSE & If set to TRUE, chipscope is instantiated. \\
\end{hdlparamtable}

\newpage

\subsubsection{Ports of BTrainFrameTransceiver}
\label{sec:Ports of BTrainFrameTransceiver}
\footnotesize
\begin{hdlporttable}

 \hdltablesection{Clocks and resets}\\
  \hline
  clk\_sys\_i    & in & 1 & clock input, it should be identical to the system 
  clock provided to the BTrainFrameTransceiver.\\
  \hline
  rst\_sys\_n\_i & in & 1 & reset input, active-low, synchronous to \tts{clk\_sys\_i}, it should be identical
  to the system reset provided to the BTrainFrameTransceiver.\\
  \hline
  \hdltablesection{Data interface with BTrainFrameTransceiver}\\
  \hline
  rx\_FrameHeader\_i    & in & rec & \tts{t\_FrameHeader}: WR-BTrain frame header, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_BFramePayloads\_i & in & rec & \tts{t\_BFramePayload}: B-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_CFramePayloads\_i & in & rec & \tts{t\_IFramePayload}: I-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_CFramePayloads\_i & in & rec & \tts{t\_CFramePayload}: C-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_Frame\_valid\_pX\_i & in & 1 & indicates the reception of a valid frame. Active-high expected\\
  \hline

  tx\_ready\_o          & in & 1 & indicates whether BTrainFrameTransceiver is ready for transmission\\
  \hline
  tx\_FrameHeader\_o    & out & rec & \tts{t\_FrameHeader}: WR-BTrain frame header, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_BFramePayloads\_o & out & rec & \tts{t\_BFramePayload}: B-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_CFramePayloads\_o & out & rec & \tts{t\_IFramePayload}: I-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_CFramePayloads\_o & out & rec & \tts{t\_CFramePayload}: C-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_Frame\_valid\_pX\_i & out & 1 & indicates the reception of a valid frame. Active-high expected\\
  
  \hline
  \hdltablesection{WR-BTrain Interface over FMC}\\
  \hline
  rx\_FrameIrq\_o       & out & 1 & indicates that BTrain frame has been received (irq that needs to be cleared via WB interface)\\
  \hline
  rst\_carrier\_n\_a\_i  & in & 1 & reset request from the carrier\\
  \hline
  rx\_wb\_clk\_i        & in & 1 & Rx WB clock input from the carrier, the operation of Rx WB interface is assumed to be synchronous with this clock \\
  \hline
  rx\_wb\_slave\_i      & in & rec & input signals of the Rx WB slave interface, see WB B4  spec\\
  \hline
  rx\_wb\_slave\_o      & out & rec & output signals of the Rx WB slave interface, see WB B4  spec\\
  \hline
  tx\_wb\_clk\_i        & in & 1 & Tx WB clock input from the carrier, the operation of Tx WB interface is assumed to be synchronous with this clock \\
  \hline
  tx\_wb\_slave\_i      & in & rec & input signals of the Tx WB slave interface, see WB B4  spec\\
  \hline
  tx\_wb\_slave\_o      & out & rec & output signals of the Tx WB slave interface, see WB B4  spec\\
\end{hdlporttable}
\normalsize
  \textbf{NOTE:} The interface of the \texttt{BTrainFmcInterface} provides uni-directional
  WB data vectors that are turned into bi-directional in the top-level entity, based
  on the value of \texttt{wb\_we} signal coming from the carrier.
  

\newpage

\subsection{BTrain Carrier Interface}
\label{sec:BTrain Carrier Interface}

The \texttt{BTrainCarrierInterface.vhd} module in \texttt{hdl/rtl/BTrainCarrierInterface}
implements the carrier's side of the \textit{BTrain over FMC interface} and provides
to the user the "standard" \textit{BTrain intrface} in the form of VHDL records.
By providing interface between the application-specific \textit{user's logic}
and \textit{BTrain-over-WhiteRabbit} cores on the pins of the FMC interface,
the two are independent in terms of maintenance and upgrades. Thus, the 
The \texttt{BTrainCarrierInterface} module is meant to
be decoupled from the BTrain-over-WhiteRabbit project as much as possible. 
As such, it does not depend on any of the submodules (inside \texttt{hld/ip\_cores}) 
of the BTrain-over-WhiteRabbit repository. It only requires the files
inside \texttt{hdl/rtl/BTrainCarrierInterface} folder and the \texttt{BTrainFrame\_pkg.vhd}
package to be included in the application-specific project.

The "standard" \textit{WR-BTrain interface} is identical 
to that of the \texttt{BTrainFrameTransceiver} module and it is composed
of a number or VHDL records to transmit and receive BTrain frames, see the table 
below. 

When a BTrain frame is received, it is indicated with a single cycle strobe on the 
\texttt{rx\_Frame\_valid\_p1\_o} output signal. The header of the 
received BTrain frame is available in the \texttt{rx\_FrameHeader\_o} record,
while the payload of the BTrain frame is available in the record corresponding
to the type of the received frame, indicated in the header: 
\texttt{rx\_BFramePayloads\_o}, \texttt{rx\_IFramePayloads\_o} or \texttt{rx\_CFramePayloads\_o}.
If the the application-specific \textit{user's logic} is 
interested in receiving only one type of BTrain frame,
the type should be indicated using \texttt{rx\_Frame\_typeID\_i} (by default
all types are received).

In order to transmit BTrain frame, data needs to be provided into the 
header record \texttt{tx\_FrameHeader\_i} and payload record of the 
frame type indicated in the header: 
\texttt{tx\_BFramePayloads\_i}, \texttt{tx\_IFramePayloads\_i} or \texttt{tx\_CFramePayloads\_i}.
The transmission can be requested with a single clock 
strobe on the \texttt{tx\_TransmitFrame\_p1\_i}, provided the \texttt{tx\_ready\_o}
is low. 

The WB-based \textit{BTrain over FMC} interface is not particularly fast
while the application-specific \textit{user's logic} might not require 
all the fields in the BTrain frames. This is way, the module allows specifying
which fields in the BTrain frames received by the CUTE-WR-DP FMC module should be
read out by the carrier. Similarly,  the module allows specifying which 
fields in the BTrain frame transmitted by the CUTE-WR-DP FMC module should
be updated with the new data from a carrier.

For reception of the BTrain frames, the \texttt{rx\_PFieldsReadVector\_i}
signal vector
can be used to specify which fields in the payload of the received BTrain
frame should be read from FMC. It is a simple mechanism where each bit 
indicates respective field from the BTrain payload, for example:
\begin{itemize*}
\item  bit 0: indicates B    for B frame, I for I and C frames
\item  bit 1: indicates Bdot for B frame, V for C frame
\end{itemize*}
Only consecutive '1' (ones) are allowed, e.g.
\begin{itemize*}
\item "00000001" is correct and will result in reading only B value if B Frame is
      received or only I value if C Frame or I Frame is received
\item "00000101" is not correct, anyhow it will not break things but the behavior
       will be equivalent to "00000001"
\item "11111111" is correct and will result in reading all fields of the frame
\end{itemize*}

For transmission of frames, the \textit{tx\_PFieldsUpdateVector\_i} vector
signal
specifies which fields in the payload of the BTrain frame should
be updated (written) in the CUTE-WR-DP FMC by the carrier. For the fields that are
not updated, the previously written value will be used. This allows to
reduce the transmission latency. The rules for the input value are similar to the 
\texttt{rx\_PFieldsReadVector\_i} vector, meaning that only consecutive '1' (ones)
are allowed (e.g. "00000111", not "00000101") and all ones ("11111111")
results in updating all the fields in the BTrain payload, regardless of 
how many fields it has.

\newpage
\scriptsize
\begin{hdlporttable}

 \hdltablesection{Clocks and resets}\\
  \hline
  clk\_i & in & 1 & clock input that is used for the Rx and Tx Wishbone interface
  over the FMC pins. There is no particular requirements as to the frequency. Too slow
  clock will cause \textit{BTrain over FMC interface} to be slow. Clock faster than
  62.5MHz is not recommended.\\
  \hline
  rst\_n\_i & in & 1 & reset input, active-low, synchronous to \tts{clk\_i}.\\

  \hline
  \hdltablesection{WR-BTrain Interface over FMC}\\
  \hline
  rx\_FrameIrq\_i       & out & 1 & indicates that BTrain frame has been received (irq that needs to be cleared via WB interface)\\
  \hline
  rx\_wb\_clk\_o        & in & 1 & Rx WB clock, the operation of Rx WB interface is assumed to be synchronous with this clock \\
  \hline
  rx\_wb\_adr\_o        & out & 8 & WB address, see WB B4 spec \\ \hline
  rx\_wb\_dat\_o        & out & 8 & WB data, see WB B4  spec \\ \hline
  rx\_wb\_dat\_i        & in  & 8 & WB data, see WB B4  spec \\ \hline
  rx\_wb\_cyc\_o        & out & 1 & WB cycle signal, see WB B4  spec \\ \hline
  rx\_wb\_stb\_o        & out & 1 & WB strobe signal, see WB B4  spec \\ \hline
  rx\_wb\_we\_o         & out & 1 & WB write-enable signal, see WB B4  spec \\ \hline
  rx\_wb\_ack\_i        & in  & 1 & WB acknolwedge signal, see WB B4  spec\\ \hline
  rx\_wb\_err\_i        & in  & 1 & WB error signal, see WB B4  spec\\ \hline
  rx\_wb\_rty\_i        & in  & 1 & WB retry signal, see WB B4  spec\\ \hline
  rx\_wb\_stall\_i      & in  & 1 & WB stall signal, see WB B4  spec \\ \hline


  tx\_wb\_clk\_i        & in & 1 & Tx WB clock, the operation of Tx WB interface is assumed to be synchronous with this clock \\
  \hline
  tx\_wb\_adr\_o        & out & 8 & WB address, see WB B4  spec \\ \hline
  tx\_wb\_dat\_o        & out & 8 & WB data, see WB B4  spec \\ \hline
  tx\_wb\_dat\_i        & in  & 8 & WB data, see WB B4  spec \\ \hline
  tx\_wb\_cyc\_o        & out & 1 & WB cycle signal, see WB B4  spec \\ \hline
  tx\_wb\_stb\_o        & out & 1 & WB strobe signal, see WB B4  spec \\ \hline
  tx\_wb\_we\_o         & out & 1 & WB write-enable signal, see WB B4  spec \\ \hline
  tx\_wb\_ack\_i        & in  & 1 & WB acknolwedge signal, see WB B4  spec\\ \hline
  tx\_wb\_err\_i        & in  & 1 & WB error signal, see WB B4  spec\\ \hline
  tx\_wb\_rty\_i        & in  & 1 & WB retry signal, see WB B4  spec\\ \hline
  tx\_wb\_stall\_i      & in  & 1 & WB stall signal, see WB B4  spec \\ \hline
  \hline
  \hdltablesection{Data interface with BTrainFrameTransceiver}\\
  \hline
  rx\_FrameHeader\_o    & out & rec & \tts{t\_FrameHeader}: WR-BTrain frame header, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_BFramePayloads\_o & out & rec & \tts{t\_BFramePayload}: B-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_CFramePayloads\_o & out & rec & \tts{t\_IFramePayload}: I-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_CFramePayloads\_o & out & rec & \tts{t\_CFramePayload}: C-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  rx\_Frame\_typeID\_o & out & 8 & ID of the received frame (equal to the respective record in the header)\\
  \hline
  rx\_Frame\_valid\_p1\_o & out & 1 & indicates the reception of a valid frame, active-high expected, sigle-clock strobe.\\
  \hline
  rx\_PFieldsReadVector\_i & in & 8 & Vector that defines which fields in the payload of the received BTrain
    frame are interesting for the user, thus should be read from FMC.\\
  \hline
  rx\_Frame\_typeID\_i & in & 8 & indicates which frames the user is interested in receiving, by default all.\\
  \hline
  tx\_FrameHeader\_i    & in & rec & \tts{t\_FrameHeader}: WR-BTrain frame header, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_BFramePayloads\_i & in & rec & \tts{t\_BFramePayload}: B-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_CFramePayloads\_i & in & rec & \tts{t\_IFramePayload}: I-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_CFramePayloads\_i & in & rec & \tts{t\_CFramePayload}: C-type payload, see~\ref{sec:BTrainFrameTransceiverPorts}\\
  \hline
  tx\_Frame\_valid\_p1\_i & in & 1 & single-cycle strobe input will result in transmission of data in 1) 
  tx\_FrameHeader\_i record and 2) one of the payload records, depending on the frame type field in the
  tx\_FrameHeader\_i\\
  tx\_ready\_o          & in & 1 & indicates whether BTrainFrameTransceiver is ready for transmission\\
  \hline
  tx\_PFieldsUpdateVector\_i & in & 8 & Vector that defines which fields in the payload of the BTrain frame should
    be updated (written) in the FMC by the Carrier \\
  \hline

\end{hdlporttable}
